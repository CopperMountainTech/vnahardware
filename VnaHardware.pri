message("VnaHardware in " $$PWD)

LIB_ARCH = x64
contains(QMAKE_HOST.arch, x86):LIB_ARCH = x32
win32:contains(QMAKE_HOST.os, Linux):LIB_ARCH = x32

DST_DIR=$$top_builddir/Devices

COPY_DIR = $$PWD/lib/$$LIB_ARCH/Devices

contains(QMAKE_HOST.os,Windows): {
    COPY_DIR ~= s,/,\\,g
    DST_DIR ~= s,/,\\,g
}

QMAKE_POST_LINK += $$escape_expand(\n\t) $$QMAKE_COPY_DIR $$quote($$COPY_DIR) $$quote($$DST_DIR)

INCLUDEPATH += $$PWD/inc/
