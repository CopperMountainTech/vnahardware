#ifndef VNAPROPERTY_H
#define VNAPROPERTY_H

#include <QJsonObject>
#include <QJsonArray>
#include <vector>
#include <QList>

#include "Units.h"
#include "DevicePropertyBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {

#define JSON_SECTION_CONSTRAINTS   "CONSTRAINTS"
#define JSON_KEY_HW_ID              "HwId"

// ключи JSON файла
#define JSON_SECTION_SYNTH          "Synthesizer"
#define JSON_SECTION_FREQ           "Frequency"
#define JSON_SECTION_POWER          "Power"
#define JSON_SECTION_POWERSLOPE     "PowerSlope"
#define JSON_SECTION_BANDWIDTH      "Bandwidth"
#define JSON_SECTION_RANGE          "Range"


#define JSON_KEY_ROM_SIZE           "ROMSize"
#define JSON_KEY_FW                 "Firmware"


#define JSON_KEY_MIN                "Min"
#define JSON_KEY_MAX                "Max"
#define JSON_KEY_DEFAULT            "Default"
#define JSON_KEY_BOUND_RESOLUTION   "BoundsResolution"
#define JSON_KEY_GRID_RESOLUTION    "GridResolution"
#define JSON_KEY_REVERSE_ENABLED    "ReverseEnabled"
#define JSON_KEY_SWEEP_ENABLED      "SweepEnabled"
#define JSON_KEY_STEP_MIN           "StepMin"

#define JSON_KEY_AVERAGE_DELAY      "AverageDelay"
#define JSON_KEY_FIRST_IF           "FirstIF"
#define JSON_KEY_SECOND_IF          "SecondIF"
#define JSON_KEY_IF_ACCURACY        "IFAccuracy"
#define JSON_KEY_BANDS              "Bands"                         // IFBW bands...

#define JSON_KEY_PORTS_TYPE         "PortsType"

class FrequencyConstraints
{
public:
    FrequencyConstraints() :
        _min(Hertz(900)),
        _max(Hertz(1E9)),
        _step(Hertz(1)),
        _defaultValue(Hertz(100E6)),
        _boundsResolution(Hertz(1)),
        _gridResolution(Hertz(1)),
        _isReverseAvailable(true) {}

    void read(const QJsonObject& parent)
    {
        QJsonObject json = parent[JSON_SECTION_FREQ].toObject();
        _min = Hertz(json[JSON_KEY_MIN].toDouble());
        _max = Hertz(json[JSON_KEY_MAX].toDouble());
        _step = Hertz(json[JSON_KEY_STEP_MIN].toDouble());
        _defaultValue = Hertz(json[JSON_KEY_DEFAULT].toDouble());
        _boundsResolution = Hertz(json[JSON_KEY_BOUND_RESOLUTION].toDouble());
        _gridResolution = Hertz(json[JSON_KEY_GRID_RESOLUTION].toDouble());
        _isReverseAvailable = json[JSON_KEY_REVERSE_ENABLED].toBool();
    }

    inline Unit min()
    {
        return _min;
    }

    inline Unit max()
    {
        return _max;
    }

    inline Unit step()
    {
        return _step;
    }

    inline Unit defaultValue()
    {
        return _defaultValue;
    }

    inline Unit boundsResolution()
    {
        return _boundsResolution;
    }

    inline Unit gridResolution()
    {
        return _gridResolution;
    }

    bool isReverseAvailable()
    {
        return _isReverseAvailable;
    }

protected:
    Unit _min;
    Unit _max;
    Unit _step;
    Unit _defaultValue;
    Unit _boundsResolution;
    Unit _gridResolution;
    bool _isReverseAvailable;
};


class  PowerConstraints
{
public:
    PowerConstraints() :
        _min(DbmW(-70)),
        _max(DbmW(10)),
        _defaultValue(DbmW(0)),
        _gridResolution(DbmW(10)),
        _isSweepAvailable(true)
    {
    }

    void read(const QJsonObject& parent)
    {
        QJsonObject json = parent[JSON_SECTION_POWER ].toObject();

        _min = DbmW(json[JSON_KEY_MIN].toDouble());
        _max = DbmW(json[JSON_KEY_MAX].toDouble());
        _defaultValue = DbmW(json[JSON_KEY_DEFAULT].toDouble());
        _gridResolution = DbmW(json[JSON_KEY_GRID_RESOLUTION].toDouble());
        _isSweepAvailable = json[JSON_KEY_SWEEP_ENABLED].toBool();
    }

    inline Unit min()
    {
        return _min;
    }

    inline Unit max()
    {
        return _max;
    }

    inline Unit defaultValue()
    {
        return _defaultValue;
    }

    inline Unit gridResolution()
    {
        return _gridResolution;
    }

    inline bool isSweepAvailable()
    {
        return _isSweepAvailable;
    }

protected:
    Unit _min;
    Unit _max;
    Unit _defaultValue;
    Unit _gridResolution;
    bool _isSweepAvailable;
};


class  PowerSlopeConstraints
{
public:
    PowerSlopeConstraints() :
        _min(DbmW(-70)),
        _max(DbmW(10))
    {
    }

    void read(const QJsonObject& parent)
    {
        QJsonObject json = parent[JSON_SECTION_POWERSLOPE].toObject();

        _min = DbmW(json[JSON_KEY_MIN].toDouble());
        _max = DbmW(json[JSON_KEY_MAX].toDouble());
    }

    inline Unit min()
    {
        return _min;
    }

    inline Unit max()
    {
        return _max;
    }

protected:
    Unit _min;
    Unit _max;
};


class  IfbwConstraints
{
public:
    IfbwConstraints() :
        _min(Hertz(900)),
        _max(Hertz(1E6)),
        _defaultValue(Hertz(100E6))
    {

    }

    void read(const QJsonObject& parent)
    {
        QJsonObject json = parent[JSON_SECTION_BANDWIDTH].toObject();

        _defaultValue = Hertz(json[JSON_KEY_DEFAULT].toDouble());

        _bands.clear();
        QJsonArray jsonArray = json[JSON_KEY_BANDS].toArray();
        if (jsonArray.size() > 0)
        {
            double min = INT_MAX;
            double max = INT_MIN;
            double ifbw;
            for (int i = 0; i < jsonArray.size(); ++i)
            {
                ifbw = jsonArray[i].toDouble();
                _bands.push_back(Hertz(ifbw));
                if (ifbw < min) min = ifbw;
                if (ifbw > max) max = ifbw;
            }
            _min = Hertz(min);
            _max = Hertz(max);
        }
        else
        {
            _min = Hertz(json[JSON_KEY_MIN].toDouble());
            _max = Hertz(json[JSON_KEY_MAX].toDouble());
        }
    }

    inline Unit min()
    {
        return _min;
    }

    inline Unit max()
    {
        return _max;
    }

    inline Unit defaultValue()
    {
        return _defaultValue;
    }

    inline QList<Unit> bands()
    {
        return _bands;
    }

protected:
    Unit _min;
    Unit _max;
    Unit _defaultValue;
    QList<Unit> _bands;
};


class  RangeConstraints
{
public:
    RangeConstraints() :
        _minPointsCount(2),
        _maxPointsCount(100000),
        _defaultPointsCount(201)
    {}

    void read(const QJsonObject& parent)
    {
        QJsonObject json = parent[JSON_SECTION_RANGE].toObject();

        _minPointsCount = json[JSON_KEY_MIN].toInt();
        _maxPointsCount = json[JSON_KEY_MAX].toInt();
        _defaultPointsCount = json[JSON_KEY_DEFAULT].toInt();
    }

    inline int minPointsCount()
    {
        return _minPointsCount;
    }

    inline int maxPointsCount()
    {
        return _maxPointsCount;
    }

    inline int defaultPointsCount()
    {
        return _defaultPointsCount;
    }

protected:
    int _minPointsCount;
    int _maxPointsCount;
    int _defaultPointsCount;
};


//
// Свойства синтеза
//
class  SynthesisConstraints
{
public:
    SynthesisConstraints() :
        _averageDelay(Second(0)),
        _firstIf(Hertz(1.27E6)),
        _secondIf(Hertz(1.17E6)),
        _accuracyIf(Hertz(0.2))
    {}

    void read(const QJsonObject& parent)
    {
        QJsonObject synthesisSection = parent[JSON_SECTION_SYNTH].toObject();

        _averageDelay = Second(synthesisSection[JSON_KEY_AVERAGE_DELAY].toDouble());
        _firstIf = Hertz(synthesisSection[JSON_KEY_FIRST_IF].toDouble());
        _secondIf = Hertz(synthesisSection[JSON_KEY_SECOND_IF].toDouble());
        _accuracyIf = Hertz(synthesisSection[JSON_KEY_IF_ACCURACY].toDouble());

        _frequency.read(synthesisSection);
        _power.read(synthesisSection);
        _powerSlope.read(synthesisSection);
        _ifbw.read(synthesisSection);
        _points.read(synthesisSection);
    }

    inline Unit averageDelay()
    {
        return _averageDelay;
    }

    inline Unit firstIf()
    {
        return _firstIf;
    }

    inline Unit secondIf()
    {
        return _secondIf;
    }

    inline Unit accuracyIf()
    {
        return _accuracyIf;
    }

    inline FrequencyConstraints frequency()
    {
        return _frequency;
    }

    inline PowerConstraints power()
    {
        return _power;
    }

    inline PowerSlopeConstraints powerSlope()
    {
        return _powerSlope;
    }

    inline IfbwConstraints ifbw()
    {
        return _ifbw;
    }

    inline RangeConstraints points()
    {
        return _points;
    }

protected:
    Unit _averageDelay;
    Unit _firstIf;
    Unit _secondIf;
    Unit _accuracyIf;
    FrequencyConstraints _frequency;
    PowerConstraints _power;
    IfbwConstraints _ifbw;
    RangeConstraints _points;
    PowerSlopeConstraints _powerSlope;
};

enum class PortType
{
    Receiving = 0,
    Active = 1                  // and Receiving
};

class VnaConstraints
{
public:
    VnaConstraints() :
        _outEP(0),
        _inEP(0),
        _mcuFirmware(""),
        _romSize(0),
        _synthesis(),
        _ports(),
        _activePortCount(0),
        _receivingPortCount(0)
    {}

    VnaConstraints(const QJsonObject& json)        //секция MODIFICATION
    {
        //  QJsonObject json = jsonDeviceObj[JSON_SECTION_MODEL].toObject();
        _outEP = static_cast<unsigned char>(json[JSON_KEY_OUT_EP].toInt());
        _inEP = static_cast<unsigned char>(json[JSON_KEY_IN_EP].toInt());
        _mcuFirmware = json[JSON_KEY_FW].toString("");
        //  qDebug() << _mcuFirmware;
        _romSize = json[JSON_KEY_ROM_SIZE].toInt(0);

        _synthesis.read(json);

        _ports.clear();
        _activePortCount = 0;
        QJsonArray jsonArray = json[JSON_KEY_PORTS_TYPE].toArray();
        if (jsonArray.size() > 0)
        {
            for (int i = 0; i < jsonArray.size(); ++i)
            {
                PortType portType = static_cast<PortType>(jsonArray[i].toInt());
                _ports.append(portType);
                if (portType == PortType::Active)
                    _activePortCount++;
            }
        }
        _receivingPortCount = _ports.size();
    }

    inline unsigned char outEP()
    {
        return _outEP;   // Номер EP OUT
    }

    inline unsigned char inEP()
    {
        return _inEP;   // Номер EP IN
    }

    inline QString mcuFirmware()
    {
        return _mcuFirmware;   // Имя файла микропрограммы
    }

    inline int romSize()
    {
        return _romSize;   // Размер ПЗУ
    }

    inline SynthesisConstraints synthesis()
    {
        return _synthesis;   // Свойства синтезатора
    }

    inline QList<PortType> ports()
    {
        return _ports;   // список типов портов
    }

    inline int activePortCount()
    {
        return _activePortCount;   // кол-во активных портов (передающий и)
    }

    inline int receivingPortCount()
    {
        return _receivingPortCount;
    }

protected:
    unsigned char _outEP;        // Номер EP OUT
    unsigned char _inEP;         // Номер EP IN
    QString _mcuFirmware;        // Имя файла микропрограммы
    int _romSize;                // Размер ПЗУ
    SynthesisConstraints _synthesis;  // Свойства синтеза
    QList<PortType> _ports;      // список типов портов
    int _activePortCount;        // кол-во активных портов (передающий и)
    int _receivingPortCount;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // VNAPROPERTY_H
