#pragma once

#include <QThread>

#include "VnaPipe.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class IVnaInPipe : public VnaPipe
{
    Q_OBJECT

public:
    IVnaInPipe() : VnaPipe() {}
    virtual ~IVnaInPipe() {}

    virtual void setBinderIdx(int idx) = 0;

signals:
    void temperatureNotice();
};

} //namespace Hardware
} //namespace VNA
} //namespace Planar
