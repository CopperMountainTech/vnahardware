#pragma once

#include <QThread>

#include "VnaFoundationConstants.h"
#include "VnaHw.h"
#include "VnaPipe.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class IVnaOutPipe : public QObject
{
    Q_OBJECT

public:
    ~IVnaOutPipe() {}

    virtual void setTriggerMode(Planar::VNA::TriggerType mode) = 0;
    virtual void setTriggerSource(TriggerSourceType source) = 0;
    virtual void setBinderIdx(int idx) = 0;
    virtual ScannerStateType scannerState() = 0;
    virtual void setTrigger() = 0;
    virtual bool waitForScannerState(ScannerStateType state) = 0;
    virtual void quit() = 0;

    virtual void beginUpdateCtrl() = 0;                     // вызвать перед изменение controlData
    virtual void endUpdateCtrl() = 0;                       // вызвать после изменения controlData
    virtual std::vector<unsigned char>& controlData() = 0;       // controlData

public slots:
    virtual void doWork() = 0;

signals:
    void scannerStateChanged(ScannerStateType state);
    void triggerTypeChanged(TriggerType triggerType);
};

} //namespace Hardware
} //namespace VNA
} //namespace Planar
