#ifndef EMULATEPOINT_H
#define EMULATEPOINT_H

namespace Planar {
namespace VNA {
namespace Hardware {


void EmulatePoint(double f, double& m0, double& m1, double& p0, double& p1);

double Noise(double noiseFactor = 0.001);

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // EMULATEPOINT_H
