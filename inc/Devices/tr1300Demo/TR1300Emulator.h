#ifndef TR1300EMULATOR_H
#define TR1300EMULATOR_H

#include <QMutex>
#include <QVector>
#include <QTime>
#include "Protocol.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class TR1300Emulator
{
public:
    TR1300Emulator();
    ~TR1300Emulator();

    bool write(unsigned char* buf, int size);
    void read(unsigned char* buf, int bufSize, int& actualSize);

    void clear();

    bool ready();                                             // готов к обработке нового блока данных

protected:
    void processMeasureCommand(InPkt32* answPkt, OutPkt32* outPkt);

    QMutex _mutex;
    QVector<InPkt32> _answQueue;
    QTime _time;
    double _kHzIfbw;
};


}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // TR1300EMULATOR_H
