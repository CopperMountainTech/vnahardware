#pragma once

#include <QMutex>
#include <QWaitCondition>

#include "VnaOutPipeBase.h"
#include "TR1300Emulator.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class TR1300DemoOutPipe : public VnaOutPipeBase
{
    Q_OBJECT
public:
    TR1300DemoOutPipe(TR1300Emulator* emulator, QList<IBinder*>* binderList);
    ~TR1300DemoOutPipe() override;

protected:
    bool processCommand(unsigned char* buf, int size);

    bool xferControlData();
    bool xferPoints();

    bool xfer(unsigned char* buf, int size) override;

    TR1300Emulator* _emulator;

};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

