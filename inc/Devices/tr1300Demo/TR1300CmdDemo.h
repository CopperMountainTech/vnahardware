#pragma once

#include <QVector>

#include "MeasuringResult.h"
#include "VnaCmd.h"

namespace Planar {
namespace VNA {
namespace Hardware {




class TR1300CmdDemo : public VnaCmd
{
public:
    TR1300CmdDemo();

    // Override VnaCmd (не обращаются к аппаратуре)
    bool ResetPipeline() override;                                           // Сброс конвейера
    bool FlashRead(int addr, unsigned char* buf, int size) override;         // Чтение FLASH памяти
    bool FlashWrite(int addr, unsigned char* buf, int size) override;        // Запись FLASH памяти
    bool DspWrite(unsigned char* buf, int size) override;                    // запись микропрограммы в RAM DSP
    bool FpgaWrite(unsigned char* buf, int size) override;                   // Запись микропрограммы FPGA


    // TR1300
    void CmdPreset(std::vector<unsigned char>& outBuf, void* context);
    void cmdMeasure(std::vector<unsigned char>& outBuf, int activePort, int pointIdx, double hzFreq);
    void CmdBackScan(std::vector<unsigned char>& outBuf, int activePort, int dacArm);

    bool tryParseMeasure(MeasuringResult* result, unsigned char* buf) override;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

