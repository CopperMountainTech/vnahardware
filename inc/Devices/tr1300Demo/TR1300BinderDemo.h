#pragma once

#include "Binder.h"

#include "TR1300SynthesizerDemo.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TR1300BinderDemo : public Binder
{
public:
    TR1300BinderDemo(Planar::AnalyzerItem* parent, VnaCmd* cmd, ScanRangeBase* range);
    ~TR1300BinderDemo() override;

    void outProcessing() override;
    unsigned char* outData(unsigned int& size) override;

private:
    TR1300SynthesizerDemo* _synthesizer;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

