#pragma once

#include "VnaBase.h"
#include "ScanRangeBase.h"
#include "TR1300Emulator.h"
#include "Properties/TR1300TemperatureNotice.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TR1300Demo : public VnaBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(BoolProperty* rfOutEnable READ rfOutEnable NOTIFY propertyChanged)
    Q_PROPERTY(EnumPropertyBase* referenceSource READ referenceSource NOTIFY propertyChanged)

    Q_PROPERTY(BoolProperty* powerDetectorEnable READ powerDetectorEnable NOTIFY propertyChanged)

    Q_PROPERTY(TriggerInProperty* triggerIn READ triggerIn NOTIFY propertyChanged)
    Q_PROPERTY(TriggerOutProperty* triggerOut READ triggerOut NOTIFY propertyChanged)

    Q_PROPERTY(TR1300TemperatureNotice* temperature READ temperature NOTIFY propertyChanged)

public:
    TR1300Demo(VnaDevicePool* pool, Planar::Hardware::DeviceId id, Planar::AnalyzerItem* parent);
    ~TR1300Demo() override;

protected:
    IBinder* CreateBinder(ScanRangeBase* range) override;  // Наследник должен переопределить этот метод и создавать свой Binder
    VnaCmd* Cmd() override;

    bool open() override;
    bool createPipeline() override;                                      // Создание и запуск конвейера измерения
    bool activate() override;
    void deactivate() override;
    void startPipeline() override;
    void stopPipeline() override;

// Properties
    inline BoolProperty* rfOutEnable()
    {
        return &_rfOutEnable;
    }

    inline EnumProperty<ReferenceSourceType>* referenceSource()
    {
        return &_referenceSource;
    }



    inline BoolProperty* powerDetectorEnable()
    {
        return &_powerDetectorEnable;
    }

    inline TriggerInProperty* triggerIn()
    {
        return &_triggerIn;
    }

    inline TriggerOutProperty* triggerOut()
    {
        return &_triggerOut;
    }

    // Notice
    inline TR1300TemperatureNotice* temperature()
    {
        return &_temperature;
    }


    TR1300Emulator* _emulator;

// Properties members
    BoolProperty _rfOutEnable;
    EnumProperty<ReferenceSourceType> _referenceSource;

    BoolProperty _powerDetectorEnable;

    TriggerInProperty _triggerIn;
    TriggerOutProperty _triggerOut;
    TR1300TemperatureNotice _temperature;

protected slots:
    void onSetTriggerSource(int source) override;
    virtual void slotSetRfOutEnable(bool enable);
    virtual void slotSetReferenceSource(int source);
    virtual void slotSetPowerDetectorEnable(bool enable);
    void slotSetTriggerIn();
    void slotSetTriggerOut();

    void slotTemperatureRequest();
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

