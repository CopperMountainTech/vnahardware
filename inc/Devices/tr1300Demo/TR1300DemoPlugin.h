#pragma once

#include "IPlugin.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TR1300DemoPlugin : public QObject, public Planar::Hardware::IPlugin
{
    Q_OBJECT
    Q_INTERFACES(Planar::Hardware::IPlugin)
    Q_PLUGIN_METADATA(IID "PLANAR.HW.TR1300Plagin.Metadata" FILE "TR1300Demo.json")

public:
    Q_INVOKABLE TR1300DemoPlugin(QObject* parent = nullptr);
    ~TR1300DemoPlugin() override;

    Planar::Hardware::DevicePool* Instance(const QJsonObject& jsonDeviceObj) override;
    void appendProperties(QList<ComponentInfo>* /*properties*/) override;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar



