#pragma once

#include <vector>

#include "SynthesizerBase.h"
#include "TR1300CmdDemo.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TR1300SynthesizerDemo : public SynthesizerBase
{
public:
    TR1300SynthesizerDemo(TR1300CmdDemo* cmd, ScanRangeBase* range);
    ~TR1300SynthesizerDemo() override;
protected:
    bool synthesizePoint(int port, int idx) override;
    bool postSynthesize() override;
    bool preSynthesize(int activePort) override;

private:
    TR1300CmdDemo* _cmd;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
