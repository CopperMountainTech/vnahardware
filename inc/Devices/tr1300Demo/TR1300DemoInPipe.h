#pragma once

#include "VnaHw.h"
#include "VnaInPipeBase.h"
#include "TR1300Emulator.h"


namespace Planar {
namespace VNA {
namespace Hardware {

class TR1300DemoInPipe : public VnaInPipeBase
{
    Q_OBJECT

public:
    TR1300DemoInPipe(TR1300Emulator* emulator, VnaCmd* cmd, QList<IBinder*>* binderList, QList<NoticeProperty*>& noticeList, QList<Planar::VNA::MeasuringResult*>& results);
    ~TR1300DemoInPipe() override;

protected:

    bool loopWork() override;

    TR1300Emulator* _emulator;
    unsigned char* _pkt;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

