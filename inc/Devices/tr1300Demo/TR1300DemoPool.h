#pragma once

#include "VnaDevicePool.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TR1300DemoPool : public VnaDevicePool
{
public:
    TR1300DemoPool(const QJsonObject& jsonDeviceObj);

    // DevicePool override
    Planar::Hardware::DeviceBase* createDevice(Planar::Hardware::DeviceId id, Planar::AnalyzerItem* parent);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
