//---------------------------------------------------------------------------
#ifndef FrequncyTableUnitH
#define FrequncyTableUnitH

#include <vector>

//---------------------------------------------------------------------------
typedef struct
{
    double f;
    double code;
} TFrequencyCode;
typedef std::vector<TFrequencyCode> TFrequencyTable;

double GetInterpolatedFrequencyCode(double f, TFrequencyTable& Table);
double GetInterpolatedFrequencyCode(double f, TFrequencyCode Table[], int size);
int    GetLowBoundFrequencyCode(double f, TFrequencyCode Table[], int size);
void   SetFrequencyTableToDefault(TFrequencyTable& Table, TFrequencyCode Data[], int size);
void   ClearFrequencyTable(TFrequencyTable& Table);
void   AddFrequencyTableRow(TFrequencyTable& Table, double f, double code);
#endif

