#pragma once

#include <vector>

#include "SynthesizerBase.h"
#include "S5180Cmd.h"

#include "Synthez5180.h"
#include "FactoryCalibration.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class S5180Synthesizer : public SynthesizerBase
{
public:
    S5180Synthesizer(S5180Cmd* cmd, ScanRangeBase* range, FactoryPowerCalibration* calibration);
    ~S5180Synthesizer();

protected:
    virtual bool synthesizePoint(int port, int idx);
    virtual bool postSynthesize();

    TSynt5180* _synth;

private:
    S5180Cmd* _cmd;
    FactoryPowerCalibration* _calibration;
//    QVector<S5180PllConfig> _pllConfig;              // частотные точки (делители синтезаторов) [points]

    int SBits;
    int RBits;
    S5180PllConfig _prevPllConfig;
    S5180Preset _preset;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

