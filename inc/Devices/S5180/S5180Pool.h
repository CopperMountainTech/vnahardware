#pragma once

#include "VnaDevicePool.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class S5180Pool : public VnaDevicePool
{
public:
    S5180Pool(const QJsonObject& jsonDeviceObj);

    // DevicePool override
    Planar::Hardware::DeviceBase* createDevice(Planar::Hardware::DeviceId deviceId, Planar::AnalyzerItem* parent);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
