#pragma once

#include "Binder.h"
#include "S5180Synthesizer.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class S5180Binder : public Binder
{
public:
    S5180Binder(Planar::AnalyzerItem* parent, VnaCmd* cmd, ScanRangeBase* range, FactoryPowerCalibration* calibration);
    ~S5180Binder() override;

    // выходной массив данных команд.
    unsigned char* outData(unsigned int& size) override;

    // Формирует выходной массив данных команд.
    void outProcessing() override;

protected:
    S5180Synthesizer* _synthesizer;

private:
    FactoryPowerCalibration* _factoryCalibration;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

