#pragma once

#include "VnaBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class S5180 : public VnaBase
{
public:
    S5180(VnaDevicePool* pool, Planar::Hardware::DeviceId id, Planar::AnalyzerItem* parent);

protected:
    bool initKeyAndSerial() override;
    IBinder* CreateBinder(ScanRangeBase* range) override;
    VnaCmd* Cmd() override;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
