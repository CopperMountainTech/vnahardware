﻿#pragma once


#include "FrequncyTableUnit.h"
#include "S5180Cmd.h"

namespace Planar {
namespace VNA {
namespace Hardware {

enum  TBlockSelect { bsLO = 0, bsRF1 = 1, bsVolt = 2, bsRF2 = 3 };

class TSynt5180;

//
// Рассчёт делителей PLL. Хранит таблицу калибровки конденсаторов.
//
class TSynthChannel
{
public:
    TSynthChannel(TSynt5180* Owner, bool IsHeterodyne);
    bool SynthesizePoint(double Freq, TSyntCh& Result, const double SignGrid, const double HetrGrid);

    double   UpAccuracy;
    double   DownAccuracy;

    TFrequencyTable FVCAPTable[2];

private:
    void GetVCAPCode(double f, int Num, unsigned char& VCO, unsigned char& CAP);

    TSynt5180*    FOwner;
    double        FUpAccuracy;
    double        FDownAccuracy;
    bool          FIsHeterodyne;

};




class TSynt5180
{
public:      // User declarations
    TSynt5180();
    ~TSynt5180();
    bool       GetSignalDivider(double Freq, TSyntCh& Result, double SignGrid, double HetrGrid);
    bool       GetNextSignalDivider(double Freq, TSyntCh& Result, double SignGrid, double HetrGrid);
    bool       GetHeterodyneDivider(double Freq, TSyntCh& Result, double SignGrid, double HetrGrid);
    void       AddVCAPTableRow(unsigned short BlockSelect, double f, unsigned char VCO, unsigned char CAP);
    void       ClearVCAPTable(unsigned short BlockSelect);


    unsigned char      CalibratedVCO()
    {
        return FCalibratedVCO;
    }

    void      setCalibratedVCO(unsigned char vco)
    {
        FCalibratedVCO = vco;
    }

    int       VCO_CAP_error()
    {
        return FVCO_CAP_error;
    }

    bool RelaxMode()
    {
        return FRelaxMode;
    }

    void SetRelaxMode(bool mode)
    {
        FRelaxMode = mode;    // для FOM
    }

    unsigned short     GetCommonDelay();
    unsigned short     GetDelayARM1();
    unsigned short     GetDelayARM2();
    unsigned short     GetPortSwitchDelay();
    unsigned short     GetVCOBSDelay();
    unsigned short     GetVCOBSDelayConditional(TSyntCh& Sign, TSyntCh& Heter, TSyntCh& PrevSign, TSyntCh& PrevHeter);
    unsigned short     GetREFTuneDelay1();
    unsigned short     GetREFTuneDelay2();
    // property
    /*


    double stepUpAccuracy()
    {
        return GetStepUpAccuracy();
    }

    void setStepUpAccuracy(double accuracy)
    {
        FSignal->UpAccuracy = accuracy;
    }

    double    stepDownAccuracy()
    {
        return FSignal->DownAccuracy;
    }

    void setStepDownAccuracy(double accuracy)
    {
        FSignal->DownAccuracy = accuracy;
    }
    */

    /*


        unsigned short    PortSwitchDelay()
        {
            return GetPortSwitchDelay;
        }

        unsigned short    REFTuneDelay1()
        {
            return GetREFTuneDelay1;
        }

        unsigned short    REFTuneDelay2()
        {
            return GetREFTuneDelay2;
        }

        unsigned short    VCOBSDelay()
        {
            return GetVCOBSDelay;
        }

        double    Frequency(TSyntCh Div)
        {
            return GetFrequency(Div);
        }

        double    VCOFrequency(TSyntCh Div)
        {
            return GetVCOFrequency(Div);
        }




        */


private:    // User declarations
    double        FCommonDelay;
    double        FDelayARM1;
    double        FDelayARM2;
    double        FPortSwitchDelay;
    double        FREFTuneDelay1;
    double        FREFTuneDelay2;
    double        FVCOBSDelay;
    unsigned char          FCalibratedVCO;
    int           FVCO_CAP_error;
    bool          FRelaxMode;

    TSynthChannel* FSignal;
    TSynthChannel* FHeterodyne;

    /*
    void SetStepUpAccuracy(const double accuracy)
    {
        FSignal->UpAccuracy = accuracy;
    }
    void SetStepDownAccuracy(const double accuracy)
    {
        FSignal->DownAccuracy = accuracy;
    }
    double     GetStepUpAccuracy() const
    {
        return FSignal->UpAccuracy;
    }
    double     GetStepDownAccuracy() const
    {
        return FSignal->DownAccuracy;
    }
    */




    double     CalcFrequency(int INT, unsigned long FRAC, int REF, bool Div, bool Mul2, int DCode, bool DownMixEnable, bool DownMixSelectLO2, bool Mul2ex);
    double     CalcVCOFrequency(int INT, unsigned long FRAC, int REF);
    void       SetDefaults();
    void       CheckSafety();
    double     GetVCOFrequency(TSyntCh Div);
    double     GetFrequency(TSyntCh Div);

};


//---------------------------------------------------------------------------
//extern PACKAGE TSynt5180* Synt5180;
//---------------------------------------------------------------------------

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
