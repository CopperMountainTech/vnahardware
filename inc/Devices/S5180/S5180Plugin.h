#pragma once

#include "IPlugin.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class S5180Plugin : public QObject, public Planar::Hardware::IPlugin
{
    Q_OBJECT
    Q_INTERFACES(Planar::Hardware::IPlugin)
    Q_PLUGIN_METADATA(IID "PLANAR.HW.S5180Plagin.Metadata" FILE "S5180.json")

public:
    Q_INVOKABLE S5180Plugin(QObject* parent = nullptr);
    ~S5180Plugin();

    Planar::Hardware::DevicePool* Instance(const QJsonObject& jsonDeviceObj);
    void appendProperties(QList<ComponentInfo>* properties);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar



