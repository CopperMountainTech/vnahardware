#pragma once

#include <QVector>

#include "MeasuringResult.h"
#include "VnaCmd.h"

namespace Planar {
namespace VNA {
namespace Hardware {


#pragma pack(push, 1)
class S5180Preset
{
public:
    S5180Preset() : Delay5us(0), ActivePort(0), IsSecondIf(0), IFBW(0), PortSwDelay5us(0) {}
    unsigned short Delay5us;            // Код задержки в 5us интервалах
    unsigned char ActivePort;           // Номер стимулирующего порта (1/0)
    unsigned char IsSecondIf;           // 1 - зеркальная ПЧ
    unsigned short IFBW;                // код полосы фильтра ПЧ: М*10^p.   М = {10, 15, 20, 30, 40, 50, 70}.  P = {-1, 0, 1, 2, 3}.  Байт 0 -М, байт 1 - P.
    unsigned short PortSwDelay5us;      // задержка переключения порта в 5us интервалах

    bool operator ==(S5180Preset const& p)
    {
        return (Delay5us == p.Delay5us) && (ActivePort == p.ActivePort) && (IsSecondIf == p.IsSecondIf) && (IFBW == p.IFBW) && (PortSwDelay5us == p.PortSwDelay5us);
    }


    bool operator !=(S5180Preset const& p)
    {
        return (Delay5us != p.Delay5us) && (ActivePort != p.ActivePort) && (IsSecondIf != p.IsSecondIf) && (IFBW != p.IFBW) && (PortSwDelay5us != p.PortSwDelay5us);
    }

    bool Update(unsigned short delay5us, unsigned char activePort, unsigned char isSecondIf, unsigned short ifbw, unsigned short portSwDelay5us)
    {
        bool changed = false;
        if (Delay5us != delay5us)
        {
            Delay5us = delay5us;
            changed = true;
        }
        if (ActivePort != activePort)
        {
            ActivePort = activePort;
            changed = true;
        }
        if (IsSecondIf != isSecondIf)
        {
            IsSecondIf = isSecondIf;
            changed = true;
        }
        if (IFBW != ifbw)
        {
            IFBW = ifbw;
            changed = true;
        }
        if (PortSwDelay5us != portSwDelay5us)
        {
            PortSwDelay5us = portSwDelay5us;
            changed = true;
        }

        return changed;
    }
};

#pragma pack(pop)

//
// Упаковка в команду
//
class TSyntCh
{
public:
    TSyntCh()
    {
        memset(&FData, 0, sizeof(FData));
    }
    // 5180
    TSyntCh(int INT, int FRAC, int REF, int Nfilt, bool Div, bool Mul2, int DCode, bool AltIF, int VCO, int CAP, int LMXPower, bool DownMixOrFiltEnable, bool DownMixSelectLO2, bool TauHi, bool TauLo,
            bool Mul2ex, bool IFAmp, int Dmy);

    unsigned char         FData[12];

    unsigned char   GetINT7();
    int    GetFRAC7();
    int    GetREF7();
    bool   GetDiv7();
    bool   GetMul2_7();
    bool   GetMul2ex_7();
    int    GetDCode7();
    unsigned char   GetFlags1_7();
    unsigned char   GetFlags2_7();
    unsigned char   GetVCO7();
    unsigned char   GetCAP7();
    unsigned char   GetLMXPwr7();
    bool   GetDownMixOrFiltEnable7();
    bool   GetDownMixSelectLO2_7();
    bool   GetTauLo7();
    bool   GetTauHi7();
};

class S5180PllConfig
{
public:
    TSyntCh rfCh;
    TSyntCh loCh;
    unsigned short stepAtt;
    unsigned short att;
};




class S5180Cmd : public VnaCmd
{
public:
    S5180Cmd(Planar::Hardware::DeviceBase* device);

    void CmdPreset(std::vector<unsigned char>& ctrlData, S5180Preset* preset);
    void CmdMeasure(std::vector<unsigned char>& ctrlData, int pointIdx, int activPort, S5180PllConfig* pllConfig);
    void CmdBackScan(std::vector<unsigned char>& ctrlData);
    void CmdTemperature(std::vector<unsigned char>& ctrlData);

    bool tryParseMeasure(MeasuringResult* result, unsigned char* buf) override;
    bool tryParseTemperature(std::vector<double>& temperature, unsigned char* buf) override; // проверка ответа температуры
    bool tryParsePointIndex(int& pointIndex, unsigned char* buf) override;

};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
