#ifndef TR5048CMD_H
#define TR5048CMD_H

#include <QVector>

#include "MeasuringResult.h"
#include "VnaCmd.h"
#include "TR5048TypeDef.h"

using namespace Planar::VNA;

namespace Hardware
{

#pragma pack(push, 1)



//
// поля регистра CTRL0 (TSyntCh.Flags)
//
#define BITP_CTRL0_LO_DIV           0                                       // 2
#define BITP_CTRL0_PLL_DIV          2
#define BITP_CTRL0_PRSC_SEL         4                                       // PRSC_SEL
#define BITP_CTRL0_D0               5                                       // D0
#define BITP_CTRL0_D1               6                                       // D1
#define BITP_CTRL0_D2               7                                       // D2

#define BITM_CTRL0_LO_DIV           (3 << BITP_CTRL0_LO_DIV )               // 2
#define BITM_CTRL0_PLL_DIV          (3 << BITP_CTRL0_PLL_DIV )
#define BITM_CTRL0_PRSC_SEL         (1 << BITP_CTRL0_PRSC_SEL)              // PRSC_SEL
#define BITM_CTRL0_D0               (1 << BITP_CTRL0_D0)                    // D0
#define BITM_CTRL0_D1               (1 << BITP_CTRL0_D1)                    // D1
#define BITM_CTRL0_D2               (1 << BITP_CTRL0_D2)                    // D2


//
// поля регистра CTRL1 (TSyntCh.Delay)
//
#define BITP_CTRL1_IF           0                                           // 0 - основная ПЧ, 1 - альтернативная ПЧ

#define BITP_CTRL1_TARM_HI      3                                           // 1, если Freq < 10 МГц
#define BITP_CTRL1_MIX          4                                           // DownMixSelect
#define BITP_CTRL1_TARM_LO      5                                           // 1, если Freq < 20 kHz

#define BITM_CTRL1_IF           (1 << BITP_CTRL1_IF)                        // 0 - основная ПЧ, 1 - альтернативная ПЧ
#define BITM_CTRL1_LO           (1 << BITP_CTRL1_LO)                        // Выбор частоты гетеродина

#define BITM_CTRL1_TARM_HI      (1 << BITP_CTRL1_TARM_HI)                   // 1, если Freq < 10 МГц
#define BITM_CTRL1_MIX          (1 << BITP_CTRL1_MIX)                       // DownMixSelect
#define BITM_CTRL1_TARM_LO      (1 << BITP_CTRL1_TARM_LO)                   // 1, если Freq < 20 kHz


//
// Упаковка делителей синтезатора
//
struct TR5048PllPkt
{
    TR5048PllPkt() : R(0), NINT(0), FRAC(0), CP(0), CTRL0(0), CTRL1(0), Reserved(0){}
    TR5048PllPkt(TSyntCh ch)
    {
        R = ch.Div.R;
        NINT = ch.Div.N;
        FRAC = ch.DDSFTW;       // так вот...
        CP = ch.CPCurr;
        CTRL0 = ch.Flags;
        CTRL1 = ch.GetFlags2();
        Reserved = 0;
    }
    unsigned short R;
    unsigned short NINT;
    unsigned int FRAC;
    unsigned char CP;                           // ток ЧФД 5 бит
    unsigned char CTRL0;
    unsigned char CTRL1;
    unsigned char Reserved;

    int GetTau()
    {
        int tau = 0;
        if((CTRL1 & BITM_CTRL1_TARM_HI) != 0)
        {
            if((CTRL1 & BITM_CTRL1_TARM_LO) != 0)
                tau = 0x03;
            else
                tau = 0x01;
        }
        return tau;
    }

    unsigned char IsDownMixSel()
    {
        return (CTRL1 & BITM_CTRL1_MIX) == 0 ? 0 : 1;
    }
};

//
// Рассчитанные и упакованные значения делителей всех синтезаторов
//
struct TR5048Pll
{
    TR5048Pll(){}
    TR5048Pll(TSyntCh rf, TSyntCh lo) : RF(rf), LO(lo){}
    TR5048PllPkt RF;
    TR5048PllPkt LO;
    int GetTau(){return RF.GetTau() | LO.GetTau();}
};

//
// Упаковка данных команды измерения
//
struct TR5048MeasurePayloadPkt
{
    TR5048Pll Plls;
    unsigned short StepAtt;
    unsigned short DacARM;
};


#pragma pack(pop)

class TR5048Cmd : public VnaCmd
{
public:
    TR5048Cmd(IIO *io);

    // Override VnaCmd
    bool CheckPipelineStart(unsigned char *buf, int markerId);
    bool CheckMeasure(unsigned char *buf);
    bool CheckBackScan(unsigned char *buf);


    // TR5048
    void CmdPreset(vector<unsigned char> &outBuf, TR5048Preset preset);
    void CmdMeasure(vector<unsigned char> &outBuf, TR5048Pll point, int activePort, int stepAtt, int dacARM, int pointIdx);
    void CmdBackScan(vector<unsigned char> &outBuf,TR5048Pll point, int stepAtt, int activePort, int dacArm);

    bool MeasureParse(MeasuringResult &result, unsigned char *buf);
    bool TemperatureParse(double &temperature, unsigned char *buf);
private:
    int _pointIdx;
    int _error;


};

}
#endif // TR5048CMD_H
