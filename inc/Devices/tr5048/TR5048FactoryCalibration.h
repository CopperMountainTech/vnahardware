#ifndef TR5048FACTORYCALIBRATION_H
#define TR5048FACTORYCALIBRATION_H

#include <QDateTime>
#include <QString>
#include <QDataStream>
#include <complex>

#include "FactoryCalibration.h"

#define PORT_NUMBER 2

namespace Hardware {

// используется для заводских калибровок 1300 и для хранения пользовательских калибровок
// структура для хранения заводской калибровки

#pragma pack(push, 1)

/////////////////////////////////////////////////////////////////////////////////////////////

///
/// \brief The TR5048Header struct
///        Начальный блок (c 0 адреса)
///
struct TR5048Header
{
    quint16  Marker;            // маркер начала блока
    quint16  Reserve;           //
    quint32  PowerTableEnd;     // Конец таблицы мощности
    //  int      CRC;              // Check summ

    bool IsValid()
    {
        return (Marker == ValidPatternConst);
    }

    static const quint32 StartAddressConst;
    static const quint16 ValidPatternConst;

    quint8* RawData(){ return reinterpret_cast<quint8*>(this); }
};
QDataStream &operator<<(QDataStream &out, const TR5048Header &p);
QDataStream &operator>>(QDataStream &in, TR5048Header &p);

///
/// \brief The TR5048PowerTableHeader struct
///        1. Заголовок таблицы калибровки мощности
///
struct TR5048PowerTableHeader
{
    quint16   Version;           // Версия
    quint16   FrequencyPlan;     // Код частотного плана
    quint32   MaxFrequency;      // MaxFrequency in MHz
    quint32   MaxPower;          // MaxPower in 0.001 dBm
    quint16   NumSteps;          // Число ступеней
    quint16   DACNumResp;        // Число характеристик ЦАП
    quint16   DACBegin;          // Начальный код ЦАП
    bool      PortValid[2];      // Порт действителен
    char      CalDate[2][12];    // Дата калибровки
    char      CalTime[2][10];    // Время калибровки
    quint16   CalTemper[2];      // Темрература калибровки
    //  int      CRC;               // Check summ

    QDateTime CalDateTime(int port);
    quint8* RawData(){ return reinterpret_cast<quint8*>(this); }

    quint32 PortBegin()
    {
        return  StartAddressConst +
                sizeof(TR5048PowerTableHeader) +
                sizeof(quint32) +
                sizeof(quint16) * DACNumResp +
                sizeof(quint32);
    }

    quint32 FreqLength(){ return static_cast<quint32>(NumFreq.at(FrequencyPlan));}
    quint32 TabLength() { return static_cast<quint32>(Version < 3 ? FreqLength() : FreqLength() + 2);}

    quint32 PortSize()
    {
        quint32 tabLength = TabLength();
        return  sizeof(TR5048PowerTableHeader) +
                sizeof(quint32) +
                NumSteps * (tabLength * sizeof(quint16) + sizeof(quint32)) +
                2*DACNumResp * (tabLength * sizeof(quint16) + sizeof(quint32)) +
                (Version >= 3 ? tabLength * sizeof(quint16) + sizeof(quint32) : 0) + // Место под таблицу калибровки R - приемника
                (Version == 4 ? tabLength * sizeof(quint16) + sizeof(quint32) : 0) + // Место под таблицу крутизны ЦАП "вниз"
                (Version >= 5 ? DACNumResp*(tabLength * sizeof(quint16) + sizeof(quint32)) : 0); // Место под таблицу крутизны ЦАП "вниз"
    }

    static const quint32 StartAddressConst;
    static const map<int, int> NumFreq;
};
QDataStream &operator<<(QDataStream &out, const TR5048PowerTableHeader &p);
QDataStream &operator>>(QDataStream &in, TR5048PowerTableHeader &p);

/// 2. Таблица номеров ступеней измерения характеристик ЦАП
///
///     USHORT    DACSteps[DACNumResp];
///     int       CRC;

/// 3.a. Заголовок таблицы канала
struct TR5048PowerPortHeader
{
    quint16   DACX01;            // Код 1 для измерения крутизны ЦАП на 0 ступени
    quint16   DACX02;            // Код 2 для измерения крутизны ЦАП на 0 ступени
    quint16   DACX1;             // Код 1 для измерения крутизны ЦАП на остальных ступенях
    quint16   DACX2;             // Код 2 для измерения крутизны ЦАП на остальных ступенях
    //  int      CRC;            // Check summ
};


/// 3.b. Таблица мощности ступеней канала I
///      NumSteps векторов + CRC:
///
///     USHORT   StepPower[N точек по частоте];
///     int      CRC;
/// 3.c. Таблица крутизны 1 ЦАП канала I
///      DACNumResp векторов + CRC:
///
///     USHORT   DACPower1[N точек по частоте];
///     int      CRC;
/// 3.d. Таблица крутизны 2 ЦАП канала I
///      DACNumResp векторов + CRC:
///
///     USHORT   DACPower2[N точек по частоте];
///     int      CRC;
///***** Введено в 3 версии
/// 3.e. Таблица крутизны 3 ЦАП на последней ступени DACSteps[DACNumResp-1] для канала I
///      1 вектор + CRC:
///
///     USHORT   DACPower3[N точек по частоте];
///     int      CRC;
/// 4. Данные пунктов 3.a - 3.e для канала II.


///
/// \brief The TR5048RcvResponseHeader struct
///        Таблица частотного отклика приемников (not used)
///
struct TR5048RcvResponseHeader
{
    quint16   Version;           // Версия
    quint16   FrequencyPlan;     // Код частотного плана
    quint32      MaxFrequency;   // MaxFrequency in MHz
    bool     PortValid[2];       // Приемник калиброван
    //QDateTime DateTime[2];
    char     CalDate[2][12];    // Дата калибровки
    char     CalTime[2][10];    // Время калибровки
    quint16   CalTemper[2];      // Темрература калибровки
    //  int      CRC;               // Check summ
    quint8* RawData(){return reinterpret_cast<quint8*>(this);}
    QDateTime CalDateTime(int port);
};
/*
struct PowerTableSegment
{
    double  start;
    double  stop;
    int     points;
    double Step(){ return (stop - start) / (points - 1); }
};
*/
using PowerTableSegmentsType = std::map<quint16, std::vector<PowerTableSegment>>;
//static const PowerTableSegmentsType TR5048PowerTableSegments;

#pragma pack(pop)

///////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////

///
/// \brief The TR5048FactoryTermsCalibration class
///
class TR5048FactoryTermsCalibration : public FactoryTermsCalibration
{
public:
    TR5048FactoryTermsCalibration(const VnaCmd& cmd);

    // FactoryCalibration interface
    virtual void Load(QByteArray &data) override{ Q_UNUSED(data) }
    virtual void Save(QByteArray &data);

    void Load(quint32 addr);
private:

    struct TR5048TermsPoint
    {
        std::complex<float> _er;//{ float re; float im;}
        std::complex<float> _ed;//struct Ed{ float re; float im;}
        std::complex<float> _es;
        std::complex<float> _et;

        Planar::DSP::Complex Er(){ return Planar::DSP::Complex(static_cast<double>(_er.real()), static_cast<double>(_er.imag()));}
        Planar::DSP::Complex Ed(){ return Planar::DSP::Complex(static_cast<double>(_ed.real()), static_cast<double>(_ed.imag()));}
        Planar::DSP::Complex Es(){ return Planar::DSP::Complex(static_cast<double>(_es.real()), static_cast<double>(_es.imag()));}
        Planar::DSP::Complex Et(){ return Planar::DSP::Complex(static_cast<double>(_et.real()), static_cast<double>(_et.imag()));}
    };

    TR5048RcvResponseHeader _header;

    vector<double>          _termsFreqPlan;             ///< Частотный план
    //CalHeader _header;
}; // class TR5048FactoryTermsCalibration



///
/// \brief The TR5048FactoryPowerCalibration class
///
class TR5048FactoryPowerCalibration : public FactoryPowerCalibration
{
public:

    struct PowerCodes
    {
        int     AttCode; ///< attenuator code
        quint16 StepAtt;
        quint16 DacCode; ///< DAC code
        PowerCodes() = default;
        PowerCodes(quint16 step, quint16 dac)
            : AttCode(step), StepAtt(step), DacCode(dac) {}
        int EncodedAtt() { return StepAtt << 8; }
        int EncodedDac() { return ~(Planar::DSP::EnsureRange((int)DacCode, 0, 4095)); }
    };

    TR5048FactoryPowerCalibration(const VnaCmd& cmd);
    TR5048FactoryPowerCalibration(const VnaCmd& cmd, Planar::VNA::CalibrationRange* range);

    // FactoryCalibration interface
    virtual void Load(QByteArray &data);
    virtual void Save(QByteArray &data);

    virtual Complex R(double frequency);

    PowerCodes GetPowerCodes(double frequency, double power);

private:
    static double FindDACCode(double P, int X1, double P1, int X2, double P2);
    static double FindDACCode(double p0, vector<double> &x, vector<double> &p);
    static double InterpolBetweenDAC(int Step, int Step0, int Step1, double Val0, double Val1);
    static double GetInterpolFactors(double f, const vector<double> &FreqPlan, size_t &i0, size_t &i1);

    int GetDACCode(int SPort, int i0, int i1, double FreqFactor, double dP, int Step);

    bool ReadAndCheckLogMag(quint32 &addr, vector<qint16> &buffer, size_t length, vector<double> &dst);

    static const int DACRefCode;
    static const int DACMinCode;
    static const int DACMaxCode;

    static const int MiddleDACIndex;
    static const int ControlCodesCount;
    static const int ControlDACCodes[];
    static const int MiddleAttIndex;
    static const int AttenuatorLevels[];
    static const int AttenuatorCodes[];

//    struct PowerTableSegment
//    {
//        double  start;
//        double  stop;
//        int     points;
//        double Step(){ return (stop - start) / (points - 1); }
//    };
//    using PowerTableSegmentsType = std::map<quint16, std::vector<PowerTableSegment>>;
//    static const PowerTableSegmentsType _powerTableSegments;

    TR5048PowerTableHeader  _header;
    TR5048PowerPortHeader   _portHeader[PORT_NUMBER];
    //TR5048RcvResponseHeader _rcvHeader[PORT_NUMBER];  ///< not used

    vector<quint16>         _dacStep;                   ///< Ступени на которых сняты хар. плавного
    vector<double>          _powerFreqPlan;             ///< Частотный план
    vector<double>          _rCorr[PORT_NUMBER];
    vector<vector<double>>  _powerStep[PORT_NUMBER];
    vector<vector<double>>  _fp1[PORT_NUMBER];          ///< Мощность 1 при измерении крутизны ЦАП
    vector<vector<double>>  _fp2[PORT_NUMBER];          ///< Мощность 2 при измерении крутизны ЦАП
    vector<vector<double>>  _fp3[PORT_NUMBER];          ///< Мощность 3 при измерении крутизны ЦАП
}; // class TR5048FactoryPowerCalibration



///
/// \brief The TR5048FactoryCalibrationBundle class
///
class TR5048FactoryCalibrationBundle : public FactoryCalibrationBundle
{
public:
    TR5048FactoryCalibrationBundle(const VnaCmd& cmd);

    // FactoryCalibration interface
    virtual void Load();
    virtual void Save();

    // FactoryCalibrationBundle interface
protected:
    virtual FactoryTermsCalibration *CreateFactoryTermsCalibratio();
    virtual FactoryPowerCalibration *CreateFactoryPowerCalibratio();

private:
    TR5048Header _header;
    quint32      _crc;
}; // class TR5048FactoryCalibrationBundle

} //namespace Hardware;
#endif // TR5048FACTORYCALIBRATION_H
