#ifndef TR5048TYPEDEF_H
#define TR5048TYPEDEF_H

#include "TR5048Synt.h"

namespace Hardware
{


#pragma pack(push, 1)
//
// Аппаратно зависимая структура, содержащая данные пресета TR5048
//
class TR5048Preset
{
public:
    TR5048Preset():Delay5us(0), ActivePort(0), Reserved(0), IFBW(0), PortSwDelay5us(0), TauArm(0){}

    unsigned short Delay5us;
    unsigned char ActivePort;              // номер стимулирующего порта
    unsigned char Reserved;                // = 0
    unsigned short IFBW;                   // Полоса фильтра = М*10^p.   М = {10, 15, 20, 30, 40, 50, 70}.  P = {-1, 0, 1, 2, 3}.  Байт 0 – содержит М, байт 1 – содержит P.
    unsigned short PortSwDelay5us;         // Задержка переключения порта
    unsigned char TauArm;                  // Постоянная времени цепи АРМ

    bool operator ==(TR5048Preset const & p)
    {
        return (Delay5us == p.Delay5us) &&
               (ActivePort == p.ActivePort) &&
               (IFBW == p.IFBW) &&
               (PortSwDelay5us == p.PortSwDelay5us) &&
               (TauArm == p.TauArm);
    }

    bool operator !=(TR5048Preset const & p)
    {
        return (Delay5us != p.Delay5us) ||
               (ActivePort != p.ActivePort) ||
               (IFBW != p.IFBW) ||
               (PortSwDelay5us != p.PortSwDelay5us) ||
               (TauArm != p.TauArm);
    }

    bool Update(unsigned short delay, unsigned char activePort, unsigned short ifbw, unsigned short portSwDelay, char tauArm)
    {
        bool changed = false;
        if(Delay5us != delay)
        {
            Delay5us = delay;
            changed = true;
        }

        if(ActivePort != activePort)
        {
            ActivePort = activePort;
            changed = true;
        }

        if(IFBW != ifbw)
        {
            IFBW = ifbw;
            changed = true;
        }

        if(PortSwDelay5us != portSwDelay)
        {
            PortSwDelay5us = portSwDelay;
            changed = true;
        }

        if(TauArm != tauArm)
        {
            TauArm = tauArm;
            changed = true;
        }

        return changed;
    }
};


#pragma pack(pop)
}

#endif // TR5048TYPEDEF_H
