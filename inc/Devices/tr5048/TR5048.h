#ifndef TR5048_H
#define TR5048_H

#include "VnaBase.h"

namespace Hardware
{

class TR5048 : public VnaBase
{
public:
    TR5048(VnaDevicePool *pool, DeviceId id);
    ~TR5048(){}

    // VnaBase override
    QString GetSerial();
    bool SetSerial(QString serial);
protected:
    Binder *CreateBinder(ScanRangeBase *range);             // Наследник должен переопределить этот метод и создавать свой Binder
    VnaCmd* CreateCmd();
    IPower *CreatePower();                                  // Управление мощностью
    virtual FactoryCalibrationBundle *CreateFactoryCalibrations();
    bool FwLoad();
    bool InitKeyAndSerial();
};

}
#endif // TR5048_H

