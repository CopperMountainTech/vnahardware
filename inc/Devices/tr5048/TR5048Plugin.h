#ifndef TR5048PLAGIN_H
#define TR5048PLAGIN_H

#include <QtCore>

#include "IPlugin.h"

namespace Hardware
{
class TR5048Plugin : public QObject, public IPlugin
{
    Q_OBJECT
    Q_INTERFACES(Hardware::IPlugin)
    Q_PLUGIN_METADATA(IID "PLANAR.HW.TR5048Plagin.Metadata" FILE "TR5048.json")

public:
    Q_INVOKABLE TR5048Plugin(QObject *parent = 0);
    ~TR5048Plugin();

    DevicePool *Instance(const QJsonObject &jsonDeviceObj);
};

}


#endif // TR5048PLAGIN_H


