#ifndef TR5048POWER_H
#define TR5048POWER_H

#include "Power.h"

#include "TR5048Cmd.h"

using namespace Hardware;

class TR5048Power : PowerBase
{
public:
    TR5048Power(TR5048Cmd *cmd);
    void OutProcessing(vector<unsigned char> &);
};

#endif // TR5048POWER_H
