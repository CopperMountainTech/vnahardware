#ifndef TR5048POOL_H
#define TR5048POOL_H

#include <QObject>

#include "VnaDevicePool.h"

using namespace Hardware;

namespace Hardware
{

class TR5048Pool : public VnaDevicePool
{
public:
    TR5048Pool(const QJsonObject &jsonDeviceObj);

    // DevicePool override
    virtual DeviceBase *CreateDevice(DeviceId deviceId);


protected:
};

}

#endif // DEVICECONTAINER_H
