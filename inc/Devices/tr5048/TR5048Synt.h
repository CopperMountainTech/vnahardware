#ifndef TR5048SYNT_H
#define TR5048SYNT_H

#include <inttypes.h>

#include "SynthCommon.h"

using namespace std;

using namespace Hardware;


class TSynt5048;
/*
struct PLL_DIVIDERS
{
    int R, N, F, M, D;
    PLL_DIVIDERS(int r, int n, int f, int m, int d) : R(r), N(n), F(f), M(m), D(d) { }
    PLL_DIVIDERS(int r, int n) : R(r), N(n) { }
    PLL_DIVIDERS() { }

};
*/


//---------------------------------------------------------------------------
class TSyntCh
{
public:

    //  TSyntCh(__int64 D, TDividerPair P, bool HighOct, bool HalfOct, bool DownMixEnable, bool Tau, bool DownMixSelect, char Cur, short Dly, int Multiplier, bool HalfOct2, bool SwMixSI);
      TSyntCh() : DDSFTW(0), Flags(0), CPCurr(0), Delay(0) { }
      TSyntCh(int NFRAC, PLL_DIVIDERS Div, int LO_DIV_SEL, int PLL_DIV_SEL, int P, int D1_3, bool Tau, bool Tau20kHz, bool DownMixSelect, char Cur, bool AlternateIF);


      void* GetDDSFTWAddr() const;
      PLL_DIVIDERS Div;
      short        Delay;         // для 5048 дополнительное поле флагов: 16 - DownMixSelect, 8 - Tau, 1 - Alternate IF
      uint64_t     DDSFTW;        // для 5048 NFRAC (32 бита)
      char         Flags;
      char         CPCurr;

      bool  GetHighOctave() const;
      bool  GetHalfOctave() const;
      bool  GetDownMixEnable() const;
      bool  GetDownMixSelect() const;
      int   GetMultiplier() const;

      // 5048
      int   GetPLL_DIV_SEL() const;
      int   GetLO_DIV_SEL() const;
      int   GetPRSC() const;
      int   GetNFRAC() const;
      bool  GetDownMixEnable5048() const;
      bool  GetDownMixSelect5048() const;
      bool  GetTau5048() const;
      bool  GetTau5048_20kHz() const;
      char  GetFlags2() const;

};

struct TDividers
{
//    TDividerTetra Sign, Heter;
//    TDiv_RFFC507x CA_Sign, CA_Heter;
    TSyntCh       Sign5048, Heter5048;

//    TDividers(TDividerTetra s, TDividerTetra h) : Sign(s), Heter(h) { }
//    TDividers(TDiv_RFFC507x s, TDiv_RFFC507x h) : CA_Sign(s), CA_Heter(h) { }
    TDividers(TSyntCh s, TSyntCh h) : Sign5048(s), Heter5048(h) { }
    TDividers() { }
};


class TSynthChannel                        // Signal and Heterodyne
{
public:
    TSynthChannel(TSynt5048* Owner, bool IsHeterodyne);

    bool SynthesizePoint(double Freq, TSyntCh& Result, const double SignGrid, const double HetrGrid, const bool begin);

    bool          Success;
    double        UpAccuracy;
    double        DownAccuracy;

private:
    TSynt5048*    FOwner;

    int           FR, FNint;
    bool          FBest;
    int           FNFrac;
    bool          FIsHeterodyne;
};



class TSynt5048
{

public:		// User declarations
    TSynt5048();
    ~TSynt5048();
    bool      GetSignalDivider(double Freq, TSyntCh& Result, double SignGrid, double HetrGrid);
    bool      GetNextSignalDivider(double Freq, TSyntCh& Result, double SignGrid, double HetrGrid);
    bool      GetHeterodyneDivider(double Freq, TSyntCh& Result, double SignGrid, double HetrGrid);
    void      Preset();
    double    CalcFrequency(const int R, const int NINT, const int NFRAC, const int PLL_DIV_SEL, const int LO_DIV_SEL, const bool DownMix, const bool DownMixSelectLO2) const;
    bool      DetectNeedle(const double f, double LO, bool agressive);
    void      SetRelaxMode(bool Value); // для FOM в версии 2 (в версии 1 не изменяет)

    double    IFAccuracy;
    int       RDivMin;
    int       RDivMax;
    double    DownMixerLO1;
    double    DownMixerLO2;
    double    DownMixerBand;
    double    GetFrequency(const TSyntCh Div) const;
    unsigned short    GetPortSwitchDelay();
    unsigned short    GetDelay1();
    unsigned short    GetDelay2();
    unsigned short    GetDelay3();
    double    FilterSplit1;
    double    FilterSplit2;
    double    MinStep;
    double    AverSyntDelay;
    double    FirstIF;
    double    MinFrequency;
    double    MaxFrequency;
    double    MinPower;
    double    MaxPower;
    double    DefaultPower;
    unsigned int  ROM_Size;

    void SetStepUpAccuracy(const double accuracy)
        { FSignal->UpAccuracy = accuracy; }

    void SetStepDownAccuracy(const double accuracy)
        { FSignal->DownAccuracy = accuracy; }

private:
    double        FDelay1;
    double        FDelay2;
    double        FDelay3;
    double        FPortSwitchDelay;


    int           FRegister1;
    int           FRegister2;
    int           FRegister4;
    int           FRegister5;
    int           FRegister6;

    double        FSecondIF;

    TSynthChannel *FSignal;
    TSynthChannel *FHeterodyne;

    void     SetDefaultR1();
    void     SetDefaultR2();
    void     SetDefaultR4();
    void     SetDefaultR5();
    void     SetDefaultR6();
    void     SetDefaultGen();
    void     SetDefaults();


    void     CheckSafety();



    double GetStepUpAccuracy() const
        { return FSignal->UpAccuracy; }

    double GetStepDownAccuracy() const
        { return FSignal->DownAccuracy; }


};


extern TSynt5048 * SyntTR5048Create();
extern void SyntTR5048Destroy();

#endif // TR5048SYNT_H
