#ifndef TR5048SYNTHESIZER_H
#define TR5048SYNTHESIZER_H

#include <vector>

#include "Synthesizer.h"
//#include "Protocol.h"

#include "TR5048Cmd.h"

#include "TR5048TypeDef.h"

#include "TR5048Synt.h"

using namespace std;

namespace Hardware
{

/*
//---------------------------------------------------------------------------
#pragma pack(push, 1)

struct CMult
{
   double Mult;
   double F;
   double M;

   friend bool operator >(const CMult &x, const CMult &y) {
      return (x.Mult > y.Mult);
   }
   friend bool operator <(const CMult &x, const CMult &y) {
      return (x.Mult < y.Mult);
   }
   friend bool operator ==(const CMult &x, const CMult &y) {
      return (x.Mult == y.Mult);
   }
};

struct CAdr
{
   int Adr;
   int Count;
};

#pragma pack(pop)

//---------------------------------------------------------------------------

typedef vector<CMult> TCMult;
typedef vector<CAdr> TCAdr;
typedef vector<bool> TBoolField;

//---------------------------------------------------------------------------
class TMultiplierBase
{
   public:
      TMultiplierBase();
      ~TMultiplierBase();

      void Init(double _Mmin, double _Mmax, double _MaxMultiplier, double _MinMultiplier);
      void Unload();

      bool pInit(){ return FInit; }
      int Size() { return  FSize; }
      double MaxMultiplier(){ return FMaxMultiplier; }
      double MinMultiplier() { return FMinMultiplier; }

      CMult C(int index) {return FC[index];}

private:
   TCMult FC;

   bool FInit;
   int FSize;
   double Mmin;
   double Mmax;
   double FMaxMultiplier;
   double FMinMultiplier;
   bool CheckFraction(double Multiplier, double Delta, double *FArray, int ASize);
};



//---------------------------------------------------------------------------
class TAddressBase
{
   private:

      TMultiplierBase* FMultiplier;   // вынесем массив С за пределы класса... пусть будет общим

      bool FInit;

      TCAdr  FAddress;
      double FStepFreq; // шаг для определения i-го адреса
      double FStartFreq;
      double FStopFreq;

      double CompareFreq;



   public:
      TAddressBase();
      ~TAddressBase();

      void Init(TMultiplierBase* Multiplier, double _CompareFreq);

      void Unload();

      bool pInit() { return FInit; }

      int Address(int Index) { return FAddress[Index].Adr; }
      int Count(int Index) { return FAddress[Index].Count; }

      double StepFreq() { return FStepFreq; }
      double StartFreq() { return FStartFreq; }
      double StopFreq() { return FStopFreq; }
};


//---------------------------------------------------------------------------
class TSynthesizerBase
{
public:		// User declarations
        TSynthesizerBase();
        ~TSynthesizerBase();

        TMultiplierBase* Multiplier;
        TAddressBase*    Address;

};
*/

/*
class TFS_TR5048
{
   public:
      TFS_TR5048();
       ~TFS_TR5048();

      void Init(TMultiplierBase* Multiplier, TAddressBase* Address, double _CompareFreq,  double _R, double _IntermediateFreq);

      bool SearchFactors(double _TargetFreq, double FreqShift, double _Accuracy, int NextVariant);

      bool pInit() { return FInit; }

      int pR() { return SaveR; }
      int pN1() { return  SaveN1;}
      int pF1() { return SaveF1; }
      int pM1() { return SaveM1; }
      int pN2() { return SaveN2; }
      int pF2() { return SaveF2; }
      int pM2() { return SaveM2; }


    private:
       TMultiplierBase* FMultiplier;
       TAddressBase* FAddress;
       bool FInit;

       double R;
       double CompareFreq;
       double N1;
       double C1StartFreq;
       double TargetFreq;
       double N2;
       double Accuracy;

       double Compare1;
       double Compare2;
       double MinError;

       int SaveR, SaveN1, SaveF1, SaveM1, SaveN2, SaveF2, SaveM2;

       int LastField;
       int LastZone;

       TBoolField PoorField1;
       TBoolField PoorField2;

       int Divider;

       bool FindInZone(int i);

       double IntermediateFreq;
       bool DetectNeedle1(double WBSFreq, double NBSFreq);
       bool DetectNeedle2(double WBSFreq, double NBSFreq);
       bool DetectNeedle3(double WBSFreq, double NBSFreq);
};
*/




class TR5048FactoryPowerCalibration;

class TR5048Synthesizer : public SynthesizerBase
{
public:
    TR5048Synthesizer(TR5048Cmd *cmd, ScanRangeBase *range, TR5048FactoryPowerCalibration* calibration);
    ~TR5048Synthesizer();
protected:
    // SynthesizerBase
    void OnRangeChanged();                                          // распределение памяти под данные
    bool InProcessing(MeasuringResult &result, unsigned char *buf);
    bool SynthesizePoint(ScanPointBase &point, int pointIdx);    // Синтез одной частотной точки.
    void AppendPoint(vector<unsigned char> &outBuf, ScanPointBase &point, int pointIdx);
    void AppendBackScan(vector<unsigned char> &outBuf, ScanPointBase &point);

private:
    TR5048Cmd *_cmd;
    TR5048FactoryPowerCalibration* _calibration;
    QVector<TR5048Pll> _TR5048Pll;              // частотные точки (делители синтезаторов) [points]

    TR5048Preset _preset;
    int _dacARM;
    unsigned char _mixRF;                                // состояние битов управления сигнльного канала
    unsigned char _mixLO;                                // состояние битов управления гетеродинного канала

    TSynt5048 *_synth;                                   // синтезатор

    bool DetectCrossNoise(double SW, double SN, double GW, double GN);

    bool LeapSynthez(const double Sign, const double Heter, const double SignLowerAccuracy,const double SignUpperAccuracy, double& SignFact, TDividers& Div);
    bool SynthezPoint(const double Sign, const double Heter, double SignDownAccuracy, double SignUpAccuracy, double& SignFact, TDividers& Div);
    void AppendPreset(vector<unsigned char> &outBuf, TR5048Pll pll, Unit ifbw, bool force);
};

}

#endif // TR5048SYNTHESIZER_H
