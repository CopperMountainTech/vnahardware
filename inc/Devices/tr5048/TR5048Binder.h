#ifndef TR5048BINDER_H
#define TR5048BINDER_H

#include "Binder.h"

namespace Hardware {
    class TR5048FactoryPowerCalibration;
}

using namespace Hardware;

class TR5048Binder : public Binder
{
public:
    TR5048Binder(VnaCmd *cmd, ScanRangeBase *range, TR5048FactoryPowerCalibration* calibration);

protected:
    // IBinder
     ISynthesizer* CreateSynthesizer();
private:
    TR5048FactoryPowerCalibration* _factoryCalibration;

};

#endif // TR5048BINDER_H
