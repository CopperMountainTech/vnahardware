#ifndef S50XXDEVICE_H
#define S50XXDEVICE_H

#include <QtCore>

#include "IPlugin.h"

namespace Hardware
{
class S50xxPlugin : public QObject, public IPlugin
{
    Q_OBJECT
    Q_INTERFACES(Hardware::IPlugin)

#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "PLANAR.HW.S50XXPlagin.Metadata" FILE "S50xx.json")
#endif // QT_VERSION >= 0x050000

public:
    Q_INVOKABLE S50xxPlugin(QObject *parent = 0);
    ~S50xxPlugin();

     DevicePool *Instance(const QJsonObject &jsonDeviceObj);
};

}


#endif // S50XXDEVICE_H
