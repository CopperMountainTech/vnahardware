#ifndef S50XXPOOL_H
#define S50XXPOOL_H

#include <QObject>

#include "VnaDevicePool.h"

using namespace Hardware;

namespace Hardware
{

class S50xxPool : public VnaDevicePool
{
public:
    S50xxPool(const QJsonObject &jsonDeviceObj);

    // DevicePool override
    virtual DeviceBase *CreateDevice(DeviceId deviceId);


protected:
};

}

#endif // DEVICECONTAINER_H
