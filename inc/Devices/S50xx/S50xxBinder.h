#ifndef S50XBINDER_H
#define S50XBINDER_H

#include "Binder.h"


namespace Hardware
{

class S50xxBinder : public Binder
{
public:
    S50xxBinder(VnaCmd *cmd, ScanRangeBase *range);

protected:
// IBinder
    ISynthesizer *CreateSynthesizer();
};

}

#endif // S50XBINDER_H
