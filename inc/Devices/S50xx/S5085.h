#ifndef VNAS5085_H
#define VNAS5085_H

#include "VnaBase.h"

namespace Hardware
{

//
// Функционал S5085, независимый от физического интерфейса
//
class S5085 : public VnaBase
{
public:
    S5085(VnaDevicePool *pool, DeviceId id);
    ~S5085(){}

    // VnaBase override
    QString GetSerial();
    bool SetSerial(QString serial);

protected:
    // VnaBase override
    Binder *CreateBinder(ScanRangeBase *range);
    bool FwLoad();
    bool InitKeyAndSerial();
    FactoryCalibrationBundle *CreateFactoryCalibrations();
};

}

#endif // VNAS5085_H
