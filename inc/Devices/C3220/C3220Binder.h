#ifndef C3220BINDER_H
#define C3220BINDER_H

//#include "Binder.h"
#include "../Hardware/Units/Vna/Binder.h"

namespace Hardware {
    class C3220FactoryPowerCalibration;
}

using namespace Hardware;

class C3220Binder : public Binder
{
public:
    C3220Binder(VnaCmd *cmd, ScanRangeBase *range, C3220FactoryPowerCalibration* calibration);    

protected:
    ISynthesizer* CreateSynthesizer();
    //IImpulseGenerators* CreateImpulseGenerators();
private:
    C3220FactoryPowerCalibration* _factoryCalibration;

    //IImpulseGenerators *_impulseGenerators;
};

#endif // C3220BINDER_H
