#ifndef C3220_H
#define C3220_H

#include "VnaBase.h"
#include "VnaDevicePool.h"
#include "C3220ImpulseGenerators.h"
#include "C3220IntegrateTrigger.h"
#include "C3220RefOsc.h"

using namespace Hardware;

class C3220 : public VnaBase
{
public:
    C3220(VnaDevicePool *pool, DeviceId id);
    ~C3220(){}

    // VnaBase
    Binder *CreateBinder(ScanRangeBase *range);             // Наследник должен переопределить этот метод и создавать свой Binder
    VnaCmd* CreateCmd();
    ITemperature* CreateTemperature();
    IImpulseGenerators *CreateImpulseGenerators();

    IExtSyncIn* CreateExtSyncIn();
    IExtSyncOut* CreateExtSyncOut();

    bool SetSerial(QString serial);

    bool FlashWrite(int addr, unsigned char *buf, int size);

    IIntegrateTrigger* IntegrateTrigger();
    IImpulseGenerators* ImpulseGenerators();

    IRefOsc *CreateRefOsc();

    void Reset();

protected:
    IIntegrateTrigger* _integrateTrigger;                    // Настройки внутреннего триггера
    IImpulseGenerators *_impulseGenerators;

    bool FwLoad();
    bool InitKeyAndSerial();
    void CreateComponents();
    void ControlOutProcessing();
    virtual FactoryCalibrationBundle *CreateFactoryCalibrations();
};

#endif // C3220_H
