#ifndef C3220POOL_H
#define C3220POOL_H

#include <QObject>

#include "VnaDevicePool.h"

using namespace Hardware;

namespace Hardware
{

class C3220Pool : public VnaDevicePool
{
public:
    C3220Pool(const QJsonObject &jsonDeviceObj);

    // DevicePool override
    virtual DeviceBase *CreateDevice(DeviceId deviceId);
};

}

#endif // C3220POOL_H
