#ifndef C3220TYPEDEF_H
#define C3220TYPEDEF_H

#include "SynthCommon.h"
#include <QVector>
#include <cstring>
#include "VnaHw.h"

namespace Hardware
{

#pragma pack(push, 1)
//
// Аппаратно зависимая структура, содержащая данные пресета C3220
//

class C3220Preset {
public:
    C3220Preset(): Delay10ns(0), ActivePort(0), Reserved(0), IFBW(0), PortSwDelay10ns(0), TypeWindow(0),
                   AnalogFilt(0), CountMeas(0), DelayFilt(0), IF_FTW(0) {}
    unsigned int   Delay10ns;          // код задержки 10 нс
    unsigned char  ActivePort;         // номер стимулирующего порта
    unsigned char  Reserved;           // = 0
    unsigned short IFBW;               // Полоса фильтра = М*10^p.   М = {10, 15, 20, 30, 40, 50, 70}.  P = {-1, 0, 1, 2, 3}.
                                       // Байт 0 – содержит М, байт 1 – содержит P.
    unsigned int   PortSwDelay10ns;    // задержка переключения порта 10 нс
    unsigned char  TypeWindow;         // выбор фильтра:
                                       //   0 – без фильтра (режим осциллографа)
                                       //   1 – прямоугольное окно
                                       //   2 – синус – окно
    unsigned char  AnalogFilt;         // вкл./выкл. (1/0) аналоговый фильтр АЦП
    unsigned int   CountMeas;          // число последовательных измерений
    unsigned int   DelayFilt;          // задержка фильтра

    unsigned int   IF_FTW;             // для цифрового генератора ПЧ ((uint32)FTW = 2^32*(необходимая частота,  МГц)/100)

    bool operator ==(C3220Preset const & p)
    {
        return ( Delay10ns  == p.Delay10ns  ) && ( ActivePort      == p.ActivePort      ) &&
               ( IFBW       == p.IFBW       ) && ( PortSwDelay10ns == p.PortSwDelay10ns ) &&
               ( TypeWindow == p.TypeWindow ) && ( AnalogFilt      == p.AnalogFilt      ) &&
               ( CountMeas  == p.CountMeas  ) && ( DelayFilt       == p.DelayFilt       ) &&
               ( IF_FTW     == p.IF_FTW     );
    }

    bool operator !=(C3220Preset const & p)
    {
        return ( Delay10ns  != p.Delay10ns  ) || ( ActivePort      != p.ActivePort      ) ||
               ( IFBW       != p.IFBW       ) || ( PortSwDelay10ns != p.PortSwDelay10ns ) ||
               ( TypeWindow != p.TypeWindow ) || ( AnalogFilt      != p.AnalogFilt      ) ||
               ( CountMeas  != p.CountMeas  ) || ( DelayFilt       != p.DelayFilt       ) ||
               ( IF_FTW     != p.IF_FTW     );
    }

    bool Update(unsigned int delay, unsigned char activePort, unsigned short ifbw, unsigned short portSwDelay,
                unsigned char typeWindow, unsigned char analogFilt, unsigned int countMeas, unsigned int delayFilt, unsigned int  if_FTW )
    {
        bool changed = false;

        if( Delay10ns       != delay       ) { Delay10ns       = delay;       changed = true; }
        if( ActivePort      != activePort  ) { ActivePort      = activePort;  changed = true; }
        if( IFBW            != ifbw        ) { IFBW            = ifbw;        changed = true; }
        if( PortSwDelay10ns != portSwDelay ) { PortSwDelay10ns = portSwDelay; changed = true; }
        if( TypeWindow      != typeWindow  ) { TypeWindow      = typeWindow;  changed = true; }
        if( AnalogFilt      != analogFilt  ) { AnalogFilt      = analogFilt;  changed = true; }
        if( CountMeas       != countMeas   ) { CountMeas       = countMeas;   changed = true; }
        if( DelayFilt       != delayFilt   ) { DelayFilt       = delayFilt;   changed = true; }
        if( IF_FTW          != if_FTW      ) { IF_FTW          = if_FTW;      changed = true; }

        return changed;
    }

};

//
// возможна в дальнейшем потребует расширения, либо переработки
//
class C3220ImpulsePreset32 {
public:
    C3220ImpulsePreset32() : CurrentGenerator(0x00), Reserved(0x00), Mode(0x00), Polarity(0x00)
    {
        std::memset( Variant, 0x00, sizeof(Variant) );
    }

    C3220ImpulsePreset32( const ImpulseProp &prop )
    {
        if( !prop.Enable )
        {
            if( prop.Type == tgGating || prop.Mode == mgBrust || prop.Mode == mgContinue )
                set( prop.Type, (char)0x00, (char)prop.Polarity, prop.Brust );
            else
                set( prop.Type, (char)0x00, (char)prop.Polarity, prop.Temps );
        }
        else
        {
            if( prop.Type == tgGating || prop.Mode == mgBrust || prop.Mode == mgContinue )
            {
                if( prop.Type == tgGating )
                {
                    set( prop.Type, prop.Enable, (char)prop.Polarity, prop.Brust );
                }
                else
                    set( prop.Type, (char)prop.Mode, (char)prop.Polarity, prop.Brust );
            }
            else
            {
                set( prop.Type, (char)prop.Mode, (char)prop.Polarity, prop.Temps );
            }
        }
    }

    void set( char currentGenerator, char mode, char polarity, const QVector<ImpulsTemplate> &delays )
    {
        ///Q_ASSERT( delays.size() <= 3 );
        Q_ASSERT( currentGenerator != 0x05 && mode != 0x05  && mode != 0x06 ); // delays не задаются для gating и для Burst режима

        CurrentGenerator = currentGenerator;
        Mode             = mode;
        Polarity         = polarity;
        std::memcpy( Variant, delays.data(), std::min( sizeof(ImpulsTemplate)*delays.size(), sizeof(Variant) ) );
    }

    void set( char currentGenerator, char mode, char polarity, const BrustConfig &brust )
    {
        // Выбран gating, либо включен Burst/Continue режим, либо выключен
        Q_ASSERT( currentGenerator == 0x05 || mode == 0x05 || mode == 0x06 || mode == 0x00 );

        CurrentGenerator = currentGenerator;
        Mode             = mode;
        Polarity         = polarity;
        std::memcpy( Variant, &brust, std::min( size_t(Variant), sizeof(BrustConfig) ) );
    }

    char CurrentGenerator;
    char Reserved; // = 0
    char Mode;
    char Polarity;
    char Variant[24];
};

//
// FFEF – не помещающаяся  в FFFC конфигурация фильтра
//
class C3220DSPPreset {
public:
    C3220DSPPreset() : Period(0), Count(0), DelayCH1(0), DelayCH2(0), DelayCH3(0), DelayCH4(0) {}
    int Period;   // период измерений фильтра
    int Count;    // число отсчетов пользовательского
    int DelayCH1;
    int DelayCH2;
    int DelayCH3;
    int DelayCH4;
};

//
// Аппаратно зависимая структура, содержащая данные пресета C1220 (size 10)
//
class C1220Preset
{
public:
    C1220Preset(): Delay100ns(0), ActivePort(0), IFBW(0), PortSwDelay100ns(0) {}
    unsigned int   Delay100ns;          // код задержки 100 нс
    unsigned char  ActivePort;          // номер стимулирующего порта
    unsigned char  Reserved;            // = 0
    unsigned short IFBW;                // Полоса фильтра = М*10^p.   М = {10, 15, 20, 30, 40, 50, 70}.  P = {-1, 0, 1, 2, 3}.
                                        // Байт 0 – содержит М, байт 1 – содержит P.
    unsigned short PortSwDelay100ns;    // задержка переключения порта 100 нс

    bool operator ==(C1220Preset const & p)
    {
        return (Delay100ns == p.Delay100ns) &&
                (ActivePort == p.ActivePort) &&
                (IFBW == p.IFBW) &&
                (PortSwDelay100ns == p.PortSwDelay100ns);
    }

    bool operator !=(C1220Preset const & p)
    {
        return (Delay100ns != p.Delay100ns) ||
               (ActivePort != p.ActivePort) ||
               (IFBW != p.IFBW) ||
               (PortSwDelay100ns != p.PortSwDelay100ns);
    }

    bool Update(unsigned int delay, unsigned char activePort, unsigned short ifbw, unsigned short portSwDelay)
    {
        bool changed = false;
        if(Delay100ns != delay)
        {
            Delay100ns = delay;
            changed = true;
        }

        if(ActivePort != activePort)
        {
            ActivePort = activePort;
            changed = true;
        }

        if(IFBW != ifbw)
        {
            IFBW = ifbw;
            changed = true;
        }

        if(PortSwDelay100ns != portSwDelay)
        {
            PortSwDelay100ns = portSwDelay;
            changed = true;
        }

        return changed;
    }
};


class TSyntCh {
public:    
    TSyntCh() {}
    TSyntCh(    long long FTW,
                int       Nint,// !
                int       Div, // !
                unsigned char Cur1, unsigned char Cur2,
                bool Mul2, // !
                bool HalfOkt, bool DownMixEnable, int  Nfilt,
                bool LPF_PLL0, // !
                bool LPF_PLL1, // !
                bool TauHi, bool TauLo,
                bool RF_CF1Band, // !
                bool DownMixSelectLO2, bool AltIF, int  UniDAC )
    {
        _Div  = Div;
        _Mul2 = Mul2;
        _LPF_PLL0 = LPF_PLL0;
        _LPF_PLL1 = LPF_PLL1;
        _RF_CF1Band = RF_CF1Band;

        memcpy(&FData[0], &FTW, 6);
        FData[6] =  Nint;

        int Value = Div;
        int DivRes = 0;
        while( Value ) {
            Value /= 2; DivRes += 1;
        }
        DivRes -= 1;


        FData[7] =  (Cur2 & 31) | ( DivRes << 5) | (AltIF ? 128:0);
        FData[8] =  (Cur1 & 31) | (HalfOkt ? 32:0) | (Mul2 ? 64:0) | (RF_CF1Band ? 128:0) ;
        FData[9] =  (Nfilt & 3) | (DownMixEnable ? 4:0) |
                    (LPF_PLL0 ? 8:0) | (LPF_PLL1 ? 64:0) |
                    (TauHi ? 16:0) | (TauLo ? 32:0)|
                    (DownMixSelectLO2 ? 128:0);
        memcpy(&FData[10], &UniDAC, 2);
    }
    const char* data() const { return FData; }
    int  getNint() const { return FData[ 6 ]; }
    int  getDiv()  const { return _Div; }
    int  getMul2() const { return _Mul2; }
    bool getLPF_PLL0() const { return _LPF_PLL0; }
    bool getLPF_PLL1() const { return _LPF_PLL1; }
    bool getCF1Band() const { return _RF_CF1Band; }
    void setUniDAC( unsigned short UniDAC ) { memcpy(&FData[10], &UniDAC, 2); }
private:
    char FData[12];

    int _Div;
    int _Mul2;
    bool _LPF_PLL0;
    bool _LPF_PLL1;
    bool _RF_CF1Band;
};


//
// Данные точки синтеза
//
class C3220Pll
{
public:
    C3220Pll(){}
    C3220Pll(TSyntCh rf, TSyntCh lo) :RF(rf), LO(lo) { }

    TSyntCh RF;
    TSyntCh LO;

    //unsigned short att; // ступенчатый аттенюатор : 0-й байт 5 бит, 1-й байт 1+1 бит (2 мл. бита)
    //unsigned short dac; // ЦАП фильтра 12-20 ГГц
};

#pragma pack(pop)
}

#endif // C3220TYPEDEF_H
