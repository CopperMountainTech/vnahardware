#ifndef C3220PLAGIN_H
#define C3220PLAGIN_H

#include <QtCore>

#include "IPlugin.h"

namespace Hardware
{
class C3220Plugin : public QObject, public IPlugin
{
    Q_OBJECT
    Q_INTERFACES(Hardware::IPlugin)
    Q_PLUGIN_METADATA(IID "PLANAR.HW.C3220Plagin.Metadata" FILE "C3220.json")


public:
    Q_INVOKABLE C3220Plugin(QObject *parent = 0);
    ~C3220Plugin();

    DevicePool *Instance(const QJsonObject &jsonDeviceObj);
};

}


#endif // C3220PLAGIN_H


