#ifndef C3220IMPULSEGENERATORS_H
#define C3220IMPULSEGENERATORS_H

#include "ImpulseGeneratorsBase.h"
#include "C3220Cmd.h"


using namespace Hardware;


class C3220ImpulseGenerators : public ImpulseGeneratorsBase
{
public:
    C3220ImpulseGenerators(): ImpulseGeneratorsBase() {}
    C3220ImpulseGenerators(C3220Cmd* cmd);

    ImpulseProp *GetProperty( size_t numGen );
    void OutProcessing(vector<unsigned char> &outBuf);
    virtual size_t GetCount() const;

public slots:
    void SetProperty( size_t numGen, const ImpulseProp &iGen );

private:
    int _countGenerators;
    QVector< ImpulseProp > _iGens;
    C3220Cmd *_cmd;
};

#endif // C3220IMPULSEGENERATORS_H
