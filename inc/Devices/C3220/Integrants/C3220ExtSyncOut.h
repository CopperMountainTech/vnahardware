#ifndef C3220EXTSYNCOUT_H
#define C3220EXTSYNCOUT_H

#include "ExtSyncOutEx.h"
#include "C3220Cmd.h"

using namespace Hardware;

class C3220ExtSyncOut : public ExtSyncOutBaseEx
{
public:
    C3220ExtSyncOut(C3220Cmd *cmd);

    void SetEventMode( int index, TriggerEventType event);
    TriggerEventType GetEventMode( int index );

    void SetPolarityEdge( int index, TriggerPolarityType edge);
    TriggerPolarityType GetPolarityEdge( int index );

    void SetPosition( int index, TriggerPositionType pos);
    TriggerPositionType GetPosition( int index );

    void SetDelay(int index, Unit delay);
    Unit GetDelay(int index);

    void Reset();

    void OutProcessing(vector<unsigned char> &outData);
protected:
    C3220Cmd* _cmd;
    SyncProperty _additional[ 3 ]; // Настройки выходов дополнительных триггеров
};

#endif // C3220EXTSYNCOUT_H
