#ifndef C3220POWER_H
#define C3220POWER_H

#include "Power.h"

#include "C3220Cmd.h"

using namespace Hardware;

class C3220Power : PowerBase
{
public:
    C3220Power(C3220Cmd *cmd);
    void OutProcessing(vector<unsigned char> &outBuf);
    void Reset();
private:
    C3220Cmd *_cmd;
};

#endif // C3220POWER_H
