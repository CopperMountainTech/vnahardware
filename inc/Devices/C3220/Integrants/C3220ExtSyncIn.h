#ifndef C3220EXTSYNCIN_H
#define C3220EXTSYNCIN_H

#include "ExtSyncInEx.h"
#include "C3220Cmd.h"

using namespace Hardware;

class C3220ExtSyncIn : public ExtSyncInBaseEx
{
public:
    C3220ExtSyncIn(C3220Cmd *cmd);

    void SetEventMode(int index, TriggerEventType event);
    TriggerEventType GetEventMode(int index);

    void SetLevel(TriggerLevelType level);
    void SetLevel(int index, TriggerLevelType level);

    TriggerLevelType GetLevel();
    TriggerLevelType GetLevel(int index);

    void SetCapture(TriggerCapture cap);
    void SetCapture(int index, TriggerCapture cap);

    TriggerCapture GetCapture();
    TriggerCapture GetCapture(int index);

    void SetPolarityEdge(int index, TriggerPolarityType edge);
    TriggerPolarityType GetPolarityEdge(int index);

    void SetPosition(int index, TriggerPositionType pos);
    TriggerPositionType GetPosition(int index);

    void SetDelay(int index, Unit delay);
    Unit GetDelay(int index);

    void OutProcessing(vector<unsigned char> &outData);

    void Reset();

protected:
    C3220Cmd* _cmd;
    SyncProperty _additional[ 3 ]; // Настройки выходов дополнительных триггеров
};


#endif // C3220EXTSYNCIN_H
