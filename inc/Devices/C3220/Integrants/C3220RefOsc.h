#ifndef C3220REFOSC_H
#define C3220REFOSC_H

#include "RefOsc.h"
#include "C3220Cmd.h"

using namespace Hardware;


class C3220RefOsc : public RefOscBase
{
public:
    C3220RefOsc(C3220Cmd *cmd) : RefOscBase(cmd)
    {
        _src = rsInt;
    }
    void OutProcessing(vector<unsigned char> &outBuf)
    {
        // однако пока не работает!
        //if(_propertyChanged)
        {
            //! ((C3220Cmd*)_cmd)->RefOsc(outBuf, _src);
            //_propertyChanged = false;
        }
    }
};

#endif // C3220REFOSC_H
