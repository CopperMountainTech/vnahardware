#ifndef C3220TEMPERATURE_H
#define C3220TEMPERATURE_H

#include "Temperature.h"

#include "C3220Cmd.h"

using namespace Hardware;

class C3220Temperature : public TemperatureBase
{   Q_OBJECT
public:
    C3220Temperature(C3220Cmd *cmd);
    bool InProcessing(unsigned char *buf);
    QVector<double> GetTemperature();
private:
    atomic<double> _temperatureRF;
    atomic<double> _temperatureLO;
};

#endif //C3220TEMPERATURE_H
