#ifndef C3220INTEGRATETRIGGER_H
#define C3220INTEGRATETRIGGER_H

#include "IntegrateTrigger.h"
#include "C3220Cmd.h"

using namespace Hardware;

class C3220IntegrateTrigger : public IntegrateTrigger
{
public:
    C3220IntegrateTrigger(C3220Cmd *cmd);

    void OutProcessing(vector<unsigned char> &outData);
private:
    C3220Cmd* _cmd;
};

#endif // C3220INTEGRATETRIGGER_H
