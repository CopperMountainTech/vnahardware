#ifndef C3220CMD_H
#define C3220CMD_H

#include <vector>

#include "VnaHw.h"
#include "C3220TypeDef.h"
#include "VnaCmdEx.h"

using namespace std;

namespace Hardware
{

class C3220ConfigSyntez;                    // c заголовками проблема однако...

typedef QMap<TypeImpulseGenerator, QPair<SourceTrigger, FrontType> > IntegrateTriggers;

class C3220Cmd : public VnaCmdEx
{
public:
    C3220Cmd(IIO *io);

    void setConfigSyntez( C3220ConfigSyntez* config ) { _config = config; }

    // Override VnaCmd
    bool CheckPipelineStart(unsigned char *buf, int markerId);
    bool CheckMeasure(unsigned char *buf);
    bool CheckBackScan(unsigned char *buf);

    void CmdImpuleGenerator(vector<unsigned char> &ctrlData, int numGen, const ImpulseProp &iGem );

    void PresetImpulse(vector<unsigned char> &outBuf, C3220ImpulsePreset32 preset );        
    void Preset(vector<unsigned char> &outBuf, C3220Preset preset);
    void PresetDSP(vector<unsigned char> &outBuf, C3220DSPPreset preset);
    void Measure(vector<unsigned char> &outBuf, C3220Pll point, int activePort, ushort RF_DAC_FIL, ushort RF_ATT, int pointIdx);
    void BackScan(vector<unsigned char> &outBuf, C3220Pll point, int activePort, ushort dacArm, ushort att);
    //void Temperature(vector<unsigned char> &outBuf);

    bool MeasureParse(MeasuringResult &result, unsigned char *buf, ScanRangeBase *range);
    bool TemperatureParse(double &t, unsigned char *buf)
    {
        Q_UNUSED(t)
        Q_UNUSED(buf)
        return true;
    }
    bool Temperature2Parse(double &tRF, double &tLO, unsigned char *buf);

    void SyncOut(vector<unsigned char> &ctrlData, SyncProperty prop);
    void SyncOutAdditional(vector<unsigned char> &ctrlData, int index, SyncProperty prop);

    void SyncIn(vector<unsigned char> &ctrlData, SyncProperty prop);
    void SyncInAdditional(vector<unsigned char> &ctrlData, int index, SyncProperty prop);

    void IntegrateTrigger(vector<unsigned char> &ctrlData, const IntegrateTriggers &triggers );

    void RefOsc( vector<unsigned char> &ctrlData, ReferenceSourceType src);

    int HardwareVersion();
    bool FlashWrite(int addr, unsigned char *buf, int size);

    bool FpgaWrite(unsigned char *buf, int size);
private:
    int _pointIdx;
    int _error;

    C3220ConfigSyntez* _config;
};

}
#endif // C3220CMD_H
