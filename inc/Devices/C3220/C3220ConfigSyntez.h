#ifndef C3220CONFIGSYNTEZ_H
#define C3220CONFIGSYNTEZ_H

#include <QObject>
#include "C3220FactoryCalibration.h"
#include "ConfigSyntezBase.h"
namespace Hardware
{

#define PORT1 0
#define PORT2 1


class C3220FactoryPowerCalibration;


class C3220ConfigSyntez : public ConfigSyntezBase
{   Q_OBJECT
public:
    C3220ConfigSyntez( int HWVersion, QObject* parent = Q_NULLPTR );
    bool GetAttControlEnabled() const { return _attControl.enable; }
    C3220FactoryPowerCalibration::PowerCodes* GetPowerCodes() const { return _attControl.powerCodes; }
    double GetAltIFDelta() const { return _altIFDelta; }
    uint32_t GetFTW() const { return _FTW; }
    char getAnalogFilter() const { return (char)_parameters[ SyntezParameter::spAnalogFilter ].toInt(); }
    vector<int> GetAttControl() const { return _attControl.toVector(); }    
signals:
    void updateCurrentPort( int port );
    void updateAltIFDelta( double altIFDeltaMhz );
    void updateEnableAttControl( bool enable );

public slots:
    void setAltIFDelta( double altIFDeltaMhz );
    void setEnableAttControl( bool enable );
    void setAttControl( const vector<int> &att );

private:
    void Default();
    struct AttControl {
        AttControl() : powerCodes( new C3220FactoryPowerCalibration::PowerCodes() ) {
            powerCodes->StepAtt = 10;
            powerCodes->AttCode = 0x00;
            powerCodes->DacCode = 4096;
        }
        bool enable;
        C3220FactoryPowerCalibration::PowerCodes* powerCodes;
        vector<int> toVector() const { return { powerCodes->StepAtt,
                                                powerCodes->AttCode,
                                                powerCodes->DacCode }; }
    };
    /// struct Delays {};

    double        _altIFDelta;
    uint32_t      _FTW;
    AttControl    _attControl;
};

}
#endif // C3220CONFIGSYNTEZ_H
