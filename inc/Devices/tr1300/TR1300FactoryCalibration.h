#ifndef TR1300FACTORYCALIBRATION_H
#define TR1300FACTORYCALIBRATION_H

#include <QDateTime>
#include <QString>
#include <QDataStream>

#include "FactoryCalibration.h"
#include "Complex.h"

#define PORT_NUMBER 2

namespace Planar {
namespace VNA {
namespace Hardware {

// используется для заводских калибровок 1300 и для хранения пользовательских калибровок
// структура для хранения заводской калибровки

#pragma pack(push, 1)
///
/// \brief The Calibaration Header struct
///
struct CalHeader
{
    QDateTime   DataTimeStamp; ///< дд.мм.гггг чч:мм:сс - ошибка при использовании других языковых стандартов
    QString     CalKitName;
    double      MinFreq;
    double      MaxFreq;
    int         IFBW;
    int         Points;
    int         SweepMode;
    int         CorrectionType[PORT_NUMBER];
    bool        VectorCorrection[PORT_NUMBER];
    int         Count;
};
QDataStream& operator<<(QDataStream& out, const CalHeader& p);
QDataStream& operator>>(QDataStream& in, CalHeader& p);

///
/// \brief The Error Terms Items struct
///
struct ExItems
{
    double   Frequency;
    Planar::Dsp::Complex64  Er[PORT_NUMBER];
    Planar::Dsp::Complex64  Ed[PORT_NUMBER];
    Planar::Dsp::Complex64  Es[PORT_NUMBER];
    Planar::Dsp::Complex64  Et[PORT_NUMBER];
};
QDataStream& operator<<(QDataStream& out, const ExItems& p);
QDataStream& operator>>(QDataStream& in, ExItems& p);

///
/// \brief The SxData struct
///
struct SxData
{
    Planar::Dsp::Complex64 RawData[PORT_NUMBER];
    Planar::Dsp::Complex64 RefData;
    Planar::Dsp::Complex64 NormData[PORT_NUMBER];
};

///
/// \brief The Power Calibration Header struct
///
struct PowerCalHeader
{
    QDateTime   DataTimeStamp;  ///< дд.мм.гггг чч:мм:сс
    int         Version;        ///< версия
    double      Temperature;    ///< температура при которой проводилась калибровка
    int         Count;          ///< количество точек
};
QDataStream& operator<<(QDataStream& out, const PowerCalHeader& p);
QDataStream& operator>>(QDataStream& in, PowerCalHeader& p);


///
/// \brief Значения мощности для контрольных значений ЦАП
///
struct ControlPower
{
    float Power[11];                ///< меньшему коду соответствует меньшая мощность
};
QDataStream& operator<<(QDataStream& out, const ControlPower& p);
QDataStream& operator>>(QDataStream& in, ControlPower& p);


///
/// \brief The Power Calibration Point struct
///
struct PowerCalPoint
{
    float        Frequency;         ///< частота
    float        Power;             ///< измерения мощности
    float        RefLevel;          ///< связанный уровень опорного канала
    ControlPower Points[8];         ///< по числу шагов грубого аттенюатора 0 8 16 24 32 40 48 54дБ

    PowerCalPoint() = default;
    PowerCalPoint(QDataStream& in);
};
QDataStream& operator<<(QDataStream& out, const PowerCalPoint& p);
QDataStream& operator>>(QDataStream& in, PowerCalPoint& p);

///
/// \brief The TR1300 Calibration Header struct
///
struct TR1300Header
{
    quint8  ValidPatern;    ///< паттерн 0x55 для индикации непустого заголовка
    quint32 Serial;         ///< 8 цифр
    double  SignalShift[2];
    double  HeterShift[2];
    quint32 StartFactoryHeaderAddress;
    quint32 FactoryDataSize;
    quint32 StartPowerHeaderAddress;
    quint32 PowerDataSize;

    quint32 StopDataAddress()
    {
        return (StartPowerHeaderAddress + PowerDataSize);
    }
    quint32 DataSize()
    {
        return (StopDataAddress() - StartAddressConst);
    }
    quint32 StartFactoryHeader()
    {
        return StartFactoryHeaderAddress - StartAddressConst;
    }
    quint32 StartPowerHeader()
    {
        return StartPowerHeaderAddress - StartAddressConst;
    }

    bool IsValid()
    {
        if (StartFactoryHeaderAddress != StartAddressConst + sizeof(TR1300Header)) return false;
        if (ValidPatern != ValidPatternConst) return false;
        if (StopDataAddress() > StopAddressConst) return false;
        return true;
    }
    static const quint32 StartAddressConst;
    static const quint32 StopAddressConst;
    static const quint8 ValidPatternConst;
};
QDataStream& operator<<(QDataStream& out, const TR1300Header& p);
QDataStream& operator>>(QDataStream& in, TR1300Header& p);

#pragma pack(pop)



///
/// \brief The TR1300FactoryTermsCalibration class
///
class TR1300FactoryTermsCalibration : public FactoryTermsCalibration
{
public:
    TR1300FactoryTermsCalibration(const VnaCmd& cmd);
    TR1300FactoryTermsCalibration(QByteArray& data);

    // FactoryCalibration interface
    virtual void Load(QByteArray& data);
    virtual void Save(QByteArray& data);

private:
    CalHeader _header;
}; // class TR1300FactoryTermsCalibration



///
/// \brief The TR1300FactoryPowerCalibration class
///
class TR1300FactoryPowerCalibration : public FactoryPowerCalibration
{
public:

    struct PowerCodes
    {
        int AttCode; ///< attenuator code
        int DacCode; ///< DAC code
    };

    TR1300FactoryPowerCalibration(const VnaCmd& cmd);
    TR1300FactoryPowerCalibration(const VnaCmd& cmd, Planar::VNA::CalibrationRange* range);

    // FactoryCalibration interface
    virtual void Load(QByteArray& data);
    virtual void Save(QByteArray& data);

    virtual Planar::Dsp::Complex64 R(double frequency) const;

    PowerCodes GetPowerCodes(double freq, double power);
    PowerCodes GetPowerCodes(Unit freq, Unit power)
    {
        return GetPowerCodes(freq.valueVna(), power.valueVna());
    }


private:
    int FindDACCode(double P, int X0, double P0, int X1, double P1, int X2, double P2);

    static const int MiddleDACIndex;
    static const int ControlCodesCount;
    static const int ControlDACCodes[];
    static const int MiddleAttIndex;
    static const int AttenuatorLevels[];
    static const int AttenuatorCodes[];

    PowerCalHeader _header;
    std::vector<PowerCalPoint> _points;

//    // RcvCalibrationRange interface
//protected:
//    Planar::VNA::RcvCalibrationRange *Adapt(const Planar::VNA::FrequencyRange &range) override;
};



///
/// \brief The TR1300FactoryCalibrationBundle class
///
class TR1300FactoryCalibrationBundle : public FactoryCalibrationBundle
{
public:
    TR1300FactoryCalibrationBundle(const VnaCmd& cmd);

    // FactoryCalibration interface
    virtual void Load();
    virtual void Save();

    // FactoryCalibrationBundle interface
protected:
    virtual FactoryTermsCalibration* CreateFactoryTermsCalibratio();
    virtual FactoryPowerCalibration* CreateFactoryPowerCalibratio();

private:
    TR1300Header _header;
    quint32      _crc;
}; // class TR1300FactoryCalibrationBundle

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // TR1300FACTORYCALIBRATION_H
