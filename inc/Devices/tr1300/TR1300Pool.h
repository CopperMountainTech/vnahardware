#ifndef TR1300POOL_H
#define TR1300POOL_H

#include <QObject>

#include "VnaDevicePool.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TR1300Pool : public VnaDevicePool
{
public:
    TR1300Pool(const QJsonObject& jsonDeviceObj);

    // DevicePool override
    virtual Planar::Hardware::DeviceBase* createDevice(Planar::Hardware::DeviceId id, Planar::AnalyzerItem* parent);


protected:
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // DEVICECONTAINER_H
