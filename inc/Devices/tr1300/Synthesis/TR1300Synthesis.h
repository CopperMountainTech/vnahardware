#ifndef TR1300SYNTHESIS_H
#define TR1300SYNTHESIS_H

#include "TSyntTR1300.h"
#include "TSynthesizerBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {


TSyntTR1300* SyntTR1300();

TSynthesizerBase* SynthesizerBaseA();

void InctrementInstanceCount();

void DestroySyntTR1300();

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // TR1300SYNTHESIS_H
