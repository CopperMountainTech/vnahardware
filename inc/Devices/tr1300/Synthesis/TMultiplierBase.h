#ifndef TMULTIPLIERBASE_H
#define TMULTIPLIERBASE_H

#include "TR1300TypeDef.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TMultiplierBase
{
public:
    TMultiplierBase();
    ~TMultiplierBase();

    void Init(double _Mmin, double _Mmax, double _MaxMultiplier, double _MinMultiplier);
    void Unload();

    bool pInit()
    {
        return FInit;
    }
    int Size()
    {
        return  FSize;
    }
    double MaxMultiplier()
    {
        return FMaxMultiplier;
    }
    double MinMultiplier()
    {
        return FMinMultiplier;
    }

    CMult C(int index)
    {
        return FC[index];
    }

private:
    TCMult FC;

    bool FInit;
    int FSize;
    double Mmin;
    double Mmax;
    double FMaxMultiplier;
    double FMinMultiplier;
    bool CheckFraction(double Multiplier, double Delta, double* FArray, int ASize);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // TMULTIPLIERBASE_H
