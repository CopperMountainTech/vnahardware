#ifndef TFS_TR1300_H
#define TFS_TR1300_H

#include "TMultiplierBase.h"
#include "TAddressBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TFS_TR1300
{
public:
    TFS_TR1300();
    ~TFS_TR1300();

    void Init(TMultiplierBase* Multiplier, TAddressBase* Address, double _CompareFreq,  double _R,
              double _IntermediateFreq);

    bool SearchFactors(double _TargetFreq, double FreqShift, double _Accuracy, int NextVariant);

    bool pInit()
    {
        return FInit;
    }

    int pR()
    {
        return SaveR;
    }
    int pN1()
    {
        return  SaveN1;
    }
    int pF1()
    {
        return SaveF1;
    }
    int pM1()
    {
        return SaveM1;
    }
    int pN2()
    {
        return SaveN2;
    }
    int pF2()
    {
        return SaveF2;
    }
    int pM2()
    {
        return SaveM2;
    }


private:
    TMultiplierBase* FMultiplier;
    TAddressBase* FAddress;
    bool FInit;

    double R;
    double CompareFreq;
    double N1;
    double C1StartFreq;
    double TargetFreq;
    double N2;
    double Accuracy;

    double Compare1;
    double Compare2;
    double MinError;

    int SaveR, SaveN1, SaveF1, SaveM1, SaveN2, SaveF2, SaveM2;

    int LastField;
    int LastZone;

    TBoolField PoorField1;
    TBoolField PoorField2;

    int Divider;

    bool FindInZone(int i);

    double IntermediateFreq;
    bool DetectNeedle1(double WBSFreq, double NBSFreq);
    bool DetectNeedle2(double WBSFreq, double NBSFreq);
    bool DetectNeedle3(double WBSFreq, double NBSFreq);
};
}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // TFS_TR1300_H
