#ifndef TSYNTHESIZERBASE_H
#define TSYNTHESIZERBASE_H

#include "TMultiplierBase.h"
#include "TAddressBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TSynthesizerBase
{
public:     // User declarations
    TSynthesizerBase();
    ~TSynthesizerBase();

    TMultiplierBase* Multiplier;
    TAddressBase*    Address;

};
}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // TSYNTHESIZERBASE_H
