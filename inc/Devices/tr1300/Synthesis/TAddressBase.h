#ifndef TADDRESSBASE_H
#define TADDRESSBASE_H

#include "TMultiplierBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TAddressBase
{
private:

    TMultiplierBase*
    FMultiplier;   // вынесем массив С за пределы класса... пусть будет общим

    bool FInit;

    TCAdr  FAddress;
    double FStepFreq; // шаг для определения i-го адреса
    double FStartFreq;
    double FStopFreq;

    double CompareFreq;



public:
    TAddressBase();
    ~TAddressBase();

    void Init(TMultiplierBase* Multiplier, double _CompareFreq);

    void Unload();

    bool pInit()
    {
        return FInit;
    }

    int Address(int Index)
    {
        return FAddress[Index].Adr;
    }
    int Count(int Index)
    {
        return FAddress[Index].Count;
    }

    double StepFreq()
    {
        return FStepFreq;
    }
    double StartFreq()
    {
        return FStartFreq;
    }
    double StopFreq()
    {
        return FStopFreq;
    }
};
}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // TADDRESSBASE_H
