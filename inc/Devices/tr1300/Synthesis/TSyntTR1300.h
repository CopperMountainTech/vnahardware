#ifndef TSYNTTR1300_H
#define TSYNTTR1300_H

#include "TSynthChannel.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TSyntTR1300
{

public:     // User declarations
    //TSyntTR1300(TComponent* Owner);
    TSyntTR1300();
    ~TSyntTR1300();

    void Initialize();
    void Reset();

    double IFAccuracy()
    {
        return FIFAccuracy;
    }

    void  SetStepAccuracy(double Value)
    {
        FSignal->Accuracy = Value;
    }
    double GetStepAccuracy()
    {
        return FSignal->Accuracy;
    }

    double GetMinStep()
    {
        return FMinStep;
    }

    double GetReferenceFreq();


    double BaseReferenceFreq()
    {
        return FReferenceFreq;
    }

    void   SetRefCorrection(double Value);

    void   SetRefCorrectionEnable(bool Value);
    bool   GetRefCorrectionEnable()
    {
        return FRefCorrectionEnable;
    }

    int    RDivider()
    {
        return FRDivider;
    }

    double GetIntermediateFreq();

    bool GetMirrorIF2()
    {
        return FMirrorIF2;
    }
    void SetMirrorIF2(bool value)
    {
        FMirrorIF2 = value;
    }

    double GetSignalShift(int Index)
    {
        return FSignalShift[Index];
    }
    void SetSignalShift(int Index, double Value)
    {
        FSignalShift[Index] = Value;
    }

    double GetHeterShift(int Index)
    {
        return FHeterShift[Index];
    }
    void SetHeterShift(int Index, double Value)
    {
        FHeterShift[Index] = Value;
    }

    void   SetMinFrequency(double f);
    double MinFrequency()
    {
        return FMinFrequency;
    }

    void   SetMaxFrequency(double f);
    double MaxFrequency()
    {
        return FMaxFrequency;
    }

    double MinPower()
    {
        return FMinPower;
    }

    double MaxPower()
    {
        return FMaxPower;
    }

    double DefaultPower()
    {
        return FDefaultPower;
    }

    TDividerTetra  GetSignalDivider(double f);

    TDividerTetra  GetNextSignalDivider(double f, int NextVariant);
    bool  GetSignalSuccess()
    {
        return FSignal->Success;
    }

    TDividerTetra  GetHeterodyneDivider(double f);
    TDividerTetra  GetNextHeterodyneDivider(double f, int NextVariant);
    bool GetHeterodyneSuccess()
    {
        return FHeterodyne->Success;
    }


    double GetSignFrequency(TDividerTetra Div);
    double GetSignWBSFrequency(TDividerTetra Div);
    double GetSignNBSFrequency(TDividerTetra Div);
    double GetHeterFrequency(TDividerTetra Div);
    double GetHeterWBSFrequency(TDividerTetra Div);
    double GetHeterNBSFrequency(TDividerTetra Div);


    double SwitchDelay()
    {
        return FSwitchDelay;
    }
    double GetBackScanDelay()
    {
        return FBackScanDelay;
    }
    void SetBackScanDelay(double value)
    {
        FBackScanDelay = value;
    }

    double GetMeasureDelay()
    {
        return FMeasureDelay;
    }
    void SetMeasureDelay(double value)
    {
        FMeasureDelay = value;
    }

    // и маски и токи
    int GetNBSMask(TDividerTetra Div);//(double Freq);
    int GetWBSMask(TDividerTetra Div);//(double Freq);
    int GetNBHMask(TDividerTetra Div);//(double Freq);
    int GetWBHMask(TDividerTetra Div);//(double Freq);

    int  GetConstantCode()
    {
        return FConstantCode;
    }
    void SetConstantCode(int value)
    {
        FConstantCode = value;
    }

    int  UpCode()
    {
        return FUpCode;
    }

    int  DownCode()
    {
        return FDownCode;
    }
    //int  Keys() { return GetKeys; }
    int GetKeys();

    bool Off1()
    {
        return FOff1;
    }
    double Clr1()
    {
        return FClr1;
    }
    int    I1()
    {
        return FI1;
    }
    int    J1()
    {
        return FJ1;
    }

    bool Off2()
    {
        return FOff2;
    }
    double Clr21()
    {
        return FClr21;
    }
    double Clr22()
    {
        return FClr22;
    }
    double Clr23()
    {
        return FClr23;
    }

    int    I2()
    {
        return FI2;
    }
    int    J2()
    {
        return FJ2;
    }

    bool Off3()
    {
        return FOff3;
    }
    double Clr3()
    {
        return FClr3;
    }
    int    A3()
    {
        return FA3;
    }
    int    N3()
    {
        return FN3;
    }
    int    M3()
    {
        return FM3;
    }

    bool Off4()
    {
        return FOff4;
    }
    double Clr4()
    {
        return FClr4;
    }
    double B4()
    {
        return  FB4;
    }
    int    N4()
    {
        return FN4;
    }
    int    M4()
    {
        return FM4;
    }
    int    NN4()
    {
        return FNN4;
    }
    int    MM4()
    {
        return FMM4;
    }
    int    MN1()
    {
        return FMN1;
    }
    int    MN2()
    {
        return FMN2;
    }

    //TNotifyEvent      OnChange;

private:
    double   FMinStep;
    double   FIFAccuracy;
    double   FIntermediateFreq;
    double   FReferenceFreq;
    double   FRefCorrection;
    bool     FRefCorrectionEnable;

    double   FBackScanDelay;
    double   FMeasureDelay;
    double   FSwitchDelay;
    double   FMinFrequency;
    double   FMaxFrequency;

    double   FMinPower;
    double   FMaxPower;
    double   FDefaultPower;

    int      FNBSMask;
    int      FWBSMask;
    int      FNBHMask;
    int      FWBHMask;

    int      FLowBandCurrent[9];
    int      FHighBandCurrent[6];

    int      FConstantCode;
    int      FUpCode;
    int      FDownCode;

    bool     FMirrorIF2;
    bool     FLowerLO;

    double   FSignalShift[2];
    double   FHeterShift[2];
    int      FRDivider;

    int      FAttenuatorAx; // по разрядам [ ]...[ ][A3][A2][A1]


    TSynthChannel* FSignal;
    TSynthChannel* FHeterodyne;

    void  SetDefaults();
    void RebuildChannels();
    void SetFields();
    //  double  Read(TEdit *Edit);
    void CheckSafety();


    int    GetSyntCurent(double Freq);

    int    GetLowBandCurrent(double Freq);
    int    GetHighBandCurrent(double Freq);

    void   SetIFAccuracy(double Value);

    double GetRefCorrection();

    bool   FOff1;
    double FClr1;
    int    FI1;
    int    FJ1;

    bool   FOff2;
    double FClr21;
    double FClr22;
    double FClr23;
    int    FI2;
    int    FJ2;

    bool   FOff3;
    double FClr3;
    int    FA3;
    int    FN3;
    int    FM3;

    bool   FOff4;
    double FClr4;
    double FB4;
    int    FN4;
    int    FM4;
    int    FNN4;
    int    FMM4;
    int    FMN1;
    int    FMN2;

};
}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // TSYNTTR1300_H
