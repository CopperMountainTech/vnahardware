#ifndef TSYNTHCHANNEL_H
#define TSYNTHCHANNEL_H

#include "TFS_TR1300.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TSyntTR1300;

class TSynthChannel                        // Signal and Heterodyne
{
public:
    TSynthChannel(TSyntTR1300* Owner);
    ~TSynthChannel();

    TDividerTetra SynthesizePoint(double f, double FreqShift, int NextVariant);
    void ResetFractional(int RDivider);
    bool Success;
    double Accuracy;

private:
    //TSyntTR1300* FOwner;
    TSyntTR1300* FOwner;

    TFS_TR1300*   FS_TR1300;
};
}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // TSYNTHCHANNEL_H
