#ifndef TR1300TYPEDEF_H
#define TR1300TYPEDEF_H

#include <vector>
#include "ADF4154.h"

namespace Planar {
namespace VNA {
namespace Hardware {

#pragma pack(push, 1)
//
// Аппаратно зависимая структура, содержащая данные пресета TR1300
//
class TR1300Preset
{
public:
    TR1300Preset(): Delay5us(0xFFFF), IFBW(0xFFFF), Ctrl(0xFFFF) {}
    unsigned short Delay5us;
    unsigned short
    IFBW;                   // Полоса фильтра = М*10^p.   М = {10, 15, 20, 30, 40, 50, 70}.  P = {-1, 0, 1, 2, 3}.  Байт 0 – содержит М, байт 1 – содержит P.
    unsigned short
    Ctrl;                    // включение зеркального канала, проверка времени установления синтезаторов, установка состояний аттенюатора

    bool operator ==(TR1300Preset const& p)
    {
        return (Ctrl == p.Ctrl) && (IFBW == p.IFBW) && (Delay5us == p.Delay5us);
    }

    bool operator !=(TR1300Preset const& p)
    {
        return !((Ctrl == p.Ctrl) && (IFBW == p.IFBW) && (Delay5us == p.Delay5us));
    }

    bool Update(int ifbw, int att, int delay)
    {
        bool changed = false;
        if (IFBW != ifbw)
        {
            IFBW = ifbw;
            changed = true;
        }
        if (Delay5us != delay)
        {
            Delay5us = delay;
            changed = true;
        }
        if (Ctrl != (att & 0x07))
        {
            Ctrl = att & 0x07;
            changed = true;
        }
        return changed;
    }

    int GetAtt()
    {
        return Ctrl & 0x07;
    }
};



struct TDividerTetra
{
    PLL_DIVIDERS NBS, WBS;
    TDividerTetra(PLL_DIVIDERS n, PLL_DIVIDERS w) : NBS(n), WBS(w) { }
    TDividerTetra() { }
};


//
// Данные точки синтеза
//
class TR1300Pll
{
public:
    TDividerTetra RF;            // сигнальный канал
    TDividerTetra LO;            // гетеродинный узкополосный канал

    TR1300Pll(TDividerTetra rf, TDividerTetra lo) : RF(rf), LO(lo) {}
    TR1300Pll() {}
};


// Регистры управления синтезаторов ADF4154
#define BITP_PLLCTL_M      0            // M3:1 – muxout
#define BITP_PLLCTL_U3     3            // U3 – rf power down
#define BITP_PLLCTL_U5     4            // U5 – phase det.polarity
#define BITP_PLLCTL_CP     5            // CP3:0 – charge pump current
#define BITP_PLLCTL_T0     9            // T8:5  и T0 – noise and spur mode.
#define BITP_PLLCTL_T5     10
#define BITP_PLLCTL_T6     11
#define BITP_PLLCTL_T7     12
#define BITP_PLLCTL_T8     13

class TR1300PllCtrl
{
public:
    TR1300PllCtrl() : CtrlNBRF(0xFFFF), CtrlWBRF(0xFFFF), CtrlNBLO(0xFFFF), CtrlWBLO(0xFFFF) {}
    unsigned short CtrlNBRF;            // сигнальный узкополосный канал
    unsigned short CtrlWBRF;            // сигнальный широкополосный канал
    unsigned short CtrlNBLO;            // гетеродинный узкополосный канал
    unsigned short CtrlWBLO;            // гетеродинный широкополосный канал

    bool operator ==(TR1300PllCtrl const& p)
    {
        return (CtrlNBRF == p.CtrlNBRF) && (CtrlWBRF == p.CtrlWBRF) && (CtrlNBLO == p.CtrlNBLO) && (CtrlWBLO == p.CtrlWBLO);
    }

    bool operator !=(TR1300PllCtrl const& p)
    {
        return (CtrlNBRF != p.CtrlNBRF) || (CtrlWBRF != p.CtrlWBRF) || (CtrlNBLO != p.CtrlNBLO) || (CtrlWBLO != p.CtrlWBLO);
    }

    bool Update(int nbrf, int wbrf, int nblo, int wblo)
    {
        bool changed = false;
        if (CtrlNBRF != nbrf)
        {
            CtrlNBRF = nbrf;
            changed = true;
        }
        if (CtrlWBRF != wbrf)
        {
            CtrlWBRF = wbrf;
            changed = true;
        }
        if (CtrlNBLO != nblo)
        {
            CtrlNBLO = nblo;
            changed = true;
        }
        if (CtrlWBLO != wblo)
        {
            CtrlWBLO = wblo;
            changed = true;
        }
        return changed;
    }


};

#pragma pack(pop)


struct CMult
{
public:
    CMult() {}

    CMult(double mult, double f, double m) :
        Mult(mult), F(f), M(m)
    {
    }

    double Mult;
    double F;
    double M;

    friend bool operator >(const CMult& x, const CMult& y)
    {
        return (x.Mult > y.Mult);
    }
    friend bool operator <(const CMult& x, const CMult& y)
    {
        return (x.Mult < y.Mult);
    }
    friend bool operator ==(const CMult& x, const CMult& y)
    {
        return (x.Mult == y.Mult);
    }
};

struct CAdr
{
    int Adr;
    int Count;
};


typedef std::vector<CMult> TCMult;
typedef std::vector<CAdr> TCAdr;
typedef std::vector<bool> TBoolField;

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // TR1300TYPEDEF_H
