#ifndef TR1300PLAGIN_H
#define TR1300PLAGIN_H

#include <QtCore>

#include "IPlugin.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class TR1300Plugin : public QObject, public Planar::Hardware::IPlugin
{
    Q_OBJECT
    Q_INTERFACES(Planar::Hardware::IPlugin)
    Q_PLUGIN_METADATA(IID "PLANAR.HW.TR1300Plagin.Metadata" FILE "TR1300.json")

public:
    Q_INVOKABLE TR1300Plugin(QObject* parent = nullptr);
    ~TR1300Plugin() override;

    Planar::Hardware::DevicePool* Instance(const QJsonObject& jsonDeviceObj) override;
    void appendProperties(QList<ComponentInfo>* properties) override;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar


#endif // TR1300PLAGIN_H


