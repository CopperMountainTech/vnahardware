#ifndef TR1300BINDER_H
#define TR1300BINDER_H

#include "Binder.h"
#include "TR1300Synthesizer.h"

namespace Planar {
namespace VNA {
namespace Hardware {

//namespace Hardware {
class TR1300FactoryPowerCalibration;
//}

using namespace Hardware;

class TR1300Binder : public Binder
{
public:
    TR1300Binder(Planar::AnalyzerItem* parent, VnaCmd* cmd, ScanRangeBase* range, TR1300FactoryPowerCalibration* calibration);
    ~TR1300Binder() override;

    // выходной массив данных команд.
    unsigned char* outData(unsigned int& size) override;

    // Формирует выходной массив данных команд.
    virtual void outProcessing() override;

private:
    TR1300FactoryPowerCalibration* _factoryCalibration;
    TR1300Synthesizer* _synthesizer;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // TR1300BINDER_H
