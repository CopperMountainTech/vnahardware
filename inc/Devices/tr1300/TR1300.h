#pragma once

#include "VnaBase.h"
#include "VnaDevicePool.h"

#include "Properties/TR1300TemperatureNotice.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TR1300 : public VnaBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(TR1300TemperatureNotice* temperature READ temperature NOTIFY propertyChanged)

public:
    TR1300(VnaDevicePool* pool, Planar::Hardware::DeviceId id, Planar::AnalyzerItem* parent);
    ~TR1300() override;

protected:
    VnaCmd* Cmd() override;

    IBinder* CreateBinder(ScanRangeBase* range) override;             // Наследник должен переопределить этот метод и создавать свой Binder
    VnaCmd* CreateCmd();
    virtual FactoryCalibrationBundle* CreateFactoryCalibrations() override;
    bool initKeyAndSerial() override;
    bool activate() override;
    void deactivate() override;
    void startPipeline() override;
    void stopPipeline() override;

    // Notice
    inline TR1300TemperatureNotice* temperature()
    {
        return &_temperature;
    }

    TR1300TemperatureNotice _temperature;

protected slots:
    void slotTemperatureRequest();

};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar



