#ifndef TR1300SYNTHESIZER_H
#define TR1300SYNTHESIZER_H

#include <vector>

#include "SynthesizerBase.h"

#include "TR1300Cmd.h"

#include "TR1300TypeDef.h"



namespace Planar {
namespace VNA {
namespace Hardware {



class TSyntTR1300;

class TR1300FactoryPowerCalibration;

class TR1300Synthesizer : public SynthesizerBase
{
public:
    TR1300Synthesizer(TR1300Cmd* cmd, ScanRangeBase* range, TR1300FactoryPowerCalibration* calibration);
    ~TR1300Synthesizer() override;
    bool processing() override;

protected:
    bool synthesizePoint(int port, int idx) override;
    bool postSynthesize() override;

private:
    bool DetectCrossNoise(double SW, double SN, double GW, double GN);
    void GetPowerCodes(double freq, double pow, int& att, int& dacARM, bool powerOn);
    bool SynthesizePoint(ScanPointBase& point, int pointIdx);    // Синтез одной частотной точки.
    void AppendPoint(std::vector<unsigned char>& outData, ScanPointBase& point, int pointIdx);
    void AppendBackScan(std::vector<unsigned char>& outData, ScanPointBase& point);

    TR1300Cmd* _cmd;
    TR1300FactoryPowerCalibration* _calibration;
    QVector<TR1300Pll>
    _tr1300Pll;              // частотные точки (делители синтезаторов) [points]

    TR1300Preset _preset;
    TR1300PllCtrl _pllCtrl;
    int _dacARM;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // TR1300SYNTHESIZER_H
