#ifndef TR1300CMD_H
#define TR1300CMD_H

#include <QVector>

#include "MeasuringResult.h"
#include "VnaCmd.h"
#include "TR1300TypeDef.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class TR1300Cmd : public VnaCmd
{
public:
    TR1300Cmd(Planar::Hardware::DeviceBase* device);

    // Override VnaCmd
//    bool CheckPipelineStart(unsigned char* buf);
//    bool CheckMeasure(unsigned char* buf);
//    bool CheckBackScan(unsigned char* buf);


    // TR1300
    void CmdPreset(std::vector<unsigned char>& ctrlData, TR1300Preset preset);
    void CmdMeasure(std::vector<unsigned char>& ctrlData, TR1300Pll point, int activePort, int dacArm, int pointIdx);
    void CmdPllCtrl(std::vector<unsigned char>& ctrlData, TR1300PllCtrl pllCtrl);
    void CmdBackScan(std::vector<unsigned char>& ctrlData, TR1300Pll point, int activePort, int dacArm);
    void CmdTemperature(std::vector<unsigned char>& ctrlData);

    bool tryParseMeasure(MeasuringResult* result, unsigned char* buf) override;
//    bool TemperatureParse(double& temperature, unsigned char* buf);
private:

    void PackDividers(unsigned char* buf, PLL_DIVIDERS div);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // TR1300CMD_H
