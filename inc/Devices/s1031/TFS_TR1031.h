#include "Synthesizer.h"

//---------------------------------------------------------------------------
class TFS_O1031
{
private:
    MultiplierBase* _multiplier;
    AddressBase* _address;
    bool _init;

    double R;
    double CompareFreq, C1StartFreq, TargetFreq;
    double Accuracy;
    double N1, N2;

    double Compare1, Compare2;
    double MinError;

    int SaveR;
    int SaveN1, SaveF1, SaveM1;
    int SaveN2, SaveF2, SaveM2;

    int LastField;
    int LastZone;

    BoolField PoorField1, PoorField2;

    int Divider;

    bool  FindInZone(int i);

    double IntermediateFreq;
    bool  DetectNeedle1(double WBSFreq, double NBSFreq);
    bool  DetectNeedle2(double WBSFreq, double NBSFreq);
    bool  DetectNeedle3(double WBSFreq, double NBSFreq);

public:
    TFS_O1031();
    ~TFS_O1031();

    void  Init(MultiplierBase* Multiplier,
               AddressBase* Address,
               double _CompareFreq,
               double _R,
               double _C1StartFreq,
               double _IntermediateFreq);

    bool  SearchFactors(double _TargetFreq,
                        double _Accuracy,
                        bool NextVariant);

    bool pInit() { return _init; }
    int pR()  { return SaveR; }
    int pN1() { return SaveN1; }
    int pF1() { return SaveF1; }
    int pM1() { return SaveM1; }
    int pN2() { return SaveN2; }
    int pF2() { return SaveF2; }
    int pM2() { return SaveM2; }
};
