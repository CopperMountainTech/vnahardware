#ifndef S5435TYPEDEF_H
#define S5435TYPEDEF_H


typedef unsigned char  uchar;
typedef unsigned int   uint;
typedef unsigned short ushort;

#pragma pack(push, 1)

struct S5435Correction {
    S5435Correction() : VCO_SEL(0), VCO_CAPCTRL(0), VCO_DACISET(0) {}
    uchar  VCO_SEL;     // Номер ГУНа.
    uchar  VCO_CAPCTRL; // Номер емкости.
    ushort VCO_DACISET; // Код ЦАПа.
};

struct S5435SyntCh {
    uint   _N;       // LMX2594: PLL_N ( uint32_t)
    uint   _NUM;     // LMX2594: PLL_NUM ( uint32_t)
    ushort _FData;
    S5435Correction _corr;

    S5435SyntCh() {}
    S5435SyntCh(uint N,
                uint NUM,
                ushort LMX2582, /* Код частоты опорного синтезатора LMX2582.
                                        0 – 200 МГц. */
                ushort nFilt, /* Номер фильтра для выхода B.
                                        0 – 1200 МГц;
                                        1 – 2250 МГц;
                                        2 – 3400 МГц;
                                        3 – 5500 МГц;
                                        4 – 7200 МГц; */
                ushort CHDIV, /* Код выходного делителя синтезатора(CHDIV). */
                ushort OUT_PWR, /* Мощность выхода синтезатора(OUT_PWR).
                                            0 – Минимальная;
                                            63 – Максимальная. */
                bool nSynt, /* Номер выхода синтезатора (A / B). А – 8000-15000 МГц, В – 600-8000 МГц.
                                            0 – А;
                                            1 – В.*/
                S5435Correction corr)
    {
        switch(CHDIV)
        {
            case  1: CHDIV = 15; break;
            case  2: CHDIV =  0; break;
            case  4: CHDIV =  1; break;
            case  6: CHDIV =  2; break;
            case  8: CHDIV =  3; break;
            case 12: CHDIV =  4; break;
            case 16: CHDIV =  5; break;
            case 24: CHDIV =  6; break;
            case 32: CHDIV =  7; break;
            case 48: CHDIV =  8; break;
            case 64: CHDIV =  9; break;
            case 72: CHDIV = 10; break;
            case 96: CHDIV = 11; break;
        }

        _N     = N;
        _NUM   = NUM;
        _FData = ( LMX2582 & 0x03 ) |
                 ( ( nFilt & 0x07 ) << 2 )  |
                 ( ( CHDIV & 0x0F ) << 5 )  |
                 ( ( OUT_PWR & 0x3F ) << 9 ) |
                 ( nSynt ? 0x8000 : 0 );
        _corr  = corr;
    }
};

struct S5435SyntChDebug {
    S5435SyntChDebug() {}
    S5435SyntChDebug( bool isHeter,
                      uint numFreq,
                      uint N,
                      uint NUM,
                      uint NUM_DEN_82,
                      uint NUM_DEN_94 )
    {
        _FData = ( isHeter ? 0x80 : 0x00 ) |
                 ( ( numFreq & 0x07 ) << 5 );
        _N = N;
        _NUM = NUM;
        _NUM_DEN_82 = NUM_DEN_82;
        _NUM_DEN_94 = NUM_DEN_94;
    }
    uchar _FData;
    uint  _N;       // LMX2594: PLL_N ( uint32_t)
    uint  _NUM;     // LMX2594: PLL_NUM ( uint32_t)
    uint  _NUM_DEN_82;
    uint  _NUM_DEN_94;
};

struct S5435MeasurePayloadPkt
{
    S5435MeasurePayloadPkt() {}
    S5435MeasurePayloadPkt( const S5435SyntCh &rf , const S5435SyntCh &lo)
    {
        RF = rf;
        LO = lo;
    }
    S5435SyntCh RF;
    S5435SyntCh LO;
};


struct S5435Preset {
public:
    S5435Preset(): DelayX5ns(0), ActivePort(0), IFBW(0), PortSwDelayX5ns(0) {}
    ushort DelayX5ns;          // Код задержки перед измерением. Интервал исчисления 5 мкс.
    uchar  ActivePort;         // номер стимулирующего порта
    uchar  Reserved;           // = 0
    ushort IFBW;               // Полоса фильтра = М*10^p.   М = {10, 15, 20, 30, 40, 50, 70}.  P = {-1, 0, 1, 2, 3}.
                               // Байт 0 – содержит М, байт 1 – содержит P.
    ushort PortSwDelayX5ns;    // Код задержки переключения порта. Интервал исчисления 5 мкс

    bool operator !=(S5435Preset const &p) const
    {
        return (DelayX5ns  != p.DelayX5ns ) ||
               (ActivePort != p.ActivePort) ||
               (IFBW       != p.IFBW      ) ||
               (PortSwDelayX5ns != p.PortSwDelayX5ns);
    }

    bool operator ==(S5435Preset const &p) const
    {
        return !( p != *this );
    }

    bool Update(uint delay, uchar activePort, ushort ifbw, ushort portSwDelay)
    {
        delay /= 5;

        bool changed = false;
        if(DelayX5ns != delay)
        {
            DelayX5ns = delay;
            changed = true;
        }

        if(ActivePort != activePort)
        {
            ActivePort = activePort;
            changed = true;
        }

        if(IFBW != ifbw)
        {
            IFBW = ifbw;
            changed = true;
        }

        if(PortSwDelayX5ns != portSwDelay)
        {
            PortSwDelayX5ns = portSwDelay;
            changed = true;
        }

        return changed;
    }
};


#pragma pack(pop)

#endif // S5435TYPEDEF_H
