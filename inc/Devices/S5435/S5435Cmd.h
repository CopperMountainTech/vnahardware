#ifndef TR5048CMD_H
#define TR5048CMD_H

#include <QVector>

#include "MeasuringResult.h"
#include "VnaCmd.h"
#include "S5435TypeDef.h"
#include "VnaHw.h"

using namespace Planar::VNA;

namespace Hardware
{


class S5435Cmd : public VnaCmd
{
public:
    S5435Cmd(IIO *io);

    // Override VnaCmd
    bool CheckPipelineStart(unsigned char *buf, int markerId);
    bool CheckMeasure(unsigned char *buf);
    bool CheckBackScan(unsigned char *buf);


    // S5435
    void Preset(vector<unsigned char> &outBuf, S5435Preset preset);
    void DebugPreset(vector<unsigned char> &outBuf, S5435SyntChDebug preset );
    void Measure(vector<unsigned char> &outBuf, S5435MeasurePayloadPkt point, int activePort, int stepAtt, int dacARM, int pointIdx);
    void BackScan(vector<unsigned char> &outBuf, S5435MeasurePayloadPkt point, int stepAtt, int activePort, int dacArm);

    void SyncIn(vector<unsigned char> &ctrlData, SyncProperty prop);
    void SyncOut(vector<unsigned char> &ctrlData, SyncProperty prop);

    bool Temperature2Parse(double &tRF, double &tLO, unsigned char *buf);

    void CorrectionFlag(vector<unsigned char> &outBuf);

    bool CorrectionParse(QVector<S5435Correction> &correctionRF, QVector<S5435Correction> &correctionLO, unsigned char *buf );
    bool MeasureParse(MeasuringResult &result, unsigned char *buf);
    bool TemperatureParse(double &temperature, unsigned char *buf);
private:
    int _pointIdx;
    int _error;


};

}
#endif // TR5048CMD_H
