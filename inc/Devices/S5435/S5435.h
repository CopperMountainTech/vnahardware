#ifndef VNAS5085_H
#define VNAS5085_H

#include "VnaBase.h"
#include "S5435Cmd.h"
#include "Integrants/S5435ExtSyncIn.h"
#include "Integrants/S5435ExtSyncOut.h"

namespace Hardware
{

//
// Функционал S5435, независимый от физического интерфейса
//
class S5435 : public VnaBase
{
public:
    S5435(VnaDevicePool *pool, DeviceId id);
    ~S5435(){}

    // VnaBase override
    VnaCmd* CreateCmd();

    QString GetSerial();
    bool SetSerial(QString serial);

    ITemperature* CreateTemperature();


protected:
    // VnaBase override
    Binder *CreateBinder(ScanRangeBase *range);

    IExtSyncIn *CreateExtSyncIn();
    IExtSyncOut *CreateExtSyncOut();

    bool FwLoad();
    bool InitKeyAndSerial();
    FactoryCalibrationBundle *CreateFactoryCalibrations();
};

}

#endif // VNAS5085_H
