#ifndef S5435SYNTHESIZER_H
#define S5435SYNTHESIZER_H

#include <vector>

#include "Synthesizer.h"

#include "S5435Cmd.h"
#include "S5435TypeDef.h"
#include "S5435ServiceDialog.h"

using namespace std;

namespace Hardware
{


///class S5435FactoryPowerCalibration;

class S5435Synthesizer : public SynthesizerBase
{
    struct LMX_SYNT { /// debug!!!
        bool debugEnable;
        uint N, NUM, DEN;
        uint freqCode;
    };

    LMX_SYNT _lmxSynt2582RF;
    LMX_SYNT _lmxSynt2582LO;
    LMX_SYNT _lmxSynt2594RF;
    LMX_SYNT _lmxSynt2594LO;

    uint     _delayBeforeMeasurement;
    uint     _portSwDelay;

    bool _forceAutoCorrection;

public:
    S5435Synthesizer(S5435Cmd *cmd, ScanRangeBase *range/*, S5435FactoryPowerCalibration* calibration*/);
    ~S5435Synthesizer();

    void setDebugEnamle(bool isHeter, bool enable = true )
    {
        if( isHeter ) _lmxSynt2582LO.debugEnable = enable;
        else          _lmxSynt2582RF.debugEnable = enable;
    }
    void setLMX2582_N  ( bool isHeter, uint N   ) { if( isHeter ) _lmxSynt2582LO.N   = N;   else _lmxSynt2582RF.N   = N;   }
    void setLMX2582_NUM( bool isHeter, uint NUM ) { if( isHeter ) _lmxSynt2582LO.NUM = NUM; else _lmxSynt2582RF.NUM = NUM; }
    void setLMX2582_DEN( bool isHeter, uint DEN ) { if( isHeter ) _lmxSynt2582LO.DEN = DEN; else _lmxSynt2582RF.DEN = DEN; }
    void setLMX2594_DEN( bool isHeter, uint DEN ) { if( isHeter ) _lmxSynt2594LO.DEN = DEN; else _lmxSynt2594RF.DEN = DEN; }

    bool getLMX2582( bool isHeter ) const { if( isHeter ) return _lmxSynt2582LO.debugEnable;
                                            else          return _lmxSynt2582RF.debugEnable;   }

    uint getLMX2582_N  ( bool isHeter ) const { if( isHeter ) return _lmxSynt2582LO.N;   else return _lmxSynt2582RF.N;   }
    uint getLMX2582_NUM( bool isHeter ) const { if( isHeter ) return _lmxSynt2582LO.NUM; else return _lmxSynt2582RF.NUM; }
    uint getLMX2582_DEN( bool isHeter ) const { if( isHeter ) return _lmxSynt2582LO.DEN; else return _lmxSynt2582RF.DEN; }
    uint getLMX2594_DEN( bool isHeter ) const { if( isHeter ) return _lmxSynt2594LO.DEN; else return _lmxSynt2594RF.DEN; }

    uint getLMX2582FreqCode( bool isHeter ) const { if( isHeter ) return _lmxSynt2582LO.freqCode; else return _lmxSynt2582RF.freqCode; }
    void setLMX2582FreqCode( bool isHeter, uint freqCode ) { if( isHeter ) _lmxSynt2582LO.freqCode = freqCode; else _lmxSynt2582RF.freqCode = freqCode; }

    uint getDelayBeforeMeasurement() const { return _delayBeforeMeasurement; }
    void setDelayBeforeMeasurement( uint delayBeforeMeasurement ) { _delayBeforeMeasurement = delayBeforeMeasurement; }

    bool getForceAutoCorrection() const { return _forceAutoCorrection; }
    void setForceAutoCorrection( bool forceAutoCorrection ) { _forceAutoCorrection = forceAutoCorrection; }

    QWidget* ServiceDialog() override;

    void OutProcessing(vector<unsigned char> &outData);

protected:       
    // SynthesizerBase

    void AppendPoints(vector<unsigned char> &outData);

    void OnRangeChanged();                                          // распределение памяти под данные
    bool InProcessing(MeasuringResult &result, unsigned char *buf);

    bool SynthesizePoint(ScanPointBase &point, int pointIdx);    // Синтез одной частотной точки.
    void AppendPoint(vector<unsigned char> &outBuf, ScanPointBase &point, int pointIdx);
    void AppendBackScan(vector<unsigned char> &outBuf, ScanPointBase &point);

private:
    int _activePort;

    S5435Cmd *_cmd;
    ///S5435FactoryPowerCalibration* _calibration;
    QVector<S5435MeasurePayloadPkt> _Plls;              // частотные точки (делители синтезаторов) [points]

    S5435Preset _preset;

    bool _autoCorrection;
    bool _enableAutoCorrection;
    QVector<S5435Correction> _correctionRF;
    QVector<S5435Correction> _correctionLO;
    
    bool DetectCrossNoise(double SW, double SN, double GW, double GN);

    bool LeapSynthez(); // const double Sign, const double Heter, const double SignLowerAccuracy,const double SignUpperAccuracy, double& SignFact, TDividers& Div
    bool SynthezPoint(int index, double freq, double pw, S5435SyntCh* div , bool isHeterodyne);
    void AppendPreset(vector<unsigned char> &outBuf, S5435MeasurePayloadPkt pll, Unit ifbw, bool force);
    void AppendAutoCorrectionFlag(vector<unsigned char> &outBuf);
    S5435ServiceDialog* _serviceDialog;
};

}

#endif // S5435SYNTHESIZER_H
