#ifndef S50XXDEVICE_H
#define S50XXDEVICE_H

#include <QtCore>

#include "IPlugin.h"

namespace Hardware
{
class S5435Plugin : public QObject, public IPlugin
{
    Q_OBJECT
    Q_INTERFACES(Hardware::IPlugin)
    Q_PLUGIN_METADATA(IID "PLANAR.HW.S5435Plagin.Metadata" FILE "S5435.json")

public:
    Q_INVOKABLE S5435Plugin(QObject *parent = 0);
    ~S5435Plugin();

     DevicePool *Instance(const QJsonObject &jsonDeviceObj);
};

}


#endif // S50XXDEVICE_H
