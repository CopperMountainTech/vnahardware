#ifndef S50XXPOOL_H
#define S50XXPOOL_H

#include <QObject>

#include "VnaDevicePool.h"

using namespace Hardware;

namespace Hardware
{

class S5435Pool : public VnaDevicePool
{
public:
    S5435Pool(const QJsonObject &jsonDeviceObj);

    // DevicePool override
    virtual DeviceBase *CreateDevice(DeviceId deviceId);


protected:
};

}

#endif // DEVICECONTAINER_H
