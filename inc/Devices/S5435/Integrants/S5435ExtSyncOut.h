#ifndef S5435EXTSYNCOUT_H
#define S5435EXTSYNCOUT_H

#include "ExtSyncOut.h"
#include <S5435Cmd.h>

namespace Hardware {

class S5435ExtSyncOut : public ExtSyncOutBase
{
public:
    S5435ExtSyncOut(S5435Cmd *cmd);

    void OutProcessing(vector<unsigned char> &outData);

private:
    S5435Cmd* _cmd;
};

}

#endif // S5435EXTSYNCOUT_H
