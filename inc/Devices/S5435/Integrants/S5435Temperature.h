#ifndef C3220TEMPERATURE_H
#define C3220TEMPERATURE_H

#include "Temperature.h"

#include "S5435Cmd.h"

using namespace Hardware;

class S5435Temperature : public TemperatureBase
{   Q_OBJECT
public:
    S5435Temperature(S5435Cmd *cmd);
    bool InProcessing(unsigned char *buf);
    QVector<double> GetTemperature();
private:
    atomic<double> _temperatureRF;
    atomic<double> _temperatureLO;
};

#endif //C3220TEMPERATURE_H
