#ifndef S5435EXTSYNCIN_H
#define S5435EXTSYNCIN_H

#include "ExtSyncIn.h"
#include <S5435Cmd.h>

namespace Hardware {

class S5435ExtSyncIn : public ExtSyncInBase
{
public:
    S5435ExtSyncIn(S5435Cmd *cmd);

    void OutProcessing(vector<unsigned char> &outData);

private:
    S5435Cmd* _cmd;
};

}
#endif // S5435EXTSYNCIN_H
