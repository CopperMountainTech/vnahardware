#ifndef S5435SERVICEDIALOG_H
#define S5435SERVICEDIALOG_H

#include <QDialog>
#include <QAbstractButton>

namespace Hardware {
    class S5435Synthesizer;
}

namespace Ui {
class S5435ServiceDialog;
}

class S5435ServiceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit S5435ServiceDialog(QWidget *parent = 0);
    S5435ServiceDialog(Hardware::S5435Synthesizer* synthesizer, QWidget *parent = 0);
    ~S5435ServiceDialog();

private slots:
    void onUpdate();
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::S5435ServiceDialog *ui;
    Hardware::S5435Synthesizer* _synthesizer;

    bool _isHeter;
    bool _isSignal;
};

#endif // S5435SERVICEDIALOG_H
