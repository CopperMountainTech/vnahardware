#ifndef S50XBINDER_H
#define S50XBINDER_H

#include "Binder.h"


namespace Hardware
{

class S5435Binder : public Binder
{
public:
    S5435Binder(VnaCmd *cmd, ScanRangeBase *range);

protected:
// IBinder
    ISynthesizer *CreateSynthesizer();

//private:
//    TR5048FactoryPowerCalibration* _factoryCalibration;
};

}

#endif // S50XBINDER_H
