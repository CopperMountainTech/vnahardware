#ifndef C3220_GLOBAL_H
#define C3220_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(C3220_LIBRARY)
#  define C3220SHARED_EXPORT Q_DECL_EXPORT
#else
#  define C3220SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // C3220_GLOBAL_H
