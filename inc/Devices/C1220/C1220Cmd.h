#ifndef C1220CMD_H
#define C1220CMD_H

#include <QVector>

#include "HW.h"
#include "C1220TypeDef.h"
#include "C1220ConfigSyntez.h"
#include "MeasuringResult.h"

using namespace Planar::VNA;

namespace Hardware
{

class C1220Cmd : public VnaCmd
{
public:
    C1220Cmd(IIO *io);

    void setConfigSyntez( C1220ConfigSyntez* config ) { _config = config; }

    // Override VnaCmd
    bool CheckPipelineStart(unsigned char *buf, int markerId);
    bool CheckMeasure(unsigned char *buf);
    bool CheckBackScan(unsigned char *buf);

    void PresetC1220(vector<unsigned char> &outBuf, C1220Preset preset);
    void Measure(vector<unsigned char> &outBuf, C1220Pll point, int activePort, ushort RF_DAC_FIL, ushort RF_ATT, int pointIdx);
    void BackScan(vector<unsigned char> &outBuf, C1220Pll point, int activePort, ushort dacArm, ushort att);
    void Temperature(vector<unsigned char> &outBuf);

    bool MeasureParse(MeasuringResult &result, unsigned char *buf);
    bool TemperatureParse(double &temperature, unsigned char *buf);

    int HardwareVersion();

private:
    int _pointIdx;
    int _error;

    C1220ConfigSyntez* _config;
};

}
#endif // C1220CMD_H
