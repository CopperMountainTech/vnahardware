#ifndef C1220PLAGIN_H
#define C1220PLAGIN_H

#include <QtCore>

#include "IPlugin.h"

namespace Hardware
{
class C1220Plugin : public QObject, public IPlugin
{
    Q_OBJECT
    Q_INTERFACES(Hardware::IPlugin)
    Q_PLUGIN_METADATA(IID "PLANAR.HW.C1220Plagin.Metadata" FILE "C1220.json")

public:
    Q_INVOKABLE C1220Plugin(QObject *parent = 0);
    ~C1220Plugin();

    DevicePool *Instance(const QJsonObject &jsonDeviceObj);
};

}


#endif // C3220PLAGIN_H


