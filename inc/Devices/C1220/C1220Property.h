#ifndef C1220PROPERTY_H
#define C3220PROPERTY_H

#include "VnaPropertyBase.h"

namespace Hardware
{

class C1220Property : public VnaPropertyBase
{
public:
    C1220Property(QJsonObject &jsonDevice);

    int NumberOfPorts();
};

}
#endif // C1220PROPERTY_H
