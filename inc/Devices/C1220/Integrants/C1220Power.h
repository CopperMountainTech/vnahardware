#ifndef C1220POWER_H
#define C1220POWER_H

#include "Power.h"

#include "C1220Cmd.h"

using namespace Hardware;

class C1220Power : PowerBase
{
public:
    C1220Power(C1220Cmd *cmd);
    void OutProcessing(vector<unsigned char> &outBuf);
    void Reset();
private:
    C1220Cmd *_cmd;
};

#endif // C1220POWER_H
