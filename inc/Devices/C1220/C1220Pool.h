#ifndef C1220POOL_H
#define C1220POOL_H

#include <QObject>

#include "VnaDevicePool.h"

using namespace Hardware;

namespace Hardware
{

class C1220Pool : public VnaDevicePool
{
public:
    C1220Pool(const QJsonObject &jsonDeviceObj);

    // DevicePool override
    virtual DeviceBase *CreateDevice(DeviceId deviceId);
};

}

#endif // C1220POOL_H
