#ifndef C1220BINDER_H
#define C1220BINDER_H

#include "Binder.h"

namespace Hardware {
    class C1220FactoryPowerCalibration;
}

using namespace Hardware;

class C1220Binder : public Binder
{
public:
    C1220Binder(VnaCmd *cmd, ScanRangeBase *range, C1220FactoryPowerCalibration* calibration);

protected:
    // IBinder
    ISynthesizer* CreateSynthesizer();

private:
    C1220FactoryPowerCalibration* _factoryCalibration;

};

#endif // C1220BINDER_H
