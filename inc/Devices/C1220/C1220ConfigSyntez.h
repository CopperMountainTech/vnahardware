#ifndef C1220CONFIGSYNTEZ_H
#define C1220CONFIGSYNTEZ_H

#include <QObject>
#include "C1220FactoryCalibration.h"
#include "ConfigSyntezBase.h"
namespace Hardware
{

#define PORT1 0
#define PORT2 1


class C1220FactoryPowerCalibration;


class C1220ConfigSyntez : public ConfigSyntezBase
{   Q_OBJECT
public:
    C1220ConfigSyntez( int HWVersion, QObject* parent = Q_NULLPTR );
    bool GetAttControlEnabled() const { return _attControl.enable; }
    C1220FactoryPowerCalibration::PowerCodes* GetPowerCodes() const { return _attControl.powerCodes; }
    double GetAltIFDelta() const { return _altIFDelta; }
    char getAnalogFilter() const { return (char)_parameters[ SyntezParameter::spAnalogFilter ].toInt(); }
    vector<int> GetAttControl() const { return _attControl.toVector(); }
signals:
    void updateCurrentPort( int port );
    void updateAltIFDelta( double altIFDeltaMhz );
    void updateEnableAttControl( bool enable );

public slots:
    void setAltIFDelta( double altIFDeltaMhz );
    void setEnableAttControl( bool enable );
    void setAttControl( const vector<int> &att );

private:
    void Default();
    struct AttControl {
        AttControl() : powerCodes( new C1220FactoryPowerCalibration::PowerCodes() ) {
            powerCodes->StepAtt = 10;
            powerCodes->AttCode = 0x00;
            powerCodes->DacCode = 4096;
        }
        bool enable;
        C1220FactoryPowerCalibration::PowerCodes* powerCodes;
        vector<int> toVector() const { return { powerCodes->StepAtt,
                                                powerCodes->AttCode,
                                                powerCodes->DacCode }; }
    };
    /// struct Delays {};

    double        _altIFDelta;
    AttControl    _attControl;
};

}
#endif // C1220CONFIGSYNTEZ_H
