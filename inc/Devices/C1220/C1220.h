#ifndef C1220_H
#define C1220_H

#include "VnaBase.h"
#include "VnaDevicePool.h"

using namespace Hardware;

class C1220 : public VnaBase
{
public:
    C1220(VnaDevicePool *pool, DeviceId id);
    ~C1220(){}

    // VnaBase
    Binder *CreateBinder(ScanRangeBase *range);             // Наследник должен переопределить этот метод и создавать свой Binder
    VnaCmd* CreateCmd();
    //ITemperature *CreateTemperature();                        // Температура
    IPower *CreatePower();

    bool IsLoader();
    QString GetSerial();
    bool SetSerial(QString serial);
protected:
    bool FwLoad();
    bool InitKeyAndSerial();
    virtual FactoryCalibrationBundle *CreateFactoryCalibrations();
};

#endif // C1220_H
