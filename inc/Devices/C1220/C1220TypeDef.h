#ifndef C1220TYPEDEF_H
#define C1220TYPEDEF_H

#include "SynthCommon.h"
#include <QVector>
#include <cstring>
#include "HW.h"

namespace Hardware
{

#pragma pack(push, 1)

//
// Аппаратно зависимая структура, содержащая данные пресета C1220
//
class C1220Preset
{
public:
    C1220Preset(): Delay100ns(0), ActivePort(0), IFBW(0), PortSwDelay100ns(0) {}
    unsigned int   Delay100ns;          // код задержки 100 нс
    unsigned char  ActivePort;          // номер стимулирующего порта
    unsigned char  Reserved;            // = 0
    unsigned short IFBW;                // Полоса фильтра = М*10^p.   М = {10, 15, 20, 30, 40, 50, 70}.  P = {-1, 0, 1, 2, 3}.
                                        // Байт 0 – содержит М, байт 1 – содержит P.
    unsigned short PortSwDelay100ns;    // задержка переключения порта 100 нс

    bool operator ==(C1220Preset const & p)
    {
        return (Delay100ns == p.Delay100ns) &&
                (ActivePort == p.ActivePort) &&
                (IFBW == p.IFBW) &&
                (PortSwDelay100ns == p.PortSwDelay100ns);
    }

    bool operator !=(C1220Preset const & p)
    {
        return (Delay100ns != p.Delay100ns) ||
               (ActivePort != p.ActivePort) ||
               (IFBW != p.IFBW) ||
               (PortSwDelay100ns != p.PortSwDelay100ns);
    }

    bool Update(unsigned int delay, unsigned char activePort, unsigned short ifbw, unsigned short portSwDelay)
    {
        bool changed = false;
        if(Delay100ns != delay)
        {
            Delay100ns = delay;
            changed = true;
        }

        if(ActivePort != activePort)
        {
            ActivePort = activePort;
            changed = true;
        }

        if(IFBW != ifbw)
        {
            IFBW = ifbw;
            changed = true;
        }

        if(PortSwDelay100ns != portSwDelay)
        {
            PortSwDelay100ns = portSwDelay;
            changed = true;
        }

        return changed;
    }
};


class TSyntCh {
public:    
    TSyntCh() {}
    TSyntCh(    long long FTW,
                int       Nint,// !
                int       Div, // !
                unsigned char Cur1, unsigned char Cur2,
                bool Mul2, // !
                bool HalfOkt, bool DownMixEnable, int  Nfilt,
                bool LPF_PLL0, // !
                bool LPF_PLL1, // !
                bool TauHi, bool TauLo,
                bool RF_CF1Band, // !
                bool DownMixSelectLO2, bool AltIF, int  UniDAC )
    {
        _Div  = Div;
        _Mul2 = Mul2;
        _LPF_PLL0 = LPF_PLL0;
        _LPF_PLL1 = LPF_PLL1;
        _RF_CF1Band = RF_CF1Band;

        memcpy(&FData[0], &FTW, 6);
        FData[6] =  Nint;

        int Value = Div;
        int DivRes = 0;
        while( Value ) {
            Value /= 2; DivRes += 1;
        }
        DivRes -= 1;


        FData[7] =  (Cur2 & 31) | ( DivRes << 5) | (AltIF ? 128:0);
        FData[8] =  (Cur1 & 31) | (HalfOkt ? 32:0) | (Mul2 ? 64:0) | (RF_CF1Band ? 128:0) ;
        FData[9] =  (Nfilt & 3) | (DownMixEnable ? 4:0) |
                    (LPF_PLL0 ? 8:0) | (LPF_PLL1 ? 64:0) |
                    (TauHi ? 16:0) | (TauLo ? 32:0)|
                    (DownMixSelectLO2 ? 128:0);
        memcpy(&FData[10], &UniDAC, 2);
    }
    const char* data() const { return FData; }
    int  getNint() const { return FData[ 6 ]; }
    int  getDiv()  const { return _Div; }
    int  getMul2() const { return _Mul2; }
    bool getLPF_PLL0() const { return _LPF_PLL0; }
    bool getLPF_PLL1() const { return _LPF_PLL1; }
    bool getCF1Band() const { return _RF_CF1Band; }
    void setUniDAC( unsigned short UniDAC ) { memcpy(&FData[10], &UniDAC, 2); }
private:
    char FData[12];

    int _Div;
    int _Mul2;
    bool _LPF_PLL0;
    bool _LPF_PLL1;
    bool _RF_CF1Band;
};


//
// Данные точки синтеза
//
class C1220Pll
{
public:
    C1220Pll(){}
    C1220Pll(TSyntCh rf, TSyntCh lo) :RF(rf), LO(lo) { }

    TSyntCh RF;
    TSyntCh LO;

    //unsigned short att; // ступенчатый аттенюатор : 0-й байт 5 бит, 1-й байт 1+1 бит (2 мл. бита)
    //unsigned short dac; // ЦАП фильтра 12-20 ГГц
};

#pragma pack(pop)
}

#endif // C1220TYPEDEF_H
