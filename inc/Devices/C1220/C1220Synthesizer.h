#ifndef C1220SYNTHESIZER_H
#define C1220SYNTHESIZER_H

#include <vector>

#include "SynthesizerEx.h"                  // расширенный вариант SynthesizerBase

#include <FrequncyTableUnit.h>
#include "C1220Cmd.h"
#include "C1220TypeDef.h"
#include "C1220FactoryCalibration.h"
#include "C1220ConfigSyntez.h"

#ifdef _C_C1220_SYNTH_
    TFrequencyCode NTableDefault[] =
    {
        #include "V01_C1220_N.cpp"
    };
#else
    extern "C" TFrequencyCode NTableDefault[3955];
#endif

using namespace std;

#define iRound (int)round

namespace Hardware
{

namespace C1220
{   
    const double  IFAccuracyDefault = 0.001;
    #if defined(COPPER)
        const double  DelayARM1Default = 0.5;    // Медный таз
    #else
        const double  DelayARM1Default = 0.1;
    #endif
    const double  DelayARM2Default = 0.05;
    const double  DelayARM3Default = 0.0;
    const double  SyntSwitchDelayDefault = 0.1;
    const double  AGCSwitchDelayDefault = 0.05;
    const double  ThdARM1Default = 10e6;
    const double  ThdARM2Default = 100e6;
    const double  Thd_LPF_PLL_Default = 10.2e9;
    const double  PortSwitchDelayDefault = 0;
    const double  DownMixerLO1v1Default = 1790e6;
    const double  DownMixerLO2v1Default = 1791.25e6;    
    const double  DownMixerLO1v3Default = 1792.72727272727272e6;
    const double  DownMixerLO2v3Default = 1794.44444444444444e6;    
    const double  DownMixerLO1v4Default = 1791.53846153846e6; // C3220
    const double  SyntStartDefault = 6e9;
    const double  ThdHalfOctDefault = 8.5e9;
    const double  ThdFilt1Default = 1.2e9;
    const double  ThdFilt2Default = 2.0e9;
    const double  ThdFilt3Default = 3.0e9;
    const double  ThdRFBandDefault = 6.0e9;
    const double  ThdCF1BandDefault = 1.0e9;
    const double  DDSReferenceDefault = 340e6;
    const double  DDSCenterDefault = 105.5e6;
    const unsigned long long DDSFreqDivisor = 0x0001000000000000/*ui64*/;
}

using namespace C1220;

class C1220FactoryPowerCalibration;
class C1220Synthesizer;

//---------------------------------------------------------------------------
//------------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//------------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TSyntC1220
{
private:
    C1220Synthesizer* C1220Synth;

    double        FPortSwitchDelay;
    double        FSyntSwitchDelay;
    double        FAGCSwitchDelay;
    double        FTreserve;
    bool          FEnableSDelay;
    bool          FEnableHDelay;
public:
    TFrequencyTable RFDACFilTable;
private:
    bool          FFastPLL;    
    int           HWVersion;

    //-------------------------------------------------------------------------
    class TSynthChannel                        // Signal and Heterodyne
    {
    friend class TSyntC1220;
    private:
        TSyntC1220*   FOwner;
        int           FNint;
        bool          FIsHeterodyne;
        TFrequencyTable FKVCOTable[2];

        //int GetCPLLCode(double f, int N, ushort BlockSelect);

    public:
        TSynthChannel(TSyntC1220* Owner, bool IsHeterodyne);
        TSyntCh SynthesizePoint(double Freq, double SignGrid, double HetrGrid);

        double GetDownMixerLO1Default(bool IsHeterodyne);
        //---------------------------------------------------------------------------
        double GetDownMixerLO2Default(bool IsHeterodyne);

        double        FUpAccuracy;
        double        FDownAccuracy;
    };
    //-------------------------------------------------------------------------
    TSynthChannel *FSignal;
    TSynthChannel *FHeterodyne;

    void     SetStepUpAccuracy(double accuracy) { FSignal->FUpAccuracy = accuracy; }
    void     SetStepDownAccuracy(double accuracy) { FSignal->FDownAccuracy = accuracy; }
    double   GetStepUpAccuracy()  { return FSignal->FUpAccuracy; }
    double   GetStepDownAccuracy()  { return FSignal->FDownAccuracy; }
    double   GetTreserve();
public:
    ulong    GetPortSwitchDelay();
    void     SetDefaults();

     double FirstIF;
private:
    ulong    GetSyntSwitchDelay();
    ulong     GetAGCSwitchDelay();
    double   CalcFrequency(__int64 FTW, int Nint, int Div, bool Mul2, bool DownMixEnable, bool DownMixSelectLO2, bool IsHeterodyne);
    double   CalcVCOFrequency(__int64 FTW, int Nint);

public:        // User declarations
    TSyntC1220();
    ~TSyntC1220();

    void SetHardwareVersion( int hv ) { HWVersion = hv; }
    void SetC1220Synthesizer( C1220Synthesizer * _C1220Synth ) { C1220Synth = _C1220Synth; }

    TSyntCh     GetSignalDivider(double Freq, double SignGrid, double HetrGrid);
    TSyntCh     GetNextSignalDivider(double Freq, double SignGrid, double HetrGrid);
    TSyntCh     GetHeterodyneDivider(double Freq, double SignGrid, double HetrGrid);
    double      GetLinearDelay(double error);
    int         GetRFFILCode(double f);
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class C1220FactoryPowerCalibration;
class C1220Synthesizer : public SynthesizerEx
{
public:
    C1220Synthesizer(C1220Cmd *cmd, ScanRangeBase *range, C1220FactoryPowerCalibration* calibration);
    C1220ConfigSyntez* Config() { return dynamic_cast<C1220ConfigSyntez*>(_config); }


    // Получение кода тока PLL. Хабин(С).
    int GetCPLLCode(double f, int N, bool fHeter );
    int GetLODACCode(double freqHz);

    bool SetAtt(const vector<int> &att);

    ~C1220Synthesizer();
protected:
    // SynthesizerBase
    void OnRangeChanged();                                          // распределение памяти под данные
    bool InProcessing(MeasuringResult &result, unsigned char *buf);
    bool SynthesizePoint(ScanPointBase &point, int pointIdx);    // Синтез одной частотной точки.
    void AppendPoint(vector<unsigned char> &outBuf, ScanPointBase &point, int pointIdx);
    void AppendBackScan(vector<unsigned char> &outBuf, ScanPointBase &point);

private:
    C1220Cmd *_cmd;
    C1220FactoryPowerCalibration* _calibration;
    QVector<C1220Pll> _C1220Pll;              // частотные точки (делители синтезаторов) [points]

    unsigned int   delay = 1000;
    C1220Preset _presetC1220;

    unsigned int CalcDelay( const C1220Pll &prev, const C1220Pll &curr);
};

//---------------------------------------------------------------------------
}

#endif // C1220SYNTHESIZER_H
