#pragma once

#include "VnaBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class TemplateDevice : public VnaBase
{
public:
    TemplateDevice(VnaDevicePool* pool, Planar::Hardware::DeviceId id);

protected:
    bool InitKeyAndSerial();
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
