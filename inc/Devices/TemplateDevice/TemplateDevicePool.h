#pragma once

#include "VnaDevicePool.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TemplateDevicePool : public VnaDevicePool
{
public:
    TemplateDevicePool(const QJsonObject& jsonDeviceObj);

    // DevicePool override
    Planar::Hardware::DeviceBase* createDevice(Planar::Hardware::DeviceId deviceId);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
