#pragma once

#include "IPlugin.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TemplateDevicePlugin : public QObject, public Planar::Hardware::IPlugin
{
    Q_OBJECT
    Q_INTERFACES(Planar::Hardware::IPlugin)
    Q_PLUGIN_METADATA(IID "TemplateDevicePlugin.Metadata" FILE "TemplateDevice.json")

public:
    Q_INVOKABLE TemplateDevicePlugin(QObject* parent = 0);
    ~TemplateDevicePlugin();

    Planar::Hardware::DevicePool* Instance(const QJsonObject& jsonDeviceObj);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar



