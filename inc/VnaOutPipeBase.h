#pragma once

#include "IVnaOutPipe.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class VnaOutPipeBase : public IVnaOutPipe
{
public:
    VnaOutPipeBase(QList<IBinder*>* binderList);
    ~VnaOutPipeBase() override;


    ScannerStateType scannerState() override;

    void setTriggerMode(TriggerType mode) override;
    void setTriggerSource(TriggerSourceType source) override;
    void setBinderIdx(int idx) override;
    void setTrigger() override;
    bool waitForScannerState(ScannerStateType state) override;

    void beginUpdateCtrl() override                      // вызвать перед изменение controlData
    {
        //      qDebug() << "begin";
        _controlDataMutex.lock();
        //     qDebug() << "end";
    }

    void endUpdateCtrl() override                       // вызвать после изменения controlData
    {
        //    qDebug() << "begin";
        _controlDataMutex.unlock();
        //    qDebug() << "end";
    }

    std::vector<unsigned char>& controlData() override    // controlData
    {
        return _controlData;
    }

    void quit() override;

    void doWork() override;

protected:
    virtual void cancelRequest() {}

    bool xferControlData();
    bool xferPoints();

    virtual bool xfer(unsigned char* /*buf*/, int /*size*/)
    {
        return true;
    }

    void setScannerState(ScannerStateType state);
    TriggerType triggerType();
    TriggerSourceType triggerSource();
    void waitForHold();
    virtual bool completeWork();


    QList<IBinder*>* _binderList;

    int _startBinderIdx;

    std::atomic_bool _cancel;
    std::atomic_bool _needToQuit;
    std::atomic_int _scannerState;
    std::atomic_int _triggerType;
    std::atomic_int _triggerSource;
    std::atomic_bool _triggerSignal;

    QMutex _controlDataMutex;
    std::vector<unsigned char>_controlData;

    QString _name;
    QMutex _cancelMutex;



};

} //namespace Hardware
} //namespace VNA
} //namespace Planar
