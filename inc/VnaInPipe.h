#pragma once

#include <vector>

#include "VnaHw.h"
#include "VnaInPipeBase.h"
#include "LibUsbBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class VnaInPipe : public VnaInPipeBase
{
    Q_OBJECT

public:
    VnaInPipe(libusb_device_handle* hDev, unsigned char epNum, VnaCmd* cmd, QList<IBinder*>* binderList, QList<NoticeProperty*>& noticeList, QList<Planar::VNA::MeasuringResult*>& results);
    ~VnaInPipe() override;

protected:
    bool beginWork() override;
    bool loopWork() override;
    bool completeWork() override;

    void cancelRequest() override;

    Planar::Hardware::LibUsbPoolXferBuffered* _xfer;
};

} //namespace Hardware
} //namespace VNA
} //namespace Planar
