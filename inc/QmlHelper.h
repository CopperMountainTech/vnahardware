#pragma once

#include <QString>

namespace Planar {
namespace VNA {
namespace Hardware {

// закомментировать строку для сборки с AIMTree
#define VNAAPPLICATION

#ifdef VNAAPPLICATION

#define QML_RCP_INT "qrc:/RightClickPanel/ControlElements/IntNumberValidator.qml"
#define QML_RCP_BOOL "qrc:/RightClickPanel/ControlElements/SwitchControl.qml"
#define QML_RCP_BUTTON "qrc:/RightClickPanel/ControlElements/ButtonControl.qml"
#define QML_RCP_ENUM "qrc:/RightClickPanel/ControlElements/ComboboxControlEnum.qml"
#define QML_RCP_LABEL "qrc:/RightClickPanel/ControlElements/ReadOnlyLabel.qml"
#define QML_RCP_UNIT "qrc:/RightClickPanel/ControlElements/UnitValueValidator.qml"


#else

#define QML_RCP_INT "qrc:/IntProperty.qml"
#define QML_RCP_BOOL "qrc:/BoolProperty.qml"
#define QML_RCP_BUTTON "qrc:/Button.qml"
#define QML_RCP_ENUM "qrc:/EnumProperty.qml"
#define QML_RCP_LABEL "qrc:/UnitPropertyReadOnly.qml"
#define QML_RCP_UNIT "qrc:/UnitPropertyReadOnly.qml"


#endif

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
