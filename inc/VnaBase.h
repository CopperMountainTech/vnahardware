#ifndef VNABASE_H
#define VNABASE_H

#include <QObject>
#include <atomic>
#include <QList>
#include <QJsonObject>
#include <QThread>

#include "VnaHw.h"
#include "LibUsbBase.h"

#include "SharedMemory.h"
#include "VnaCmd.h"
#include "FactoryCalibration.h"
#include "VnaDevicePool.h"
#include "VnaOutPipe.h"
#include "VnaInPipe.h"

#include "Properties/BoolProperty.h"
#include "Properties/IntProperty.h"
#include "Properties/EnumProperty.h"

#include "Properties/TriggerInProperty.h"
#include "Properties/TriggerOutProperty.h"

#include "Properties/TemperatureNotice.h"

namespace Planar {
namespace VNA {
namespace Hardware {

//
// Базовый функционал VNA
//
class  VnaBase : public IVna
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(IntProperty* serialNumber READ serialNumber NOTIFY propertyChanged)
    Q_PROPERTY(EnumPropertyBase* triggerType READ triggerType NOTIFY propertyChanged)
    Q_PROPERTY(EnumPropertyBase* triggerSource READ triggerSource NOTIFY propertyChanged)
    Q_PROPERTY(EnumPropertyBase* scannerState READ scannerState NOTIFY propertyChanged)

public:
    VnaBase(VnaDevicePool* pool, Planar::Hardware::DeviceId id, Planar::AnalyzerItem* parent);
    ~VnaBase() override;

    bool select(bool select) override;

    Planar::Hardware::DeviceId uniqueId() override
    {
        return _deviceId;                                   // Уникальный идентификатор устройства
    }

    VnaConstraints* constraints() override
    {
        return _constraints;
    }

    // каналы
    IBinder* bind(ScanRangeBase* range) override;

    void unbind(IBinder* findBinder) override;


    FactoryCalibrationBundle* FactoryCalibrations() override
    {
        return _factoryCalibrations;
    }


    virtual bool initialize(bool firstInstance) override;        // Вызывается сразу после создания класса устройства
    bool isSame(DeviceBase* vna) override;

protected:
// properties
    inline IntProperty* serialNumber()
    {
        return &_serialNumber;
    }

    inline EnumProperty<TriggerType>* triggerType()
    {
        return &_triggerType;
    }

    inline EnumProperty<TriggerSourceType>* triggerSource()
    {
        return &_triggerSource;
    }

    Q_INVOKABLE void setTrigger();

    inline EnumProperty<ScannerStateType>* scannerState()
    {
        return &_scannerState;
    }


    VnaDevicePool* _vnaPool;                // то же что и pool.
    int _hwId;                              // аппаратная модификация
    VnaConstraints* _constraints;            // JSON constartints

    bool _isLoader;                         // true, если загрузчик
    Planar::Hardware::DeviceId _deviceId;                              // Уникальный идентификатор

// USB устройство
    libusb_device_handle* _hDev;            // указатель на структуру, содержащую хендл устройства
    virtual bool open() override;
    virtual void close() override;
    Planar::Hardware::IUsbControlEp* _controlPipe;
    Planar::Hardware::IUsbControlEp* controlPipe() override;  // интерфейс ControlEp

    virtual bool activate() override;
    virtual void deactivate() override;

// Команды VNA
    VnaCmd* _cmd;
    virtual VnaCmd* Cmd();
    virtual bool FwLoad();                                  // здесь загружаем микропрограмму в устройство
    virtual bool initKeyAndSerial();                        // здесь читаем серийный номер из устройства.

// каналы
    QList<IBinder*> _binderList;
    virtual IBinder* CreateBinder(ScanRangeBase* range);    // Наследник может переопределить этот метод и создавать свой Binder
    IBinder* findBinder(ScanRangeBase* range);                  // ищет binder по range

// заводские калибровки
    FactoryCalibrationBundle* _factoryCalibrations;
    virtual FactoryCalibrationBundle* CreateFactoryCalibrations()
    {
        return nullptr;
    }


// конструкторы интегрантов [depricated]
//   virtual IAdcCalibration* CreateAdcCalibration();
//   virtual IExtendedDynRange* CreateReceiver();                                    // Чувствительности приёмников портов

// конвейер измерения
    QThread _outPipeThread;
    IVnaOutPipe* _outPipe;

    QThread _inPipeThread;
    IVnaInPipe* _inPipe;

    virtual bool createPipeline();                                      // Создание и запуск конвейера измерения
    virtual bool destroyPipeline();                                     // Останов и освобождение ресурсов конвейера измерения

    virtual void startPipeline();
    virtual void stopPipeline();


// property members
    IntProperty _serialNumber;
    EnumProperty<TriggerType> _triggerType;
    EnumProperty<TriggerSourceType> _triggerSource;
    EnumProperty<ScannerStateType> _scannerState;

// NoticePropertiesList
    QList<NoticeProperty*> _noticeList;                                 // сюда добавляем NoticeProperty

// результаты измерений
    QList<Planar::VNA::MeasuringResult*> _results;                       //

protected slots:
// property setters callback
    virtual void onSetSerialNumber(int value);
    virtual void onSetTriggerType(int tgType);
    virtual void onSetTriggerSource(int source);

    void onScannerStateChanged(ScannerStateType state);
    void onTriggerTypeChanged(TriggerType triggerType);

    void onRebuildRangeFinished(int buildId);                           // подписать на сигналы PropertyChanged всех Binder
    void onRebuildRangeStarted();                                       // перед ребюлдом range
    void onActivePortListChanged();                                           // после изменения активного порта

    void onBinderListChanged();          // через очередь после изменения _binderList


signals:
    void bindersChanged();              // после изменения _binderList

};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // VNABASE_H
