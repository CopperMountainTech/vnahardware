#ifndef BinderEx_H
#define BinderEx_H

#include <QList>

#include "VnaHw.h"
#include "Binder.h"
#include "VnaCmd.h"
#include "ScanRangeBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {

// базовый класс. От этого класса должны наследоваться Binder устройств
class  BinderCx220 : public Binder
{
    Q_OBJECT
public:
    BinderCx220(VnaCmd* cmd, ScanRangeBase* range);
    ~BinderCx220();

    void initialize();                                    // Этот метод вызывается после создания класса Binder.
    IImpulseGenerators* ImpulseGenerators()
    {
        return _impulseGenerators;
    }

protected:
    IImpulseGenerators* _impulseGenerators;
    void outProcessing();                           // Формирует выходной массив данных команд.
    virtual IImpulseGenerators*
    CreateImpulseGenerators();  // Наследник Binder не обязан переопределять этот метод
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // BinderEx_H
