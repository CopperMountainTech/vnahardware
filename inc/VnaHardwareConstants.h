#ifndef VNAHARDWARECONSTANTS_H
#define VNAHARDWARECONSTANTS_H

namespace Planar {
namespace VNA {
namespace Hardware {


//
// Тип приёмников порта
//
enum RECEIVER_TYPE
{
    RECEIVER_TYPE_TR,                       // тестовый и опорный приёмники
    RECEIVER_TYPE_T,                        // только тестовый приёмник
    RECEIVER_TYPE_R                         // только опорный приёмник
};


struct DRangeProp
{
    bool Enable;            // Включение / отключение расширенного динамического диапазона
    int Align;              // Выравнивание
    int FresholdLo;         // Нижний порог
    int FresholdHi;         // Верхний порог
};


enum SENS : unsigned char
{
    SENS_LOW,
    SENS_HIGH
};


struct SensProp
{
    SENS T1;
    SENS R1;
    SENS T2;
    SENS R2;
};


struct SyncProperty
{
    SyncProperty(): Enable(false), Event(TriggerEventType::teOnSweep), Edge(TriggerPolarityType::tpNegativeEdge), Pos(TriggerPositionType::tpBeforeSampl),
        Delay(Second(0)) {}

    bool Enable;                            // Разрешает формирование СИ
    TriggerEventType Event;
    TriggerPolarityType Edge;
    TriggerPositionType Pos;
    Unit Delay;
    TriggerLevelType Level;
    TriggerCapture   Cap;
};


struct VoltmeterProp
{
    bool Enable;                                    // Разрешение работы вольтметра
    SENS Sens[2];                                   // Чувствительность каналов
};

//
// Состояние детектора перегрузок
//
enum OVERLOAD
{
    OVERLOAD_NO,                                   // Нормальный уровень мощности
    OVERLOAD_TH1,                                  // Превышен порог 1
    OVERLOAD_TH2                                   // Превышен порог 2
};


enum REFOSC_TUNE_MODE
{
    OFF,
    ONCE,
    CONTINUE
};


struct RefOscTuneProp
{
    REFOSC_TUNE_MODE Mode;          // Режим подстройки
    int Value;                      // Значение регистра триммера подстройки частоты
    int Interval;                   // в секундах
};


enum TypeImpulseGenerator : char
{
    tgExternal0 = 0x00,
    tgExternal1 = 0x01,
    tgExternal2 = 0x02,
    tgExternal3 = 0x03,
    tgModulator = 0x04,
    tgGating    = 0x05
};


enum ModeImpulseGenerator : char
{
    //mgDisable   = 0x00,
    //mgEnable    = 0x01, // Для gating генератора
    mgTemplate1 = 0x01,
    mgTemplate2 = 0x02,
    mgTemplate3 = 0x03,
    mgTemplate4 = 0x04,
    mgBrust     = 0x05,
    mgContinue  = 0x06
};

enum ImpulsPolarity : char
{
    ipPositive = 0x00,
    ipNegative = 0x01
};


#pragma pack(push, 1)
struct ImpulsTemplate
{
    int Delay;
    int Width;
};

struct BrustConfig

{
    int  Delay;  // задержка x10нс
    int  Width;  // Длительность x10нс
    int  Period; // Период x10нс
    int  Count;  // Число импульсов
};

#pragma pack(pop)

struct ImpulseProp
{
    bool Enable;

    TypeImpulseGenerator Type; // для tgGating всегда Brust
    ModeImpulseGenerator Mode; // если tgGating, то mgDisable или mgEnable

    ImpulsPolarity Polarity;

    // настраивается либо Temps, либо Brust
    QVector<ImpulsTemplate> Temps;
    BrustConfig             Brust;

    bool operator == (const ImpulseProp& right) const
    {
        if ( this->Enable != right.Enable || this->Type     != right.Type     ||
                this->Mode   != right.Mode   || this->Polarity != right.Polarity )
            return false;

        if ( this->Temps.size() != right.Temps.size() )
            return false;

        for ( int i = 0 ; i < this->Temps.size(); ++ i )
            if ( this->Temps[ i ].Delay != right.Temps[ i ].Delay ||
                    this->Temps[ i ].Width != right.Temps[ i ].Width )
                return false;

        if ( this->Brust.Count  != right.Brust.Count  ||
                this->Brust.Delay  != right.Brust.Delay  ||
                this->Brust.Period != right.Brust.Period ||
                this->Brust.Width  != right.Brust.Width )
            return false;

        return true;
    }
    inline bool operator != (const ImpulseProp& right) const
    {
        return !(*this == right);
    }
};


}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // VNAHARDWARECONSTANTS_H
