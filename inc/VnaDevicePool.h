#ifndef VNADEVICEPOOL_H
#define VNADEVICEPOOL_H

#include "DevicePool.h"
#include "VnaConstraints.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class VnaDevicePool : public Planar::Hardware::DevicePool
{
public:
    VnaDevicePool(const QJsonObject& jsonDeviceObj);

    // VNA Property
    QMap<int, VnaConstraints> Constraints;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // VNADEVICEPOOL_H
