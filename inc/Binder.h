#ifndef Binder_H
#define Binder_H

#include <QList>
#include <QMutex>

#include "VnaHw.h"

#include "VnaCmd.h"


namespace Planar {
namespace VNA {
namespace Hardware {

// базовый класс. От этого класса должны наследоваться Binder устройств
class  Binder : public IBinder
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

public:
    Binder(Planar::AnalyzerItem* parent, VnaCmd* cmd, ScanRangeBase* range);
    ~Binder() override;

    virtual void initialize() {}                                   // Этот метод вызывается после создания класса Binder.

    inline ScanRangeBase* range() override
    {
        return _range;
    }

protected:
    VnaCmd* _cmd;
    ScanRangeBase* _range;                          // диапазон сканирования
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // Binder_H
