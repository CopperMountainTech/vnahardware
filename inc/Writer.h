#ifndef BULKIO_H
#define BULKIO_H

#include <QObject>
#include <stdint.h>
#include <QVector>

//#include "HwGlobal.h"

#include "IUsbIO.h"
#include "Binder.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class Writer : public QObject
{
    Q_OBJECT

public:
    Writer(IIO* io, unsigned char ep);
    ~Writer();

    bool BeginXfer(unsigned char* buf, int size, int pktId = 0);
    bool CancelXfer();
    bool WaitCompleted(int msTimeout);

protected:
    IUsbIO* _io;
    XferContext* _context;
    bool _cancel;

protected slots:
    void OnXferCompleted(XferContext* context);

signals:
    void XferCompleted(XferContext* context);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // XFER_H
