#pragma once

namespace Planar {
namespace VNA {
namespace Hardware {

//
// Упаковка минимально необходимого набора делителей
//
class PLL_DIVIDERS
{
public:
    int R;
    int N;
    int F;
    int M;
    int D;

    PLL_DIVIDERS(int r, int n, int f, int m, int d) : R(r), N(n), F(f), M(m), D(d) { }
    PLL_DIVIDERS(int r, int n) : R(r), N(n) { }
    PLL_DIVIDERS() { }
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
