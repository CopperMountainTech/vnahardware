#pragma once

#include <stdint.h>
#include <QString>
#include <QFile>
#include <QVector>


namespace Planar {
namespace Hardware {

enum LDR_TYPE
        {
            LDR_TYPE_UNKNOW,
            LDR_TYPE_BINARY8,
            LDR_TYPE_ASCII
        };

//
// Парсер файла микропрограммы DSP
//
class FwParser
{
    public:
        static bool Parse(QString fileName, QVector<uint8_t>& fw);

    private:
        static LDR_TYPE GetLdrType(QFile &fp);
        static bool  ParseBinary(QFile &fp, QVector<uint8_t>& fw);
        static bool ParseAscii(QFile &fp, QVector<uint8_t>& fw);
};

} // namespace Hardware
} // namespace Planar


