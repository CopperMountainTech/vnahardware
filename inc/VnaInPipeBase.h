#pragma once

#include <QThread>
#include <QSemaphore>

#include "VnaHw.h"
#include "IVnaInPipe.h"
#include "MeasuringResult.h"
#include "VnaCmd.h"

#include "NoticeProperty.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class VnaInPipeBase : public IVnaInPipe
{
    Q_OBJECT

public:
    VnaInPipeBase(VnaCmd* cmd, QList<IBinder*>* binderList, QList<NoticeProperty*>& noticeList, QList<Planar::VNA::MeasuringResult*>& results);
    ~VnaInPipeBase();

    virtual void setBinderIdx(int idx);


protected:
    void Parse(unsigned char* buf, int length);
    bool ControlInProcessing(unsigned char* buf);
    void resultUpdated(IBinder* binder, MeasuringResult* result);
    MeasuringResult* tryGetFreeChunk();

    VnaCmd* _cmd;
    QList<IBinder*>* _binderList;
    QList<NoticeProperty*>& _noticeList;
    QList<Planar::VNA::MeasuringResult*>& _results;

    std::atomic_int _startBinderIdx;
    int _rxBinderIdx;
};

} //namespace Hardware
} //namespace VNA
} //namespace Planar
