#ifndef VNACMD_H
#define VNACMD_H

#include <vector>

#include "DeviceBase.h"
#include "VnaFoundationConstants.h"
#include "MeasuringResult.h"


namespace Planar {
namespace VNA {
namespace Hardware {

//
// Базовый класс команд
//
class  VnaCmd
{
public:
    VnaCmd(Planar::Hardware::DeviceBase* device);
    virtual ~VnaCmd();

    // Обращаются к железу через Control интерфейс
    virtual bool ResetPipeline();                                           // Сброс конвейера
    virtual bool FlashRead(int addr, unsigned char* buf, int size);         // Чтение FLASH памяти
    virtual bool FlashWrite(int addr, unsigned char* buf, int size);        // Запись FLASH памяти
    virtual bool DspWrite(unsigned char* buf, int size);                    // запись микропрограммы в RAM DSP
    virtual bool FpgaWrite(unsigned char* buf, int size);                   // Запись микропрограммы FPGA

    // команды управления по bulk интерфейсу
    virtual void cmdStartPipeline(std::vector<unsigned char>& ctrlData);    // старт конвейера
    virtual void cmdPower(std::vector<unsigned char>& ctrlData, bool enable); // включение/выключение мощности
    virtual void cmdTriggerInConfigure(std::vector<unsigned char>& ctrlData, TriggerEventType event, TriggerPolarityType polarity, TriggerPositionType position, int delay);
    virtual void cmdTriggerOutConfigure(std::vector<unsigned char>& ctrlData, bool enable, TriggerPolarityType polarity, TriggerOutPositionType position);
    virtual void cmdPowerDetectorConfigure(std::vector<unsigned char>& ctrlData, bool enable);
    virtual void cmdTemperature(std::vector<unsigned char>& ctrlData);
    // ...

    // команды проверки ответов (условий)
    virtual bool isBackScan(unsigned char* buf);                         // проверка ответа "обратный ход"
    virtual bool isPipelineStart(unsigned char* buf);                    // проверка ответа "старт конвейера"
    virtual bool isMeasure(unsigned char* buf);                          // проверка ответа измерения

    // парсеры ответов
    virtual bool tryParseTemperature(std::vector<double>& temperature, unsigned char* buf); // проверка ответа температуры
    virtual bool tryParseMeasure(MeasuringResult* result, unsigned char* buf); // проверка и парсинг результатов измерения
    virtual bool tryParsePointIndex(int& pointIndex, unsigned char* buf);    // индекс точки
    virtual bool tryParsePortIndex(int& portIndex, unsigned char* buf);      // индекс порта

    int AnswerLength();                                                  // длина ответа

    bool IsMeasureEnable()
    {
        return _measureEnable;
    }

    void setMeasureEnable(bool state)
    {
        _measureEnable = state;
    }

    int markerRestart()
    {
        return _markerRestart;
    }


protected:
    Planar::Hardware::DeviceBase* _device;
    int _answerLength;
    void* AppendPkt(std::vector<unsigned char>& ctrlData, int size);        // добавляет пустую команду к вектору

    int _markerId;
    bool _measureEnable;
    int _markerRestart;

    int _pointIdx;
    int _error;

};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // VNACMD_H
