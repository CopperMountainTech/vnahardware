#ifndef VNACMDEX_H
#define VNACMDEX_H

#include "VnaCmd.h"
#include "VnaHw.h"
#include "VnaHardwareConstants.h"

namespace Planar {
namespace VNA {
namespace Hardware {
//
// C1220, C3220
//
class VnaCmdCx220 : public VnaCmd
{
public:
    VnaCmdCx220(Planar::Hardware::DeviceBase* device);
    ~VnaCmdCx220();

    bool LORead(int addr, unsigned char* buf, int size);
    bool RFRead(int addr, unsigned char* buf, int size);
    bool VoltRead(int addr, unsigned char* buf, int size);

    virtual void CmdImpuleGenerator(std::vector<unsigned char>&, int,
                                    const ImpulseProp& ) {}    // управление импульсными генераторами

protected:
    bool LoRfVoltmeterRead( uint fl, int addr, unsigned char* buf, int size);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // VNACMDEX_H
