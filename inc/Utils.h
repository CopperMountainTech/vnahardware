#pragma once

#include <limits.h>
#include <vector>

#include "FoundationGlobal.h"

namespace Planar {
namespace VNA {
namespace Hardware {

bool  GetVersionOfFile (char* pszAppName,     // file
                        char* pszVerBuff,     // receives version
                        int    iVerBuffLen,   // size of buffer
                        char* pszLangBuff,    // receives language
                        int    iLangBuffLen); // size of buffer

QString BitFieldToString(int Value, int FieldPos, int FieldSize);
void StringToBitField(QString Str, int& Value, int FieldPos, int FieldSize);


QString  SymbolicIndex(QString Str, int Index);

//todo save
//bool  Save(TFastIni* ini, const QString Section, const QString Name, const double Value, const double Default);
//bool  Save(TFastIni* ini, const QString Section, const QString Name, const int Value, const int Default);
//bool  Save(TFastIni* ini, const QString Section, const QString Name, const unsigned Value, const unsigned Default);
//bool  Save(TFastIni* ini, const QString Section, const QString Name, const bool Value, const bool Default);
//bool  Save(TFastIni* ini, const QString Section, const QString Name, const QString Value, const QString Default);

QString   GetAppDirectory();
QString   GetDataDirectory();
QString   GetFixtureDirectory(bool Create);
QString   CompleteFixtureFileName(QString FileName, bool Create);
QString   GetPerfTestDirectory(bool Create);
QString   CompleteFactoryTestFileName(QString FileName, bool Create);


void  ChangeFileExt(QString& FileName, const QString& Ext);
void  ChangeEmptyFileExt(QString& FileName, const QString& Ext);
void  ExtractValues(QString& source, std::vector<double>& Values);
void  ExtractValues(QString& source, std::vector<int>& Values);

double   CorrectDecimalSeparator(const QString& source);
//void  CorrectReadFloat(TFastIni* ini, const QString Section, const QString Name, double &Value, const double Default);

} //namespace Hardware
} //namespace VNA
} //namespace Planar




