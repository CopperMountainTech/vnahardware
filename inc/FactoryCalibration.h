#ifndef FACTORYCALIBRATION_H
#define FACTORYCALIBRATION_H

//#include "IIO.h"
#include "VnaCmd.h"

#include "CalibrationRange.h"
#include "RcvCalibrationRange.h"

namespace Planar {
namespace VNA {
namespace Hardware {

struct PowerTableSegment {
    double  start;
    double  stop;
    int     points;
    double Step()
    {
        return (stop - start) / (points - 1);
    }
};
using PowerTableSegmentsType = std::map<quint16, std::vector<PowerTableSegment>>;

///
/// \brief The FactoryCalibrationPoint class
///
//class HARDWARE_EXPORT FactoryCalibrationPoint
//        //: public CalibrationPoint
//{
//public:
//    FactoryCalibrationPoint(QDataStream &in);
//protected:
//    virtual void Load(QDataStream &in);
//    virtual void Save(QDataStream &in);
//};



///
/// \brief The FactoryCalibration base class
///
class  FactoryCalibration
{
public:
    FactoryCalibration(const VnaCmd& cmd);
    virtual ~FactoryCalibration() = default;

    virtual void Load(QByteArray& data) = 0; ///< Loading from eeprom
    virtual void Save(QByteArray& data) = 0; ///< Saving to eeprom
    bool ReadAndCheck(quint32& address, unsigned char* buffer, int size);
    bool ReadAndCheck(const QByteArray& data, quint32& address, unsigned char* buffer, int size);
protected:
    const VnaCmd& _cmd;  ///< vna device commands
};



///
/// \brief The FactoryTermsCalibration class
///
class  FactoryTermsCalibration
    : public FactoryCalibration, public Planar::VNA::CalibrationRange
{
public:
    FactoryTermsCalibration(const VnaCmd& cmd);
    virtual ~FactoryTermsCalibration() = default;

    // FactoryCalibration interface
    virtual void Load(QByteArray& data) = 0;
    virtual void Save(QByteArray& data) = 0;
};



///
/// \brief The FactoryPowerCalibration class
///
class  FactoryPowerCalibration
    : public FactoryCalibration, public Planar::VNA::RcvCalibrationRange
{
public:
    FactoryPowerCalibration(const VnaCmd& cmd);
    FactoryPowerCalibration(const VnaCmd& cmd, Planar::VNA::CalibrationRange* range);
    virtual ~FactoryPowerCalibration() = default;

    // FactoryCalibration interface
    virtual void Load(QByteArray& data) = 0;
    virtual void Save(QByteArray& data) = 0;

    //virtual double R(double frequency) = 0;
};



///
/// \brief The FactoryCalibrationBundle class
///
class  FactoryCalibrationBundle
//: public FactoryCalibration
{
public:
    FactoryCalibrationBundle(const VnaCmd& cmd);
    virtual ~FactoryCalibrationBundle() = default;

    FactoryTermsCalibration* Terms()
    {
        return _termsCalibration;
    }
    FactoryPowerCalibration* Power()
    {
        return _powerCalibration;
    }

    // FactoryCalibration interface
public:
    virtual void Load() = 0;
    virtual void Save() = 0;
protected:
    virtual FactoryTermsCalibration* CreateFactoryTermsCalibratio() = 0;
    virtual FactoryPowerCalibration* CreateFactoryPowerCalibratio() = 0;

    FactoryTermsCalibration* _termsCalibration;
    FactoryPowerCalibration* _powerCalibration;

    const VnaCmd& _cmd;  ///< commands
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // FACTORYCALIBRATION_H
