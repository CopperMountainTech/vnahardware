//
// Протокол обмена данными для всех VNA
//
#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <complex>

namespace Planar {
namespace VNA {
namespace Hardware {

// Команды Control интерфейса
enum VEN_RQ
{
    VEN_RQ_FLASH_RD = 0x03,
    VEN_RQ_FLASH_WR = 0x05,
    VEN_RQ_PIPLENE_RESET = 0x06,
    VEN_RQ_DSP_WR = 0x08,
    VEN_RQ_FPGA_WR = 0x10,
    VEN_RQ_FLASH_EN = 0x0B,

    VEN_RQ_BLOCK_MEM_RD = 0x0D,                             // Чтение памяти блоков LO, RF, Voltmeter (C3220, C1220, C1209)
    VEN_RQ_BLOCK_MEM_WR = 0x0E                              // Запись памяти блоков LO, RF, Voltmeter (C3220, C1220, C1209)
};

// команды bulk интерфейса
enum BULK_RQ
{
    BULK_RQ_PRESET_DSP = 0xFFEF,
    BULK_RQ_MEASURE = 0xFF00,                               // Команды измерения до этого кода. По сути это номер точки.
    BULK_RQ_IMP_GEN = 0xFFF0,                               // Конфигурирование импульсных генераторов
    BULK_RQ_FREQ_TUNE = 0xFFF1,                             // подстройка частоты
    BULK_RQ_POWER = 0xFFF2,                                 // вкл./выкл. мощности ВЧ
    BULK_RQ_EXT_TG_IN = 0xFFF3,                             // активировать вход внешнего тригера
    BULK_RQ_EXT_AD_TG_IN = 0xFFE9,                          // Настройки входных дополнительных триггеров
    BULK_RQ_EXT_AD_TG = 0xFFE8,                             // настройки внутреннего триггера
    BULK_RQ_TEM = 0xFFF4,                                   // измерение температуры (с запросом только 304)
    BULK_RQ_OSC_SRC = 0xFFF5,                               // переключить источник опоры
    BULK_RQ_DREG = 0xFFF6,                                  // загрузить регистр для отладки 5048
    BULK_RQ_EXT_TG_OUT = 0xFFF7,                            // настройки выхода внешнего тригера
    BULK_RQ_EXT_AD_TG_OUT = 0xFFEA,                         // Настройки выходов дополнительных триггеров
    BULK_RQ_EN_PWR_OFF = 0xFFF8,                            // разрешение отключения мощности при перегрузке (по порогу 2)
    BULK_RQ_OVERLOAD = 0xFFF9,                              // сработка перегрузки
    BULK_RQ_ADC_SYNC = 0xFFFA,                              // синхронизация АЦП
    BULK_RQ_PIPELINE_START = 0xFFFB,                        // старт конвейера с указанным маркером
    BULK_RQ_PRESET = 0xFFFC,                                // предустановка: задержка,  номер генератора, фильтр и др.
    BULK_RQ_CTRL = 0xFFFD,                                  // управление вольтметром, или синтезаторами (TR1300)
    BULK_RQ_ADC_CAL = 0xFFFE,                               // калибровка АЦП.
    BULK_RQ_BACK_SCAN = 0xFFFF                              // загрузить синтезаторы и ответить как можно быстрее. Для 804M логика меняется: ничего не грузим, сразу отвечаем.
};

// TR1300
#define BITP_CMDPKT_ID_PORT     14                              // позиция номера порта
#define BITM_CMDPKT_ID_PORT     (0x3 << BITP_CMDPKT_ID_PORT)    // маска номера порта
#define LOW_PART_POINTS         0x3F00                          // количество точек, в младшей части поля индекса TR1300

// S2 series
#define BITP_CMDPKT_ID_PORT_S2  8                               // позиция номера порта
#define BITM_CMDPKT_ID_PORT_S2  (0x3 << BITP_CMDPKT_ID_PORT_S2) // маска номера порта




#pragma pack(push, 1)

//
// 32 байта ответ (приборы Обзор304 и TR1300)
//
struct InPkt32
{
    unsigned short Id;                                       // Идентификатор команды
    unsigned char  Payload[30];
};


//
// 64 байта ответ (все приборы кроме Обзор304 и TR1300)
//
struct InPkt64
{
    unsigned short Id;                                          // Идентификатор команды
    unsigned short ExtId;                                       // Расширение индекса точки
    unsigned char  Payload[40];
};

//
// 128 байта ответ (приборы C3220)
//
struct InPkt128
{
    unsigned short Id;                           // Идентификатор команды
    unsigned short ExtId;                        // Расширение индекса точки
    unsigned char  Payload[120];
};

//
// 32 байтная команда
//
struct   OutPkt32
{
    unsigned short Id;                          // Идентификатор команды или индекс точки
    unsigned char Payload[28];                  // Данные команды
    unsigned short
    ExtId;                       // Расширение индекса точки (индекс активного порта + ст. слово индекса точки)
};


//
// 64 байтная команда
//
struct   OutPkt64
{
    unsigned short Id;                          // Идентификатор команды или индекс точки
    unsigned char Payload[60];                  // Данные команды
    unsigned short
    ExtId;                       // Расширение индекса точки (индекс активного порта + ст. слово индекса точки)
};



// ...
// Сюда добавить структуру 64 байтной команды
// ...




//
// Данные команды измерения для 2-х портовых приборов
//
struct PayloadMeasurePort2
{
    std::complex<float> T1;
    std::complex<float> T2;
    std::complex<float> R1;
    std::complex<float> R2;
    int V1;                                        // в формате 16.16
    int V2;                                        // в формате 16.16
    unsigned char SensT1;
    unsigned char SensR1;
    unsigned char SensT2;
    unsigned char SensR2;
};

#pragma pack(pop)

#define KEY_SIZE    8                               // размер поля идентификатора прибора (обычно там серийный номер)


}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // PROTOCOL_H
