#pragma once

#include "AnalyzerItem.h"
#include "Properties/BoolProperty.h"
#include "Properties/EnumProperty.h"
#include "Properties/IntProperty.h"
#include "VnaFoundationConstants.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TriggerOutProperty : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(BoolProperty* enable READ enable NOTIFY propertyChanged)
    Q_PROPERTY(EnumPropertyBase* polarity READ polarity NOTIFY propertyChanged)
    Q_PROPERTY(EnumPropertyBase* position READ position NOTIFY propertyChanged)

public:
    TriggerOutProperty(Planar::AnalyzerItem* parent);

    inline BoolProperty* enable()
    {
        return &_enable;
    }

    inline EnumProperty<TriggerPolarityType>* polarity()
    {
        return &_polarity;
    }

    inline EnumProperty<TriggerOutPositionType>* position()
    {
        return &_position;
    }


protected:
    BoolProperty _enable;
    EnumProperty<TriggerPolarityType> _polarity;
    EnumProperty<TriggerOutPositionType> _position;

protected slots:
    void slotBoolItemChanged(bool value);
    void slotItemChanged(int value);

signals:
    void propertyChanged();
    void valueChanged();                    // при изменении конфиурации
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
