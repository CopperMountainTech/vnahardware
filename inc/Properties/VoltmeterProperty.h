#pragma once

#include "AnalyzerItem.h"
#include "Properties/BoolProperty.h"
#include "Properties/EnumProperty.h"
#include "Properties/IntProperty.h"
#include "VnaFoundationConstants.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class VoltmeterProperty : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(BoolProperty* enable READ enable NOTIFY propertyChanged)
    Q_PROPERTY(BoolProperty* highSensitive READ highSensitive NOTIFY propertyChanged)

public:
    VoltmeterProperty(Planar::AnalyzerItem* parent);

    inline BoolProperty* enable()
    {
        return &_enable;
    }

    inline BoolProperty* highSensitive()
    {
        return &_highSensitive;
    }

protected:
    BoolProperty _enable;
    BoolProperty _highSensitive;

protected slots:
    void slotBoolItemChanged(bool value);

signals:
    void propertyChanged();
    void valueChanged();                    // при изменении конфиурации
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
