#pragma once

#include "Properties/UnitProperty.h"
#include "VnaCmd.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class NoticeProperty : public UnitProperty
{
    Q_OBJECT
public:
    NoticeProperty(Planar::AnalyzerItem* parent, const Planar::Unit& value, const Unit& min, const Unit& max);

    virtual bool inProcessing(VnaCmd* cmd, unsigned char* buf);  // true, если буфер обработан
protected:

};

} //namespace Hardware
} //namespace VNA
} //namespace Planar

