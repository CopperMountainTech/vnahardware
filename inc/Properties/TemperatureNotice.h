#pragma once

#include "Properties/NoticeProperty.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class TemperatureNotice : public NoticeProperty
{
    Q_OBJECT
public:
    TemperatureNotice(AnalyzerItem* parent);
    bool inProcessing(VnaCmd* cmd, unsigned char* buf) override;

};

} //namespace Hardware
} //namespace VNA
} //namespace Planar
