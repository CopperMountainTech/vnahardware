#pragma once

#include <QTimer>

#include "Properties/TemperatureNotice.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TR1300TemperatureNotice : public TemperatureNotice
{
    Q_OBJECT
public:
    TR1300TemperatureNotice(AnalyzerItem* parent);
    void setEnable(bool enable);

protected:
    QTimer _timer;
    bool _enable;

protected slots:
    void slotTimout();

signals:
    void temperatureRequest();

};

} //namespace Hardware
} //namespace VNA
} //namespace Planar
