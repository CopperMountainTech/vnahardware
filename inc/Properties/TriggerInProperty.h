#pragma once

#include "AnalyzerItem.h"
#include "Properties/EnumProperty.h"
#include "Properties/IntProperty.h"
#include "VnaFoundationConstants.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class TriggerInProperty : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(EnumPropertyBase* eventType READ eventType NOTIFY propertyChanged)
    Q_PROPERTY(EnumPropertyBase* polarity READ polarity NOTIFY propertyChanged)
    Q_PROPERTY(EnumPropertyBase* position READ position NOTIFY propertyChanged)
    Q_PROPERTY(IntProperty* delay READ delay NOTIFY propertyChanged)

public:
    TriggerInProperty(Planar::AnalyzerItem* parent);

    inline EnumProperty<TriggerEventType>* eventType()
    {
        return &_eventType;
    }

    inline EnumProperty<TriggerPolarityType>* polarity()
    {
        return &_polarity;
    }

    inline EnumProperty<TriggerPositionType>* position()
    {
        return &_position;
    }

    inline IntProperty* delay()
    {
        return &_delay;
    }

protected:
    EnumProperty<TriggerEventType> _eventType;
    EnumProperty<TriggerPolarityType> _polarity;
    EnumProperty<TriggerPositionType> _position;
    IntProperty _delay;

protected slots:
    void slotItemChanged(int value);

signals:
    void propertyChanged();
    void valueChanged();                    // при изменении конфиурации
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
