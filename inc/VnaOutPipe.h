#pragma once

#include <QThread>
#include <atomic>
#include <QWaitCondition>
#include <QMutex>
#include <vector>


#include "VnaFoundationConstants.h"
#include "LibUsbBase.h"
#include "VnaOutPipeBase.h"
#include "VnaHw.h"
#include "LibUsbBase.h"

namespace Planar {
namespace VNA {
namespace Hardware {


class VnaOutPipe : public VnaOutPipeBase
{
    Q_OBJECT

public:
    VnaOutPipe(libusb_device_handle* hDev, unsigned char epNum, QList<IBinder*>* binderList);
    ~VnaOutPipe() override;

protected:
    void cancelRequest() override;
    bool xfer(unsigned char* buf, int size) override;
    bool completeWork() override;

    Planar::Hardware::LibUsbPoolXferDirect* _bulkPipe;
};

} //namespace Hardware
} //namespace VNA
} //namespace Planar
