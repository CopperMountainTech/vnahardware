#ifndef INTEGRATETRIGGER_H
#define INTEGRATETRIGGER_H

#include <QPair>
#include <QMap>

#include "VnaHw.h"

namespace Planar {
namespace VNA {
namespace Hardware {
/*
typedef QMap<TypeImpulseGenerator, QPair<SourceTrigger, FrontType> > IntegrateTriggers;

class IntegrateTrigger : public IIntegrateTrigger
{
public:
    IntegrateTrigger();
    virtual bool OutProcessing(std::vector<unsigned char> & ) {}
    SourceTrigger GetSource( TypeImpulseGenerator distr );
    FrontType GetFront( TypeImpulseGenerator distr );

public slots:
    void AppointTrigger( TypeImpulseGenerator distr, SourceTrigger src );
    void SetFront(TypeImpulseGenerator distr, FrontType front);

protected:
    IntegrateTriggers _triggers;
};
*/
}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // INTEGRATETRIGGER_H
