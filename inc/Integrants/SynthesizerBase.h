﻿#pragma once

#include "Integrants/IIntegrant.h"

#include "VnaCmd.h" // -> Protocol.h  -> структуры данных!
#include "ScanRangeBase.h"


namespace Planar {
namespace VNA {
namespace Hardware {

//
// Синтезатор данных росчерка. Хранит данные росчерка.
//
class SynthesizerBase : public IIntegrant
{
public:
    SynthesizerBase(ScanRangeBase* range);
    ~SynthesizerBase() override;

    virtual bool processing() override;
    inline unsigned char* data(unsigned int& size) override
    {
        size = _data.size();
        return _data.data();
    }

protected:
    virtual bool synthesizePoint(int port, int idx);
    virtual bool postSynthesize();
    virtual bool preSynthesize(int activePort);
    virtual void changeActivePort(unsigned int startIdx, unsigned int size, int activePort);

    unsigned short ifbwCode(Unit ifbw);

    ScanRangeBase* _range;
    std::vector<unsigned char> _data;
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar


