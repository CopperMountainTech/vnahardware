#ifndef IMPULSEGENERATORSBASE_H
#define IMPULSEGENERATORSBASE_H

#include "../Hardware/Units/Vna/VnaHw.h"

namespace Hardware
{

class ImpulseGeneratorsBase : public IImpulseGenerators
{
public:
    ImpulseGeneratorsBase(){}

    virtual ImpulseProp* GetProperty( size_t  ) { return new ImpulseProp(); }
    virtual void OutProcessing( vector<unsigned char> & ) {}
    virtual size_t GetCount() const { return 0; }
public slots:
    virtual void SetProperty( size_t, const ImpulseProp & ) {}
};

}
#endif // IMPULSEGENERATORSBASE_H
