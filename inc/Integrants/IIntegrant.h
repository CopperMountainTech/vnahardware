#ifndef IINTEGRANT_H
#define IINTEGRANT_H

namespace Planar {
namespace VNA {
namespace Hardware {

//
// Интерфейс программных подсистем VNA. Предполагается, что это базовый класс...
//
class   IIntegrant
{
public:
    virtual ~IIntegrant();

    // рассчёт данных управления этой подсистемы
    virtual bool processing() = 0;

    //возвращает указатель на рассчитанный в processing массив данных
    virtual unsigned char* data(unsigned int& data) = 0;
};


}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar

#endif // IINTEGRANT_H
