#ifndef SYNTHESIZEREX_H
#define SYNTHESIZEREX_H

#include "Units/Vna/Integrants/Synthesizer.h"
#include "ConfigSyntezBase.h"

namespace Hardware
{

class SynthesizerEx : public SynthesizerBase
{
     Q_OBJECT
public:
    SynthesizerEx(VnaCmd *cmd, ScanRangeBase *range);
    ~SynthesizerEx();

// override SynthesizerBase
    virtual bool InProcessing(MeasuringResult &result, unsigned char *buf);                  // true, если успешно распарсен buf
    virtual void OutProcessing(vector<unsigned char> &outData);                             // упаковка (сериализация) данных команд


    virtual bool SetAtt(const vector<int> &att) { Q_UNUSED(att); return true; }
    ConfigSyntezBase* Cnfg() { return _config; }


protected:
    int _activePort;
    ConfigSyntezBase* _config = Q_NULLPTR;

// override SynthesizerBase
    virtual void Synthesize();                                                  // Синтез частотных точек
    virtual void AppendPoints(vector<unsigned char> &outData);                  // Добавляет точки и команду обратного хода в росчерк
    virtual bool SynthesizePoint(ScanPointBase &point, int pointIdx) = 0;
    virtual void AppendPoint(vector<unsigned char> &outData, ScanPointBase &point, int pointIdx) = 0;
    virtual void AppendBackScan(vector<unsigned char> &outData, ScanPointBase &point) = 0;

private:
    VnaCmd *_cmd;


};

}
#endif // SYNTHESIZEREX_H
