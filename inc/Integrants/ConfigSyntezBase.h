#ifndef CONFIGSYNTEZBASE_H
#define CONFIGSYNTEZBASE_H

#include <QObject>
#include <vector>
#include <QMap>
#include <QVariant>
#include <QDebug>

namespace Hardware
{

using namespace std;

enum SyntezParameter {
    spAnalogFilter,
    spDelayDSP,
    spPeriodDCP,
    spCountDSP
};


class ConfigSyntezBase : public QObject
{
    Q_OBJECT
public:
    ConfigSyntezBase( int HWVersion, QObject* parent = Q_NULLPTR ) : QObject( parent ), _HWVersion(HWVersion)
    {
        Default();
    }
    virtual bool   GetAttControlEnabled() const { return false; }
    virtual double GetAltIFDelta() const { return 0.0; }
    virtual int    GetActivePort() const { return _port; }
    virtual int    GetHWVersion() const { return _HWVersion; }
    virtual vector<int> GetAttControl() const { return vector<int>(); }
    QVariant       GetParameter( SyntezParameter key ) const { return _parameters[ key ]; }
signals:
    void updateCurrentPort( int port );
    void updateAltIFDelta( double altIFDeltaMhz );
    void updateEnableAttControl( bool enable );

public slots:
    virtual void setPort( int port )
    {
        if( port == _port ) return;
        if( port == 0 || port == 1 )
            _port = port;
        emit updateCurrentPort( _port );
    }
    virtual void setAltIFDelta( double mhz ) { qDebug() << "setAltIFDelta: " << mhz; }
    virtual void setEnableAttControl( bool enable) { qDebug() << "setEnableAttControl: " << enable; }
    virtual void setAttControl( const vector<int> &att ) { qDebug() << "setAttControl.size(): " << att.size(); }
    void setParameter( SyntezParameter key, const QVariant &value )
    {
        if( _parameters.contains( key ) )
            if( _parameters[ key ] != value )
            {                
                _parameters[ key ] = value;
                UpdateParameters();
            }
    }

protected:
    virtual void Default() {}
    virtual void UpdateParameters() {}

    int _HWVersion;
    int _port;

    QMap<SyntezParameter, QVariant> _parameters;
};

}
#endif // CONFIGSYNTEZBASE_H
