#pragma once

#include <QObject>
#include <atomic>
#include "WaitCondition.h"

namespace Planar {
namespace VNA {
namespace Hardware {

class VnaPipe : public QObject
{
    Q_OBJECT

public:
    VnaPipe();
    ~VnaPipe();

    void start();
    void stop();
    void quit();

protected:
    // вызываются из рабочего потока
    virtual bool beginWork();       // начало работы
    virtual bool loopWork();        // работа
    virtual bool completeWork();    // завершение работы

    // вызываются из управляющего потока
    void waitForStopped();
    virtual void cancelRequest();

    std::atomic_bool _needToQuit;
    std::atomic_bool _needToStop;
    std::atomic_bool _stopped;
    QString _name;
    QMutex _cancelMutex;

public slots:
    void doWork();
};

} //namespace Hardware
} //namespace VNA
} //namespace Planar
