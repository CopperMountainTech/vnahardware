#ifndef VNAHW_H
#define VNAHW_H

//
// Интерфейс приборов VNA
//

#include "AnalyzerItem.h"
#include "ScanRangeBase.h"
#include "MeasuringResult.h"
#include "FactoryCalibration.h"         // CalibrationRange
#include "VnaConstraints.h"


namespace Planar {
namespace VNA {
namespace Hardware {


class IBinder : public Planar::AnalyzerItem
{
    Q_OBJECT

public:
    using Planar::AnalyzerItem::AnalyzerItem;

    ~IBinder() {}

    // выходной массив данных команд.
    virtual unsigned char* outData(unsigned int& size) = 0;

    // Формирует выходной массив данных команд.
    virtual void outProcessing() = 0;

    virtual ScanRangeBase* range() = 0;

signals:
    void measuringResultReady(MeasuringResult* result);
};



//
// Интерфейс VNA.
//
class IVna: public Planar::Hardware::DeviceBase
{
    Q_OBJECT

public:
    IVna(Planar::Hardware::DevicePool* pool, Planar::AnalyzerItem* parent)
        : Planar::Hardware::DeviceBase(pool, parent)
    {
    }
    virtual ~IVna() override {}

    // Уникальный идентификатор устройства
    virtual Planar::Hardware::DeviceId uniqueId() override = 0;

    // Constraints из JSON
    virtual VnaConstraints* constraints() = 0;

    // Связывает канал сканирования и связывает его с range
    virtual IBinder* bind(ScanRangeBase* range) = 0;

    // Удаляет канал сканирования
    virtual void unbind(IBinder* Binder) = 0;

    // заводские калибровки
    virtual FactoryCalibrationBundle* FactoryCalibrations() = 0;

signals:
    // сканирование завершено (получена последняя точка последнего range)
    void scanFinished();

    // При изменении режима сканера
    void scannerStateChanged(ScannerStateType);
};

}   // namespace Hardware
}   // namespace VNA
}   // namespace Planar
#endif // HW_H
